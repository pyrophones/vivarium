#version 410 core
in vec2 UV;

uniform sampler2D tex;
uniform vec3 texColor;

void main()
{
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(tex, UV).rgb);
    gl_FragColor = vec4(texColor, 1.0) * sampled;
}
