#version 450 core

in VS_OUT
{
	vec2 uv;
} fsIn;

out vec4 fragColor;

uniform sampler2D tex;

void main()
{
	fragColor = texture(tex, fsIn.uv);
}
