#version 450 core

in VS_OUT
{
	vec3 uv;
} fsIn;

out vec4 fragColor;

uniform samplerCube cubeMap;

void main()
{
	fragColor = vec4(texture(cubeMap, fsIn.uv).rgb, 1.0);
}
