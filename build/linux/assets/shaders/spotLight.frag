#version 450 core

struct Light
{
	vec3 pos;
	float specFactor;
	vec3 ambColor;
	float intensity;
	vec3 diffColor;
	float radius;
	vec3 specColor;
};

out vec4 fragColor;

uniform sampler2D gDepth;
uniform sampler2D gRough;
uniform sampler2D gAlbedoMetal;
uniform sampler2D gNormalAo;

layout (std140) uniform LightingInfo
{
	Light light;
	vec2 screenSize;
	vec3 camLoc;
	mat4 invViewProj;
};

void main()
{
	//Set the UV's to use
	vec2 uv = gl_FragCoord.xy / screenSize;

	//Retrieve data from gBuffer
	vec3 normal = texture(gNormalAo, uv).rgb;
	vec3 albedo = texture(gAlbedoMetal, uv).rgb;
	float depth = texture(gDepth, uv).r * 2.0 - 1.0;
	float roughness = texture(gRough, uv).r;
	float metallic = texture(gAlbedoMetal, uv).a;

	vec4 pos = vec4(uv * 2.0 - 1.0, depth, 1.0);
	pos = (invViewProj * pos);
	pos /= pos.w;

	//Lighting variables
	vec3 dir = light.pos - pos.xyz;
	float dist = length(dir);
	vec3 l = normalize(dir);
	vec3 v = normalize(-pos.xyz);
	vec3 h = normalize(l + v);

	vec3 amb = albedo * light.ambColor * light.intensity; //Ambient light
	vec3 diff = clamp(dot(normal, l), 0.0, 1.0) * albedo * light.diffColor * light.intensity; //Diffuse Light
	vec3 spec = pow(clamp(dot(normal, h), 0.0, 1.0), light.specFactor) * metallic * light.specColor * light.intensity; //Specular light
	float att = clamp(1.0 - ((dist * dist) / (light.radius * light.radius)), 0.0, 1.0);
	att *= att;
	float spotAmount = pow(max(dot(-dir, dir), 0.0), att);

	fragColor = vec4(amb + diff + spec, 1.0) * spotAmount;
}
