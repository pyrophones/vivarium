#version 410 core
layout (location = 0) in vec3 vertex; //3D position coords
layout (location = 1) in vec2 uv; //2D texture coords

out vec2 UV;

uniform mat4 projection;

void main()
{
	gl_Position = projection * vec4(vertex, 1.0);
	UV = uv;
}
