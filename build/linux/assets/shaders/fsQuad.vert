#version 450 core

layout (location = 0) in vec3 vertex; //The vertex position
layout (location = 1) in vec2 uv; //The UV coords

out VS_OUT
{
	vec2 uv;
} vsOut;

void main()
{
	vsOut.uv = uv;
	gl_Position = vec4(vertex, 1.0);
}
