#version 450 core

struct Light
{
	vec3 pos;
	float radius;
	vec3 color;
	float intensity;
};

out vec4 fragColor;

uniform sampler2D gDepth;
uniform sampler2D gRough;
uniform sampler2D gAlbedoMetal;
uniform sampler2D gNormalAo;

layout (std140) uniform LightingInfo
{
	Light light;
	vec2 screenSize;
	vec3 camLoc;
	mat4 invViewProj;
};

const float PI = 3.14159265359;
const float ONE_OVER_PI = 0.318309886184;
const vec3 F0_NON_METAL = vec3(0.04);
const float MAX_REFLECTION_LOD = 4.0;

vec3 fresnelSchlick(float cosTheta, vec3 f0)
{
	return f0 + (1.0 - f0) * pow(1.0 - cosTheta, 5.0);
}

float distributionGGX(vec3 n, vec3 h, float roughness)
{
	float a = roughness * roughness;
	float a2 = max(a * a, 0.0000001);

	float nDotH = max(dot(n, h), 0.0);
	float nDotH2 = nDotH * nDotH;

	float denom = nDotH2 * (a2 - 1.0) + 1.0;
	denom = PI * denom * denom;

	return a2 / denom;
}

float partialGeometryGGX(float nDotV, float k)
{
	return nDotV / (nDotV * (1.0 - k) + k);
}

float geometryGGX(float nDotV, float nDotL, float roughness)
{
	float r = roughness + 1.0;
	float k = (r * r) * 0.125;

	return partialGeometryGGX(nDotV, k) * partialGeometryGGX(nDotL, k);
}

void main()
{
	//Set the UV's to use
	vec2 uv = gl_FragCoord.xy / screenSize;

	//Retrieve data from gBuffer
	vec3 normal = texture(gNormalAo, uv).rgb;
	vec3 albedo = texture(gAlbedoMetal, uv).rgb;
	float depth = texture(gDepth, uv).r * 2.0 - 1.0;
	float roughness = texture(gRough, uv).r;
	float metallic = texture(gAlbedoMetal, uv).a;

	vec4 pos = vec4(uv * 2.0 - 1.0, depth, 1.0);
	pos = (invViewProj * pos);
	pos /= pos.w;

	//Lighting variables
	vec3 dir = light.pos - pos.xyz;
	float dist = length(dir);
	vec3 l = normalize(dir);
	vec3 v = normalize(camLoc - pos.xyz);
	vec3 h = normalize(l + v);
	vec3 r = reflect(-v, normal);
	float nDotV = max(dot(normal, v), 0.0);
	float nDotL = max(dot(normal, l), 0.0);

	//Attenuation
	float att = clamp(1.0 - dist * dist / (light.radius * light.radius), 0.0, 1.0);
	att *= att;
	vec3 rad = light.color * att * light.intensity;

	float ndf = distributionGGX(normal, h, roughness);
	float g = geometryGGX(nDotV, nDotL, roughness);

	vec3 f0 = mix(F0_NON_METAL, albedo, metallic);
	vec3 ks = fresnelSchlick(max(dot(h, v), 0.0), f0);

	vec3 num = ndf * g * ks;
	float denom = 4.0 * max(nDotV, nDotL) + 0.0001;
	vec3 spec = num / denom; //Specular

	vec3 kd = vec3(1.0) - ks;
	kd *= 1.0 - metallic;

	vec3 color = (kd * nDotL * albedo + spec) * rad;

	//NOTE: Move these somewhere else
	//color = color / (color + vec3(1.0));
	//color = pow(color, vec3(0.4545454545));

	fragColor = vec4(color, 1.0); //Add the light types
}
