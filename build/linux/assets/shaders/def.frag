#version 450 core

in VS_OUT
{
	vec3 vert;
	vec2 uv;
	mat3 tbn;
} fsIn;

out vec4 fragColor;

layout (std140) uniform LightingInfo
{
	vec3 lightPos;
	vec3 lightColor;
	vec3 camLoc;
};

uniform sampler2D diffMap;
uniform sampler2D normMap;
uniform sampler2D specMap;

uniform vec3 texTint;

void main()
{
	vec3 norm = normalize(texture(normMap, fsIn.uv).xyz * 2.0 - 1.0);
	norm = normalize(fsIn.tbn * norm);

	vec3 l = normalize(lightPos - fsIn.vert);
	vec3 v = normalize(camLoc - fsIn.vert);
	vec3 h = normalize(l + v);

	vec3 amb = 0.1 * texture(diffMap, fsIn.uv).xyz;
	vec3 diff = clamp(dot(norm, l), 0.0, 1.0) * texture(diffMap, fsIn.uv).xyz;
	vec3 spec = pow(clamp(dot(norm, h), 0.0, 1.0), 1024) * texture(specMap, fsIn.uv).xyz;

	vec4 finalColor = vec4(lightColor * (amb + diff + spec), 1.0) * vec4(texTint, 1.0);

	fragColor = finalColor;
}
