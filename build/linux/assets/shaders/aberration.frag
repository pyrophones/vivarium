#version 450

in VS_OUT
{
	vec2 uv;
} fsIn;

out vec4 fragColor;

uniform sampler2D tex;

void main()
{
	float colorR = texture(tex, vec2(fsIn.uv.x, fsIn.uv.y - 0.002)).x;
	float colorG = texture(tex, vec2(fsIn.uv.x, fsIn.uv.y + 0.002)).y;
	float colorB = texture(tex, vec2(fsIn.uv.x, fsIn.uv.y - 0.002)).z;

	fragColor = vec4(colorR, colorG, colorB, 1.0);
}
