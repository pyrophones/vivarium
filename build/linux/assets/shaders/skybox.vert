#version 450 core

layout (location = 0) in vec3 position;

out VS_OUT
{
	vec3 uv;
} vsOut;

layout (std140) uniform CamMats
{
	mat4 view;
	mat4 proj;
};

void main()
{
	gl_Position = (proj * mat4(mat3(view)) * vec4(position, 1.0)).xyww;
	vsOut.uv = position;
}
