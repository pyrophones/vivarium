#version 450

out vec4 fragColor;

in VS_OUT
{
	vec2 uv;
} fsIn;

uniform sampler2D tex;

void main()
{
	fragColor = vec4(vec3(1.0 - texture(tex, fsIn.uv)), 1.0);
}
