#version 450

in VS_OUT
{
	vec2 uv;
} vsOut;

out vec4 fragColor;

uniform sampler2D tex;

const float THRES = 0.7;
const float FACTOR = 4.0;

void main()
{
	vec4 color = texture(tex, fsIn.uv);
	color.w = 1.0;

	float lum = dot(color.rgb, vec3(0.2126, 0.7152, 0.0722));

	color *= clamp(lum - THRES, 0.0, 1.0) * FACTOR;

	fragColor = color;
}
