#version 450

in VS_OUT
{
	vec2 uv;
} vsOut;

out vec4 fragColor;

uniform sampler2D tex;
uniform vec2 blurDir;

const float kernel[5] = float[](0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);

void main()
{
	vec2 texOffset = 1.0 / textureSize(tex, 0);
	texOffset *= blurDir;

	vec4 result = texture(tex, fsIn.uv) * kernel[0];
	for(int i = 1; i < 5; i++)
	{
		result += texture(tex, fsIn.uv + vec2(texOffset.x * i, texOffset.y * i)) * kernel[i];
		result += texture(tex, fsIn.uv - vec2(texOffset.x * i, texOffset.y * i)) * kernel[i];
	}

	fragColor = result;
}
