#version 450 core

layout (location = 0) in vec3 position;		//3D position coords
layout (location = 1) in vec2 uv;			//2D texture coords
layout (location = 2) in vec3 normal;		//3D normal coords
layout (location = 3) in vec3 tan;			//3D tangent vector
layout (location = 4) in vec3 bitan;		//3D bitangent vector
layout (location = 5) in uint matNum;		//The material number
layout (location = 6) in vec3 tint;			//The color of the mesh
layout (location = 7) in mat4 worldMat;		//4x4 world matrix
layout (location = 11) in mat3 normalMat;	//3x3 normal matrix

out VS_OUT
{
	layout (location = 0) flat uint matNum;
	layout (location = 1) vec3 pos;
	layout (location = 2) vec2 uv;
	layout (location = 3) vec3 tint;
	layout (location = 4) vec3 tan;
	layout (location = 5) vec3 bitan;
	layout (location = 6) vec3 normal;
	layout (location = 7) mat3 normalMat;
} vsOut;

layout (std140) uniform CamMats
{
	mat4 view;
	mat4 proj;
};

void main()
{
	vec4 worldPos = worldMat * vec4(position, 1.0);
	gl_Position = proj * view * worldPos;
	vsOut.pos = worldPos.xyz;

	vsOut.uv = uv;
	vsOut.normalMat = normalMat;

	vsOut.matNum = matNum;
	vsOut.tint = tint;
	vsOut.tan = tan;
	vsOut.bitan = bitan;
	vsOut.normal = normal;
}
