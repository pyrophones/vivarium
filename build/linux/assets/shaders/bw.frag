#version 450

in VS_OUT
{
	vec2 uv;
} fsIn;

out vec4 fragColor;

uniform sampler2D tex;

const vec3 COLOR_WEIGHTS = vec3(0.2126, 0.7152, 0.0722);

void main()
{
	vec3 color = texture(tex, fsIn.uv).rgb * COLOR_WEIGHTS;

	float newColor = (color.r + color.y + color.z) / 3.0;

	color = vec3(newColor);

	fragColor = vec4(color, 1.0);
}
