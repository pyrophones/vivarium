/*! \file windowData.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "windowData.h"

viv::Vec2 viv::WindowData::outerSize = Vec2();
viv::Vec2 viv::WindowData::innerSize = Vec2();
viv::Vec2 viv::WindowData::outerPos = Vec2();
viv::Vec2 viv::WindowData::innerPos = Vec2();
viv::Vec2 viv::WindowData::center = Vec2();
