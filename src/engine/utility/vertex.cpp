/*! \file mesh.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "vertex.h"

/*! \brief Vertex Constructor
 *
 * \param (const Vec3 &) vert - The vertex coordinates
 * \param (const Vec2 &) uv - The uv coordinates
 * \param (const Vec3 &) norm - The vertex normals
 * \param (const Vec3 &) tan - The tangent vector
 * \param (const Vec3 &) bitan - The bitangent vector
 *
 */
viv::Vertex::Vertex(const Vec3 &pos, const Vec2 &uv, const Vec3 &norm, const Vec3 &tan, const Vec3 &bitan)
{
	this->pos = pos;
	this->uv = uv;
	this->norm = norm;
	this->tan = tan;
	this->bitan = bitan;
}

/*! \brief Vertex copy constructor
 *
 * \param (const Vertex &) The vertex to copy
 */
viv::Vertex::Vertex(const Vertex &v)
{
	this->pos = v.pos;
	this->uv = v.uv;
	this->norm = v.norm;
	this->tan = v.tan;
	this->bitan = v.bitan;
}

/*! \brief Vertex copy assignment operator
 *
 * \param (const Vertex &) The vertex to copy
 *
 * \return (const Vertex &) This vertex after copying
 */
const viv::Vertex & viv::Vertex::operator=(const Vertex &v)
{
	this->pos = v.pos;
	this->uv = v.uv;
	this->norm = v.norm;
	this->tan = v.tan;
	this->bitan = v.bitan;

	return *this;
}

/*! \brief Vertex equality operator
 *
 * \param (const Vertex &) v - The vertex to compare with
 *
 * \return (bool) Whether the vertices are equal or not
 */
bool viv::Vertex::operator==(const Vertex &v)
{
	if(this->pos == v.pos && this->uv == v.uv && this->norm == v.norm)
		return true;

	return false;
}

/*! \brief Vertex inequality operator
 *
 * \param (const Vertex &) v - The vertex to compare with
 *
 * \return (bool) Whether the vertices are equal or not
 */
bool viv::Vertex::operator!=(const Vertex &v)
{
	return !(*this == v);
}

/*! \brief Vertex destructor
 */
viv::Vertex::~Vertex()
{

}
