/*! \file timer.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "timer.h"

float viv::Time::dt = 0.0f;
clock_t viv::Time::time;
clock_t viv::Time::curTime;
std::unique_ptr<viv::Time> viv::Time::instance = std::unique_ptr<viv::Time>(nullptr);

/*! \brief Time constructor
 */
viv::Time::Time()
{
	Time::curTime = clock();
}

/*! \brief Updates deltaTime
 */
void viv::Time::calcDeltaTime()
{
	Time::time = Time::curTime;
	Time::curTime = clock();
	Time::dt = (float)(Time::curTime - Time::time) / CLOCKS_PER_SEC;
}

/*! \brief Gets the Timer singleton instance. If there is no instance, one is created
 *
 */
viv::Time* viv::Time::getInstance()
{
	if(Time::instance ==  nullptr)
	{
		printf("Creating Time singleton . . . ");
		Time::instance = std::unique_ptr<Time>(new Time());
	}

	printf("ok\n");

	return Time::instance.get();
}

/*! \brief Gets deltaTime
 *
 * \return (float) - DeltaTime;
 */
float viv::Time::deltaTime()
{
	return Time::dt;
}

/*! \brief Deletes the Timer singleton
 */
void viv::Time::deleteInstance()
{
	Time::instance.reset();
}

/*! \brief Time destructor
 */
viv::Time::~Time()
{

}
