/*! \file uid.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "uid.h"

viv::UID::curUid = 1;

viv::UID::UID()
{

}

viv::UID::UID(const UID &id)
{
	this->uId = id.uId;
}

const viv::UID & viv::UID::operator=(const UID &id)
{
	this->uId = id.uId;

	return *this;
}

viv::UID::~UID()
{

}

void viv::UID::genUID(UID &id)
{
	id.uId = UID::curUid;
	UID::curUid++;
}
