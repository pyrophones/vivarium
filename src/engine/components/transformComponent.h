/*! \file transformComponent.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef TRANSFORMCOMPONENT_H
#define TRANSFORMCOMPONENT_H

#include <cstdint>
#include "gmath.h"
#include "component.h"

namespace viv
{
	class TransformComponent
	{
		public:
			const Vec3 & getFront();
			const Vec3 & getRight();
			const Vec3 & getUp();
			const Mat4 & getWorldMatrix();
			void setWorldMatrix(const Mat4 &worldMat);
			const Mat3 & getNormalMatrix();
			void setNormalMatrix(const Mat3 &normalMat);

		protected:
			Vec3 front; //! The front vector of the object
			Vec3 right; //! The right vector of the object
			Vec3 up; //! The up vector of the object
			Mat4 worldMat; //! The world matrix of the mesh
			Mat3 normalMat; //! The normal matrix of the mesh
	};
};

#endif

