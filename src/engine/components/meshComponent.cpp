/*! \file meshComponent.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2018-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "meshComponent.h"

std::shared_ptr<viv::Mesh> viv::MeshComponent::getMesh()
{
	return this->mesh;
}

void viv::MeshComponent::setMesh(std::shared_ptr<Mesh> mesh)
{
	this->mesh = std::shared_ptr<Mesh>(mesh);
	this->needsUpdate = true;
}

std::shared_ptr<viv::Shader> viv::MeshComponent::getShader()
{
	return this->shader;
}

void viv::MeshComponent::setShader(std::shared_ptr<Shader> shader)
{
	this->shader = std::shared_ptr<Shader>(shader);
	this->needsUpdate = true;
}