/*! \file transformComponent.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 **********************************
 ********************************************
 */

#include "transformComponent.h"

const viv::Vec3 & viv::TransformComponent::getFront()
{
	return this->front;
}

const viv::Vec3 & viv::TransformComponent::getRight()
{
	return this->right;
}

const viv::Vec3 & viv::TransformComponent::getUp()
{
	return this->up;
}

const viv::Mat4 & viv::TransformComponent::getWorldMatrix()
{
	return this->worldMat;
}

void viv::TransformComponent::setWorldMatrix(const Mat4 &worldMat)
{
	this->worldMat = worldMat;
	this->needsUpdate = true;
}

const viv::Mat3 & TransformComponent::getNormalMatrix()
{
	return normalMat;
}

void viv::TransformComponent::setNormalMatrix(const Mat3 &normalMat)
{
	this->normalMat = normalMat;
	this->needsUpdate = true;
}