/*! \file renderComponent.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2018-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef RENDERCOMPONENT_H
#define RENDERCOMPONENT_H

#include "component.h"
#include "assetManager.h"

namespace viv
{
	/*! \class RenderComponent
	 *  \brief Holds data for render components
	 */
	struct RenderComponent
	{
		public:
			std::shared_ptr<Material> getMaterial();
			void setMaterial(std::shared_ptr<Material> mat);

		private:
			std::shared_ptr<Material> material = std::shared_ptr<nullptr>;
			std::shared_ptr<Drawable> drawable = std::shared_ptr<nullptr>;
	};
};

#endif