/*! \file lightInstance.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "lightInstance.h"

/******************************
 * Directional Light Instance *
 ******************************/

/*! \brief Directional light constructor
 *
 * \param (std::shared_ptr<DirectionalLight>) l - The light to use for this instance
 * \param (const Vec3 &) pos - The position of the light
 * \param (const std::string) tag - The tag for the node
 */
viv::DirectionalLightInstance::DirectionalLightInstance(std::shared_ptr<DirectionalLight> l, const Vec3 &pos, std::string tag)
							  : Transform(pos, tag)
{
	this->light = std::shared_ptr<DirectionalLight>(l);
	this->light->addWorldAndNormalMat(IDENTITY_MAT4, IDENTITY_MAT3);
}

/*! \brief Directional light copy constructor
 *
 * \param (const DirectionalLightInstance &) dli - The directional light instance to copy
 */
viv::DirectionalLightInstance::DirectionalLightInstance(const DirectionalLightInstance &dli) : Transform(dli.getPos(), dli.tag)
{
	this->light = std::shared_ptr<DirectionalLight>(dli.light);
}

/*! \brief Updates the light
 */
void viv::DirectionalLightInstance::update()
{
	Transform::update();
	this->light->l.pos = this->getPos();
}

/*! \brief Directional light copy assignment operator
 *
 * \param (const DirectionalLightInstance &) dli - The directional light instance to copy
 *
 * \return (const DirectionalLightInstance &) This instance after copying
 */
const viv::DirectionalLightInstance & viv::DirectionalLightInstance::operator=(const DirectionalLightInstance &dli)
{
	this->light = std::shared_ptr<DirectionalLight>(dli.light);

	return *this;
}

/*! \brief Directional light destructor
 */
viv::DirectionalLightInstance::~DirectionalLightInstance()
{

}

/************************
 * Point Light Instance *
 ************************/

/*! \brief Point light constructor
 *
 * \param (std::shared_ptr<PointLight>) l - The light to use for this instance
 * \param (const Vec3 &) pos - The position of the light
 * \param (const std::string) tag - The tag for the node
 */
viv::PointLightInstance::PointLightInstance(std::shared_ptr<PointLight> l, const Vec3 &pos, const std::string tag) : Transform(pos, tag)
{
	this->light = std::shared_ptr<PointLight>(l);
	this->light->addWorldAndNormalMat(this->transMat, this->normalMat);
}

/*! \brief Point light copy constructor
 *
 * \param (const PointLightInstance &) dli - The point light instance to copy
 */
viv::PointLightInstance::PointLightInstance(const PointLightInstance &pli) : Transform(pli.getPos(), pli.tag)
{
	this->light = std::shared_ptr<PointLight>(pli.light);
}

/*! \brief Updates the light
 */
void viv::PointLightInstance::update()
{
	Transform::update();
	this->light->l.pos = this->getPos();
	this->setScale(Vec3(this->light->l.radius * 2.0f));
}

/*! \brief Point light copy assignment operator
 *
 * \param (const PointLightInstance &) dli - The point light instance to copy
 *
 * \return (const PointLightInstance &) This instance after copying
 */
const viv::PointLightInstance & viv::PointLightInstance::operator=(const PointLightInstance &pli)
{
	this->light = std::shared_ptr<PointLight>(pli.light);

	return *this;
}

/*! \brief Point light destructor
 */
viv::PointLightInstance::~PointLightInstance()
{

}

/***********************
 * Spot Light Instance *
 ***********************/

/*! \brief Spot light constructor
 *
 * \param (std::shared_ptr<SpotLight>) l - The light to use for this instance
 * \param (const Vec3 &) pos - The position of the light
 * \param (const std::string) tag - The tag for the node
 */
viv::SpotLightInstance::SpotLightInstance(std::shared_ptr<SpotLight> l, const Vec3 &pos, const std::string tag) : Transform(pos, tag)
{
	this->light = std::shared_ptr<SpotLight>(l);
	this->light->addWorldAndNormalMat(this->transMat, this->normalMat);
}

/*! \brief Spot light copy constructor
 *
 * \param (const SpotLightInstance &) dli - The spot light instance to copy
 */
viv::SpotLightInstance::SpotLightInstance(const SpotLightInstance &sli) : Transform(sli.getPos(), sli.tag)
{
	this->light = std::shared_ptr<SpotLight>(sli.light);
}

/*! \brief Updates the light
 */
void viv::SpotLightInstance::update()
{
	Transform::update();
	this->light->l.pos = this->getPos();
	this->light->lightDir = this->getRot() * this->getPos();
	this->setScale(Vec3(this->light->l.radius, this->light->range, this->light->l.radius));
}

/*! \brief Spot light copy assignment operator
 *
 * \param (const SpotLightInstance &) dli - The spot light instance to copy
 *
 * \return (const SpotLightInstance &) This instance after copying
 */
const viv::SpotLightInstance & viv::SpotLightInstance::operator=(const SpotLightInstance &sli)
{
	this->light = std::shared_ptr<SpotLight>(sli.light);

	return *this;
}

/*! \brief Spot light destructor
 */
viv::SpotLightInstance::~SpotLightInstance()
{

}
