/*! \file camera.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#include "camera.h"

/*! \brief Camera constructor. Inherits from Object
 *
 * \param (float) fov - The field of view of the camera
 * \param (float) aspect - The aspect ratio of the camera
 * \param (float) near - The near clipping plane of the camera
 * \param (float) far - The far clipping plane of the camera
 * \param (const Vec3 &) pos - The position of the camera
 * \param (std::string) tag - The tag for the camera
 */
viv::Camera::Camera(float fov, float aspect, float near, float far, const Vec3 &pos, std::string tag) : Transform(pos, tag)
{
	this->fov = fov;
	this->aspect = aspect;
	this->near = near;
	this->far = far;

	this->view = glm::lookAt(this->getPos(), this->getPos() + Vec3(0.0f, 0.0f, -1.0f), Vec3(0.0f, 1.0f, 0.0f));
	this->updateProjection();
	this->projViewInv = glm::inverse(this->proj * this->view);
}

/*! \brief Camera Copy Constructor
 *
 * \param (const Camera&) c - The Camera to copy
 */
viv::Camera::Camera(const Camera &c) : Transform(c)
{
	this->fov = c.fov;
	this->aspect = c.aspect;
	this->near = c.near;
	this->far = c.far;
	this->view = c.view;
	this->proj = c.proj;
}

/*! \brief Camera update method
 */
void viv::Camera::update()
{
	Transform::update();
	this->view = glm::lookAt(this->getPos(), this->getPos() + this->getFront(), this->getUp());
	this->projViewInv = glm::inverse(this->proj * this->view);
}

/*! \brief Returns the camera's view matrix
 *
 * \return (const Mat4 &) The view matrix
 */
const viv::Mat4 & viv::Camera::getViewMat() const
{
	return this->view;
}

/*! \brief Returns the camera's projection matrix
 *
 * \return (const Mat4 &) The projection matrix
 */
const viv::Mat4 & viv::Camera::getProjMat() const
{
	return this->proj;
}

/*! \brief Returns the camera's inverted view * projection matrix
 *
 * \return (const Mat4 &) The inverted view * projection matrix
 */
const viv::Mat4 & viv::Camera::getProjViewInvMat() const
{
	return this->projViewInv;
}

/*! \brief Returns the camera's near clipping plane
 *
 * \return (const float &) The camera's near clipping plane
 */
const float & viv::Camera::getNear() const
{
	return this->near;
}

/*! \brief Returns the camera's far clipping plane
 *
 * \return (const float &) The camera's far clipping plane
 */
const float & viv::Camera::getFar() const
{
	return this->far;
}

void viv::Camera::setAspectRatio(float aspect)
{
	this->aspect = aspect;
	this->updateProjection();
}

/*! \brief udpates the projection matrix
 */
void viv::Camera::updateProjection()
{
	this->proj = glm::perspective(this->fov * Math::TO_RAD, this->aspect, this->near, this->far);
}

/*! \brief Camera copy-assignment operator
 *
 * \param (const Camera &) c - The camera to copy from
 *
 * \return (const Camera &) This camera after copying
 */
const viv::Camera & viv::Camera::operator=(const Camera &c)
{
	this->fov = c.fov;
	this->aspect = c.aspect;
	this->near = c.near;
	this->far = c.far;
	this->view = c.view;
	this->proj = c.proj;

	return *this;
}

/*! \brief Camera destructor.
 */
viv::Camera::~Camera()
{

}
