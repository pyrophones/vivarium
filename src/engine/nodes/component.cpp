/*! \file component.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "component.h"

viv::Component::Component()
{

}

viv::Component::Component(const Component &c)
{

}

const viv::Component & viv::Component::operator=(const Component &c)
{
	return *this;
}

viv::Component::~Component()
{

}
