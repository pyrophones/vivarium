/*! \file transform.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#include "transform.h"

/*! \brief Transform Constructor. Sets the position to the vec3 value
 *
 * \param (Vec3) pos - The vector used to set the position
*/
viv::Transform::Transform(const Vec3 &pos, const std::string tag) : Node(tag)
{
	this->pos = pos;
	this->rot = Quat(1.0f, Vec3());
	this->scale = Vec3(1.0f, 1.0f, 1.0f);
	this->isDirty = true;
}

viv::Transform::Transform(const Mat4 &trans)
{
	this->transMat = trans;
	Vec3 skew;
	Vec4 pers;
	glm::decompose(trans, this->scale, this->rot, this->pos, skew, pers);
}

/*! \brief Transform Copy Constructor
 *
 * \param (const Transform &) t - The Transform to copy
 */
viv::Transform::Transform(const Transform& t) : Node(t)
{
	this->pos = t.pos;
	this->rot = t.rot;
	this->scale = t.scale;
	this->front = t.front;
	this->right = t.right;
	this->up = t.up;
	this->isDirty = true;
}

/*! \brief Method used to update the transforms rotation matrix and front, right, and up vectors
 */
void viv::Transform::update()
{
	Node::update();

	//if(this->isDirty) {
		if(this->rot.length() > 1.0f)
			this->rot = glm::normalize(this->rot);

		Mat4 trans = glm::translate(Mat4(1.0f), this->pos);
		Mat4 rot = glm::mat4_cast(this->rot);
		Mat4 scale = glm::scale(Mat4(1.0f), this->scale);
		this->transMat = trans * rot * scale;
		this->front = Mat3(this->transMat) * Vec3(0.0f, 0.0f, -1.0f);
		this->right = glm::normalize(glm::cross(this->front, Mat3(this->transMat) * Vec3(0.0f, 1.0f, 0.0f)));
		this->up = glm::normalize(glm::cross(this->front, this->right));

		if(this->parent != nullptr)
			this->transMat *= static_cast<Transform*>(this->parent)->transMat;
		this->normalMat = glm::inverseTranspose((Mat3)this->transMat);

		this->isDirty = false;
	//}
}

/*! \brief Transform assigment operator
 *
 * \param (const Transform &) t - The Transform to copy
 *
 * \return (Transform &) - This instance
 */
const viv::Transform & viv::Transform::operator=(const Transform &t)
{
	Node::operator=(t);

	this->pos = t.pos;
	this->rot = t.rot;
	this->scale = t.scale;
	this->front = t.front;
	this->right = t.right;
	this->up = t.up;

	return *this;
}

/*! \brief Gets the position of the object
 *
 * \return (const Vec3 &) The position vector
 */
const viv::Vec3 & viv::Transform::getPos() const
{
	return this->pos;
}

/*! \brief Sets the position of the object
 *
 * \param (const Vec3 &) pos - The new position vector
 */
void viv::Transform::setPos(const Vec3 &pos)
{
	this->pos = pos;
	this->isDirty = true;
}

/*! \brief Gets the scale of the object
 *
 * \return (const Vec3 &) The scale vector
 */
const viv::Vec3 & viv::Transform::getScale() const
{
	return this->scale;
}

/*! \brief Sets the scale of the object
 *
 * \param (const Vec3 &) scale - The new scale vector
 */
void viv::Transform::setScale(const Vec3 &scale)
{
	this->scale = scale;
	this->isDirty = true;
}

/*! \brief Gets the rotation of the object
 *
 * \return (const Quat &) The rotation vector
 */
const viv::Quat & viv::Transform::getRot() const
{
	return this->rot;
}

/*! \brief Sets the rotation of the object
 *
 * \param (const Quat &) rot - The new rotation vector
 */
void viv::Transform::setRot(const Quat &rot)
{
	this->rot = rot;
	this->isDirty = true;
}

/*! \briefUsed to get the transforms front vector
 *
 * \return (const Vec3 &) The front vector of the transform
 */
const viv::Vec3 & viv::Transform::getFront() const
{
	return this->front;
}

/*! \brief Used to get the transforms right vector
 *
 * \return (const Vec3 &) The right vector of the transform
 */
const viv::Vec3 & viv::Transform::getRight() const
{
	return this->right;
}

/*! Used to get the transforms up vector
 *
 * \return (const Vec3 &) The up vector of the transform
 */
const viv::Vec3 & viv::Transform::getUp() const
{
	return this->up;
}

/*! \brief Destructor
 */
viv::Transform::~Transform()
{

}

/*! \brief AngelScript default constructor binding
 *
 * \param (Transform*) self - Pointer to the transform
 */
void viv::Transform::DefCtor(Transform* self)
{
	new (self) Transform();
}

/*! \brief AngelScript copy constructor binding
 *
 * \param (const Transform&) other - The other transform to copy
 * \param (Transform*) self - Pointer to the transform
 */
void viv::Transform::CopyCtor(const Transform &other, Transform* self)
{
	new (self) Transform(other);
}

/*! \brief AngelScript init constructor binding
 *
 * \param (const Vec3 &) pos - The default pos
 * \param (std::string) tag - The tag of the node
 * \param (Transform*) self - Pointer to the transform
 */
void viv::Transform::InitCtor(const Vec3 &pos, std::string tag, Transform* self)
{
	new (self) Transform(pos, tag);
}

/*! \brief AngelScript destructor
 *
 * \param (Transform*) self - Pointer to the memory to deallocate
 */
void viv::Transform::Dtor(Transform* self)
{
	self->~Transform();
}
