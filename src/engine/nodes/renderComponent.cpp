/*! \file renderComponent.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "renderComponent.h"

viv::RenderComponent::RenderComponent() : Component()
{

}

viv::RenderComponent::RenderComponent(const RenderComponent &r) : Component(r)
{

}

const viv::RenderComponent & viv::RenderComponent::operator=(const RenderComponent &r)
{
	Component::operator=(r);

	return *this;
}

viv::RenderComponent::~RenderComponent()
{

}
