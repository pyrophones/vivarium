/*! \file node.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#include "node.h"

/*! \brief Node constructor
 *
 * \param (const std::string) tag - The tag of the Node
 * \param (char*) scriptPath - The path to script
 */
viv::Node::Node(const std::string tag)
{
	this->tag = tag;
	this->subNodeCount = 0;
	transMat = Mat4(1.0f);
	normalMat = Mat4(1.0f);
}

/*! \brief Node copy constructor
 *
 * \param (const Node&) n - The Node to copy
 */
viv::Node::Node(const Node &n)
{
	this->tag = n.tag;
	this->subNodes.clear();

	for (auto i = n.subNodes.begin(); i != n.subNodes.end(); i++) {
		this->subNodes[i->first] = std::unique_ptr<Node>(new Node(*i->second));
	}

	this->subNodeCount = n.subNodeCount;
}

/*! \brief Adds a subNode to this Node
 *
 * \param (std::unique_ptr<Node>) n - The Node to make a child Node
 */
viv::Node* viv::Node::addSubNode(std::unique_ptr<Node> n)
{
	n.get()->parent = this;

	if(this->subNodes[n.get()->tag] != nullptr) {
		uint32_t i = 1;
		std::string tmp(n.get()->tag);
		tmp += i;
		while(this->subNodes[tmp] != nullptr) {
			i++;
			tmp = n.get()->tag;
			tmp += i;
		}
		n.get()->tag += i;
	}

	std::string tag = n.get()->tag;
	this->subNodes[tag] = std::move(n);
	this->subNodeCount++;
	return this->subNodes[tag].get();
}

/*! \brief Update method
 */
void viv::Node::update()
{
	for(auto it = this->subNodes.begin(); it != this->subNodes.end(); it++)
		it->second.get()->update();
}

/*! \brief Gets the parent node of this Node
 */
viv::Node* viv::Node::getParent()
{
	return this->parent;
}

/*! \brief Gets the child node with corresponding tag
 *
 * \param (std::string) tag - The tag of the child Node to find
 *
 * \return (Node*) The child Node if found
 */
viv::Node* viv::Node::getChildByTag(std::string tag)
{
	return this->subNodes[tag].get();
}

/*! \brief Gets the number of subNodes
 *
 * \return (uint32_t) The subNode count
 */
uint32_t viv::Node::getSubNodeCount()
{
	return this->subNodeCount;
}

/*! \brief Get's the tag of this Node
 *
 * \return (std::string) This Node's tag
 */
std::string viv::Node::getTag()
{
	return this->tag;
}

/*! \brief Node assignment operator
 *
 * \param (const Node &) n - The Node to copy
 *
 * \return (Node &) n - This instance
 */
const viv::Node & viv::Node::operator=(const Node &n)
{
	this->tag = n.tag;
	this->subNodes.clear();

	for (auto i = n.subNodes.begin(); i != n.subNodes.end(); i++) {
		this->subNodes[i->first] = std::unique_ptr<Node>(new Node(*i->second));
	}

	this->subNodeCount = n.subNodeCount;

	return *this;
}

/*! \brief Node destructor
 */
viv::Node::~Node()
{
	this->subNodes.clear();
}


/*! \brief AngelScript default constructor binding
 *
 * \param (Node*) self - Pointer to the node
 */
void viv::Node::DefCtor(Node* self)
{
	new (self) Node();
}

/*! \brief AngelScript copy constructor binding
 *
 * \param (const Node&) other - The other node to copy
 * \param (Node*) self - Pointer to the node
 */
void viv::Node::CopyCtor(const Node &other, Node* self)
{
	new (self) Node(other);
}

/*! \brief AngelScript init constructor binding
 *
 * \param (std::string) tag - The tag of the node
 * \param (Node*) self - Pointer to the node
 */
void viv::Node::InitCtor(std::string tag, Node* self)
{
	new (self) Node(tag);
}

/*! \brief AngelScript destructor
 *
 * \param (Node*) self - Pointer to the memory to deallocate
 */
void viv::Node::Dtor(Node* self)
{
	self->~Node();
}
