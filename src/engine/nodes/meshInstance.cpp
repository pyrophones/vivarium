/*! \file meshInstance.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#include "meshInstance.h"

/*! \brief MeshInstance constructor
 *
 * \param (std::shared_ptr<Mesh>) m - The mesh for this instance to use
 * \param (const Vec3 &) pos - The position of the instance in the world
 * \param (const std::string) tag - The tag of the instance
 */
viv::MeshInstance::MeshInstance(std::shared_ptr<Mesh> m, const Vec3 &pos, const std::string tag)
								: Transform(pos, tag)
{
	this->mesh = std::shared_ptr<Mesh>(m);
	this->mesh->addWorldAndNormalMat(this->transMat, this->normalMat);
	this->mesh->addObjectTint(this->tint);
}

/*! \brief MeshInstance copy constructor
 *
 * \param (const MeshInstance &) mi - The instance to copy
 */
viv::MeshInstance::MeshInstance(const MeshInstance &mi) : Transform(mi.getPos(), mi.tag)
{
	this->mesh = std::shared_ptr<Mesh>(mi.mesh);
	this->tint = mi.tint;
	this->mesh->addWorldAndNormalMat(this->transMat, this->normalMat);
	this->mesh->addObjectTint(this->tint);
}

/*! \brief Sets this instances material
 *
 * \param (std::shared_ptr<Material>) mat - The pointer to the material to use for this instance
 */
void viv::MeshInstance::setMaterial(std::shared_ptr<Material> mat)
{
	this->material = mat;
	this->mesh->addMaterial(this->material);
}

/*! \brief Sets this instances tint
 *
 * \param (const Vec3 &) tint - The tint of this instance
 */
void viv::MeshInstance::setTint(const Vec3 &tint)
{
	this->tint = tint;
}

/*! \brief Updates the MeshInstance
 */
void viv::MeshInstance::update()
{
	Transform::update();
}

/*! \brief MeshInstance copy-assignment operator
 *
 * \param (const MeshInstance &) mi - The instance to copy
 */
const viv::MeshInstance & viv::MeshInstance::operator=(const MeshInstance &mi)
{
	this->mesh = std::shared_ptr<Mesh>(mi.mesh);
	this->mesh->addWorldAndNormalMat(this->transMat, this->normalMat);

	return *this;
}

/*! \brief MeshInstance destructor
 */
viv::MeshInstance::~MeshInstance()
{

}
