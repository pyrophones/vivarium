/*! \file renderingSystem.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef RENDERINGSYSTEM_H
#define RENDERINGSYSTEM_H

#include <string>
#include <vector>
#include <memory>
#include <new>
#include <typeinfo>
#include <cstdint>
#include <cstring>
#include "system.h"

namespace viv
{
	/*! \class RenderingSystem
	 *  \brief Class for the rendering component systems
	 */
	class RenderingSystem : public System
	{
		public:
			RenderingSystem();
			RenderingSystem(const RenderingSystem &s);
			const RenderingSystem & operator=(const RenderingSystem &s);
			virtual void receiveMessage() override;
			virtual void postMessage() override;
			virtual void update() override;
			virtual ~RenderingSystem();

			//Static methods for as bindings
			//static void DefCtor(Entity* self);
			//static void CopyCtor(const Entity &other, Entity* self);
			//static void InitCtor(std::string tag, char* scriptPath, Entity* self);
			//static void Dtor(Entity* self);

		private:
			//bool renderSort();
			static std::vector<uint32_t> componentsToRender;
			static std::unique_ptr<RenderingSystem> instance;
	};
}

#endif
