/*! \file system.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "system.h"


viv::System::System()
{
	this->components.reserve(MAX_COMPONENTS);
	this->componentRefs.reserve(MAX_COMPONENTS);
}

viv::System::System(const System &s)
{
	this->components = s.components;
	this->componentRefs = s.componentRefs;
}

const viv::System & viv::System::operator=(const System &s)
{
	this->components = s.components;
	this->componentRefs = s.componentRefs;

	return *this;
}

viv::System::~System()
{

}
