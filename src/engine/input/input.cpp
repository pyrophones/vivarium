/*! \file input.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "input.h"
#include "timer.h"

viv::Vec2 viv::Input::MouseData::curMousePosition = viv::Vec2();
viv::Vec2 viv::Input::MouseData::prevMousePosition = viv::Vec2();
bool viv::Input::mouseLocked = false;
std::unique_ptr<viv::Input> viv::Input::instance = std::unique_ptr<viv::Input>(nullptr);
std::map<Qt::Key, viv::Input::KeyData> viv::Input::keyStates;
std::map<Qt::MouseButton, viv::Input::MouseData> viv::Input::mouseStates;

/*! \brief Input Default Constructor
 */
viv::Input::Input()
{

}

/*! \brief Gets the instance of the mesh singleton
 *
 * \return (viv::Input*) The instance of the input singleton
 */
viv::Input* viv::Input::getInstance()
{
	//Create the input instance if it doesn't exist
	if(Input::instance == nullptr) {
		printf("Creating Input singleton . . . ");
		Input::instance = std::unique_ptr<Input>(new Input);
	}

	return Input::instance.get();
}

/*! \brief Deletes the instance of the Input singleton
 */
void viv::Input::deleteInstance()
{
	if(Input::instance != nullptr)
		Input::instance.reset();
}

/*! \brief Records a key press event
 *
 * \param (QKeyEvent*) event - The key event to record
 */
void viv::Input::keyPressEvent(QKeyEvent* event)
{
	//Check if the key exists already in the map
	if(Input::keyStates.find((Qt::Key)event->key()) != Input::keyStates.end()) {
		Input::KeyData* kd = &Input::keyStates[(Qt::Key)event->key()];

		//If the key press was repeated, set the time and data
		if(event->isAutoRepeat()) {
			kd->isPressed = false;
			kd->isActive = true;
			kd->pressDuration += Time::deltaTime();
		}

		//If it was not repeated, it was just pressed
		else {
			kd->isPressed = true;
			kd->isActive = true;
			kd->isReleased = false;
		}
	}

	//If the key is not in the map, add it to the map and set it's data
	else
		Input::keyStates[(Qt::Key)event->key()] = Input::KeyData{true, true, false};
}

/*! \brief Records a key release event
 *
 * \param (QKeyEvent*) event - The key event to record
 */
void viv::Input::keyReleaseEvent(QKeyEvent* event)
{
	//Ignore auto-repeats
	if(event->isAutoRepeat())
		event->ignore();

	//If it's not auto-repeated, sets the data appropriately
	else {
		Input::KeyData* kd = &Input::keyStates[(Qt::Key)event->key()];
		kd->isPressed = false;
		kd->isActive = false;
		kd->isReleased = true;
	}
}

/*! \brief Checks if a key is down
 *
 * \param (Qt::Key) key - The key to check for
 *
 * \return (bool) If the key is down or not
 */
bool viv::Input::isKeyDown(Qt::Key key)
{
	return Input::keyStates[key].isPressed;
}

/*! \brief Checks if a key is activated
 *
 * \param (Qt::Key) key - The key to check for
 *
 * \return (bool) If the key is active or not
 */
bool viv::Input::isKeyActive(Qt::Key key)
{
	return Input::keyStates[key].isActive;
}

/*! \brief Checks if a key is up
 *
 * \param (Qt::Key) key - The key to check for
 *
 * \return (bool) If the key is up or not
 */
bool viv::Input::isKeyUp(Qt::Key key)
{
	return Input::keyStates[key].isReleased;
}

/*! \brief Gets the duration of a key press
 *
 * \param (Qt::Key) key - The key to check for
 *
 * \return (float) The duration of the key press
 */
float viv::Input::getKeyPressDuration(Qt::Key key)
{
	return Input::keyStates[key].pressDuration;
}

/*! \brief Records a mouse move event
 *
 * \param (QMouseEvent*) event - The mouse event to record
 */
void viv::Input::mouseMoveEvent(QMouseEvent* event)
{
	Input::MouseData::curMousePosition = Vec2(event->globalPos().x(), event->globalPos().y());

	//If mouseLocked is true, do some calculations to check where it is on the screen and if it should be reset
	if(Input::mouseLocked) {
		if(glm::distance(Input::MouseData::curMousePosition, WindowData::center) > 10) {
			QCursor::setPos(WindowData::center.x, WindowData::center.y);
			Input::MouseData::prevMousePosition = (WindowData::center - Input::MouseData::curMousePosition) + WindowData::center;
			Input::MouseData::curMousePosition = WindowData::center;
		}
	}
}

/*! \brief Records a mouse press event
 *
 * \param (QMouseEvent*) event - The mouse event to record
 */
void viv::Input::mousePressEvent(QMouseEvent* event)
{
	if(Input::mouseStates.find((Qt::MouseButton)event->button()) != Input::mouseStates.end()) {
		Input::MouseData* md = &Input::mouseStates[(Qt::MouseButton)event->button()];

		md->isPressed = true;
		md->isActive = true;
		md->isReleased = false;
	}

	else
		Input::mouseStates[(Qt::MouseButton)event->button()] = Input::MouseData{true, true, false};
}

/*! \brief Records a mouse release event
 *
 * \param (QMouseEvent*) event - The mouse event to record
 */
void viv::Input::mouseReleaseEvent(QMouseEvent* event)
{
	Input::MouseData* md = &Input::mouseStates[(Qt::MouseButton)event->button()];
	md->isPressed = false;
	md->isActive = false;
	md->isReleased = true;
}

/*! \brief Records a mouse leave event
 */
void viv::Input::mouseLeave()
{
	//TODO: Logic to pause game updating
}

/*! \brief Checks if a mouse button is down
 *
 * \param (Qt::MouseButton) btn - The mouse button to check for
 *
 * \return (bool) If the mouse button is down or not
 */
bool viv::Input::isMouseButtonDown(Qt::MouseButton btn)
{
	return Input::mouseStates[btn].isPressed;
}

/*! \brief Checks if a mouse button is activated
 *
 * \param (Qt::MouseButton) btn - The mouse button to check for
 *
 * \return (bool) If the mouse button is active or not
 */
bool viv::Input::isMouseButtonActive(Qt::MouseButton btn)
{
	return Input::mouseStates[btn].isActive;
}

/*! \brief Checks if a mouse button was released
 *
 * \param (Qt::MouseButton) btn - The mouse button to check for
 *
 * \return (bool) If the mouse button was released or not
 */
bool viv::Input::isMouseButtonUp(Qt::MouseButton btn)
{
	return Input::mouseStates[btn].isReleased;
}

/*! \brief Gets the duration of a mouse button press
 *
 * \param (Qt:MouseButton) btn - The mouse button to check for
 *
 * \return (float) The duration of the mouse button press
 */
float viv::Input::getMouseButtonPressDuration(Qt::MouseButton btn)
{
	return Input::mouseStates[btn].pressDuration;
}

/*! \brief Gets the current mouse position
 *
 * \return (const Vec2 &) The current position of the mouse
 */
const viv::Vec2 & viv::Input::getCurrentMousePosition()
{
	return Input::MouseData::curMousePosition;
}

/*! \brief Sets the mouse position
 *
 * \param (const Vec2 &) newPos - The new mouse position
 */
void viv::Input::setMousePosition(const Vec2 &newPos)
{
	QCursor::setPos(newPos.x, newPos.y);
	Input::MouseData::curMousePosition = newPos;
	Input::MouseData::prevMousePosition = newPos;
}

/*! \brief Gets the previous mouse position
 *
 * \return (const Vec2 &) The previous position of the mouse
 */
const viv::Vec2 & viv::Input::getPrevMousePosition()
{
	return Input::MouseData::prevMousePosition;
}

/*! \brief Toggles to disable / enable the mouse cursor
 *
 * \param (bool) disabled - If the cursor should be disabled or not
 */
void viv::Input::disableCursor(bool disabled)
{
	if(disabled)
		QApplication::setOverrideCursor(QCursor(Qt::BlankCursor));
	else
		QApplication::setOverrideCursor(QCursor(Qt::ArrowCursor));
}

/*! \brief Toggle to lock the mouse
 *
 * \param (bool) locked - If the mouse should be locked
 */
void viv::Input::lockMouse(bool locked)
{
	Input::mouseLocked = locked;
}

/*! \brief Cleans-up and resets variables relating to the different keystates
 */
void viv::Input::cleanStates()
{
	for(auto i = Input::keyStates.begin(); i != Input::keyStates.end(); i++) {
		Input::KeyData* kd = &i->second;

		if(kd->isReleased) {
			kd->isReleased = false;
			kd->pressDuration = 0.0f;
		}
	}

	for(auto i = Input::mouseStates.begin(); i != Input::mouseStates.end(); i++) {
		Input::MouseData* md = &i->second;

		if(md->isPressed) {
			md->isPressed = false;
			md->isActive = true;
		}

		else if(md->isActive)
			md->pressDuration += Time::deltaTime();

		else if(md->isReleased) {
			md->isReleased = false;
			md->pressDuration = 0.0f;
		}
	}

	Input::MouseData::prevMousePosition = Input::MouseData::curMousePosition;
}

/*! \brief Input Destructor
 */
viv::Input::~Input()
{

}

///*! Used for the GLFW char callback
// *
// * \param (GLFWwindow*) window - The window variable to pass in
// * \param (unsigned int) codepoint - The codepoint of the key
// */
//void viv::Input::charCallback(GLFWwindow* window, unsigned int codepoint)
//{
//	input(codepoint);
//}
