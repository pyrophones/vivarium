/*! \file console.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2017 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#include "console.h"

/*! Command constructor
 *
 * \param (const char*) text - The string to create the command message
*/
viv::Command::Command(const char* text)
{
	this->msg = (char*) malloc(sizeof(text));

	for(uint32_t i = 0; i < strlen(text); i++) {
		this->msg[i] = text[i];
	}
}

/*! Gets the command message
 *
 * \return (const char*) The message of the command
*/
const char* viv::Command::getMsg()
{
	return (const char*) this->msg;
}

/*! Frees memory allocated by the message
*/
void viv::Command::freeMem()
{
	free(msg);
}

/*! Logs the current command
 *
 * \param (const char*) cmd - The string command to be logged
*/
void viv::log(const char* cmd)
{
	if(commandLog.size() == 50) {
		commandLog[0].freeMem();
		commandLog.pop_front();
	}

	commandLog.push_back(Command(cmd));
}

/*! Draws the console
*/
void viv::drawConsole()
{
	size_t cLogSize = commandLog.size();

	if(cLogSize > 0) {
		int32_t i = cLogSize - 4;
		const uint32_t subNum = cLogSize - 5;

		if(i < 0)
			i = 0;

		for(; (uint32_t) i < cLogSize; i++) {
			int32_t screenDiff = (i - subNum) * 20;
			//drawString(commandLog[i].getMsg(), glm::vec2(1.0, System::getHeight() - screenDiff), glm::vec3(1.0f, 1.0f, 1.0f), cColor);
		}
	}

	//if(currentInput.length() != 0)
		//drawString(currentInput.c_str(), glm::vec2(1.0, System::getHeight() - 100), glm::vec3(1.0f, 1.0f, 1.0f), cColor);
}

/*! Convertes key input characters in current input
 *
 * \param (unsigned int) codepoint - The codepoint to be converted to a char
*/
void viv::input(uint32_t codepoint)
{
	char convChar = (char) codepoint;
	if(currentInput.length() == 0)
		currentInput = convChar;

	else
		currentInput += convChar;
}

/*! Deletes the last character from the player input
*/
void viv::delLastChar()
{
	if(currentInput.size() > 0)
		currentInput.pop_back();
}

/*! Sends the current input to the terminal
*/
void viv::sendInput()
{
	inputMarker = -1;
	if(currentInput.length() != 0) {
		log(currentInput.c_str());
		currentInput.clear();
	}
}

/*! Selects the previous input in the consoles log
*/
void viv::prevInput()
{
	if(inputMarker == -1) {
		saveInput = currentInput;
	}

	inputMarker++;

	if((uint32_t) inputMarker >= commandLog.size())
		inputMarker = commandLog.size() - 1;

	currentInput = commandLog[(commandLog.size() - 1) - inputMarker].getMsg();
}

/*! Selects the next input in the consoles log
*/
void viv::nextInput()
{
	inputMarker--;
	if(inputMarker < -1)
		inputMarker = -1;

	if(inputMarker == -1) {
		currentInput = saveInput;
	}

	else {
		currentInput = commandLog[(commandLog.size() - 1) - inputMarker].getMsg();
	}
}

void viv::lastChar()
{
	//
}
