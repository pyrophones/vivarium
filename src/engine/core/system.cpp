/*! \file system.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "system.h"

BEGIN_AS_NAMESPACE

std::unique_ptr<viv::System> viv::System::instance = std::unique_ptr<viv::System>(nullptr);
bool viv::System::shouldClose = false;
viv::System::RendererCode viv::System::rendererCode = viv::System::RendererCode::OGL;
viv::RenderBackend* viv::System::renderBackend = nullptr;
viv::Renderer* viv::System::renderer = nullptr;
asIScriptEngine* viv::System::asEngine = nullptr;
FT_Library viv::System::lib;
FT_Face viv::System::face;

/*! \brief System contrutor
 */
viv::System::System()
{

}

/*! \brief Initializes the system
 *
 * \param (QWidget*) window - Pointer to the main window widget
 *
 * \return (int32_t) The methods success variable
 */
int32_t viv::System::init(QMainWindow* mainWindow)
{
	System::getInstance();

	printf("Initializing FreeType . . . ");
	//Check if FreeType can initialize
	if(FT_Init_FreeType(&lib)) {
		printf("Failed to initialize FreeType!\n");
		return -1;
	}

	//Check if the font library can be loaded
	if(FT_New_Face(lib, "/usr/share/fonts/truetype/Roboto-Medium.ttf", 0, &face)) {
		printf("Failed to load font!\n");
		return -1;
	}

	FT_Set_Pixel_Sizes(face, 0, 24);

	//Test if a char can be loaded
	if(FT_Load_Char(face, 'x', FT_LOAD_RENDER)) {
		printf("Failed to load Glyph!\n");
		return -1;
	}

	printf("ok\n");

	System::renderer = new Renderer((QWidget*)mainWindow);
	mainWindow->setCentralWidget(System::renderer);

	printf("Starting graphics library . . .\n");
	switch(System::rendererCode) {
		case System::RendererCode::VKN:
			return -1;
			//System::renderBackend = new VKRenderBackend();

			//if(System::renderBackend->init(System::renderer->winId()) == 0) {
			//	printf("Failed to start GL context!\n");
			//	delete System::renderBackend;
			//	return -1;
			//	break;
			//}
			printf("VK context successfully started\n");
			break;

		case System::RendererCode::DX11:
			return -1;
			printf("DX11 context successfully started\n");
			break;

		case System::RendererCode::OGL:
		default:
			System::renderBackend = new GLRenderBackend();

			if(System::renderBackend->init(System::renderer->winId())) {
				printf("Failed to start GL context!\n");
				delete System::renderBackend;
				return -1;
			}

			printf("GL context successfully started\n");
			break;

	}

	Game::getInstance();
	Input::getInstance();
	AssetManager::getInstance();
	AssetManager::init(System::renderBackend);

	//Start AngelScipt vm
	System::asEngine = asCreateScriptEngine();

	if(System::asEngine == nullptr)
		return -1;

	if(asEngine->SetMessageCallback(asFUNCTION(asMessageCallback), 0, asCALL_CDECL) > 0)
		return -1;

	System::registerASClasses();
	printf("Loading assets . . . \n");
	Game::loadAssets();
	System::renderer->init(System::renderBackend);

	return 0;
}

/*! \brief Runs the main system
 *
 * \return (int32_t) The methods success variable
 */
int32_t viv::System::run()
{
	Time::getInstance();
	printf("Running main loop . . .\n");
	Game::start(System::renderBackend);

/*	Vec3 gah(3.0f, 4.6f, 2.3f);
	Vec3 hah(6.2f, 5.1f, 4.4f);
	Vec3 res;

	float beg = glfwGetTime();
	Vec jah(gah);
	Vec kah(hah);
	jah += kah;
	Vec::store(&res, jah);
	float end = glfwGetTime();
	std::cout << end - beg << ": " << res.x << ", " << res.y << ", " << res.z << std::endl;

	beg = glfwGetTime();
	gah += hah;
	end = glfwGetTime();
	std::cout << end - beg << ": " << gah.x << ", " << gah.y << ", " << gah.z << std::endl;

	Vec tah;
	beg = glfwGetTime();
	tah = jah * kah;
	tah += kah - jah;
	Vec::store(&res, tah);
	end = glfwGetTime();
	std::cout << end - beg << ": " << res.x << ", " << res.y << ", " << res.z << std::endl;
	Vec3 dah;
	beg = glfwGetTime();
	dah = gah * hah;
	dah += hah - gah;
	end = glfwGetTime();
	std::cout << end - beg << ": " << dah.x << ", " << dah.y << ", " << dah.z << std::endl;

	float dp;
	Vec::store(&res, jah);
	beg = glfwGetTime();
	tah = Vec::dot(kah, jah);
	Vec::store(&res, tah);
	end = glfwGetTime();
	std::cout << end - beg << ": "<< res.z << std::endl;
	beg = glfwGetTime();
	dp = Vec3::dot(gah, hah);
	end = glfwGetTime();
	std::cout << end - beg << ": "<< dp << std::endl;*/

	while(!System::shouldClose) {
		Time::calcDeltaTime();

		if(Game::getCurState() == Game::GameState::INGAME) {
			Game::update();
			System::renderer->update();
		}

		//Console::drawConsole(); //Draw the console
		//glFlush();
		System::asEngine->GarbageCollect(asGC_ONE_STEP);
		Input::cleanStates();
		QApplication::processEvents();
	}

	printf("Exited Main Loop\n");

	return 0;
}

/*! \brief Deletes the instance and cleans up other systems
 *
 * \return (int32_t) The methods success variable
 */
int32_t viv::System::end()
{
	//Cleanup
	FT_Done_Face(face);
	FT_Done_FreeType(lib);

	Time::deleteInstance();
	AssetManager::deleteInstance();
	Input::deleteInstance();
	Game::deleteInstance();

	delete System::renderBackend;
	System::asEngine->ShutDownAndRelease();
	delete System::renderer;
	System::instance.reset();

	return 0;
}

/*! \brief Gets the System singleton instance
 *
 * \return (System) The current System Instance
 */
viv::System* viv::System::getInstance()
{
	if(System::instance == nullptr) {
		printf("Creating System singleton . . .\n");
		System::instance = std::unique_ptr<System>(new System());
	}

	return System::instance.get();
}

/*! \brief Gets the AngelScript engine context
 *
 * \return asIScriptEngine - The current AngelScript engine
 */
asIScriptEngine* viv::System::getEngine()
{
	return System::asEngine;
}

/*! \brief Binds the modules and classes that will be used in angelscript files
 */
void viv::System::registerASClasses()
{
	//TODO: Make something that does this for me cause it's awful
	printf("Registering classes to AngelScript . . . ");

	/** Vec2 **/
	//Properties
	System::asEngine->RegisterObjectType("Vec2", sizeof(Vec2), asOBJ_VALUE | asGetTypeTraits<Vec2>() | asOBJ_APP_CLASS_ALLFLOATS);
	System::asEngine->RegisterObjectProperty("Vec2", "float x", asOFFSET(Vec2, x));
	System::asEngine->RegisterObjectProperty("Vec2", "float y", asOFFSET(Vec2, y));

	//Constructors and destructor
	System::asEngine->RegisterObjectBehaviour("Vec2", asBEHAVE_CONSTRUCT, "void f()", asFUNCTION(ASBindings::Vec2DefCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Vec2", asBEHAVE_CONSTRUCT, "void f(const Vec2 &in)", asFUNCTION(ASBindings::Vec2CopyCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Vec2", asBEHAVE_CONSTRUCT, "void f(float x = 0, float y = 0)", asFUNCTION(ASBindings::Vec2InitCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Vec2", asBEHAVE_DESTRUCT, "void f()", asFUNCTION(ASBindings::Vec2Dtor), asCALL_CDECL_OBJLAST);

	//Operator Overloads
	System::asEngine->RegisterObjectMethod("Vec2", "float& opIndex(int)", asMETHODPR(Vec2, operator[], (int32_t), float&), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec2", "Vec2 opSub() const", asFUNCTIONPR(glm::operator-, (const Vec2 &), Vec2), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec2", "Vec2 opAssign(const Vec2 &in)", asMETHODPR(Vec2, operator=, (const Vec2 &), Vec2 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec2", "Vec2 opAdd(const Vec2 &in)", asFUNCTIONPR(glm::operator+, (const Vec2 &, const Vec2 &), Vec2), asCALL_CDECL_OBJLAST);
	//System::asEngine->RegisterObjectMethod("Vec2", "Vec2 opAdd(const float &in)", asFUNCTIONPR(glm::operator+, (const float &, const Vec2 &), Vec2), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec2", "Vec2 opSub(const Vec2 &in)", asFUNCTIONPR(glm::operator-, (const Vec2 &, const Vec2 &), Vec2), asCALL_CDECL_OBJLAST);
	//System::asEngine->RegisterObjectMethod("Vec2", "Vec2 opSub(const float &in)", asFUNCTIONPR(glm::operator-, (const float &, const Vec2 &), Vec2), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec2", "Vec2 opMul(const Vec2 &in)", asFUNCTIONPR(glm::operator*, (const Vec2 &, const Vec2 &), Vec2), asCALL_CDECL_OBJLAST);
	//System::asEngine->RegisterObjectMethod("Vec2", "Vec2 opMul(const float &in)", asFUNCTIONPR(glm::operator*, (const float &, const Vec2 &), Vec2), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec2", "Vec2 opDiv(const Vec2 &in)", asFUNCTIONPR(glm::operator/, (const Vec2 &, const Vec2 &), Vec2), asCALL_CDECL_OBJLAST);
	//System::asEngine->RegisterObjectMethod("Vec2", "Vec2 opDiv(const float &in)", asFUNCTIONPR(glm::operator/, (const float &, const Vec2 &), Vec2), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec2", "Vec2 & opAddAssign(const Vec2 &in) const", asMETHODPR(Vec2, operator+=, (const Vec2 &), Vec2 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec2", "Vec2 & opSubAssign(const Vec2 &in) const", asMETHODPR(Vec2, operator-=, (const Vec2 &), Vec2 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec2", "Vec2 & opMulAssign(const Vec2 &in) const", asMETHODPR(Vec2, operator*=, (const Vec2 &), Vec2 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec2", "Vec2 & opDivAssign(const Vec2 &in) const", asMETHODPR(Vec2, operator/=, (const Vec2 &), Vec2 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec2", "bool opEquals(const Vec2 &in)", asFUNCTIONPR(glm::operator==, (const Vec2 &, const Vec2 &), bool), asCALL_CDECL_OBJLAST);

	//Methods
	System::asEngine->RegisterGlobalFunction("float dot(const Vec2 &in, const Vec2 &in)", asFUNCTIONPR(glm::dot, (const Vec2 &, const Vec2 &), float), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Vec2", "float dot(const Vec2 &in)", asFUNCTIONPR(glm::dot, (const Vec2 &, const Vec2 &), float), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterGlobalFunction("float distance(const Vec2 &in, const Vec2 &in)", asFUNCTIONPR(glm::distance, (const Vec2 &, const Vec2 &), float), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Vec2", "float distance(const Vec2 &in, const Vec2 &in)", asFUNCTIONPR(glm::distance, (const Vec2 &, const Vec2 &), float), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterGlobalFunction("Vec2 normalize(Vec2 &in)", asFUNCTIONPR(glm::normalize, (const Vec2 &), Vec2), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Vec2", "Vec2 normal()", asFUNCTIONPR(glm::normalize, (const Vec2 &), Vec2), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterGlobalFunction("float magnitude(const Vec2 &in)", asFUNCTIONPR(glm::length, (const Vec3 &), float), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Vec2", "float magnitude()", asFUNCTIONPR(glm::length, (const Vec3 &), float), asCALL_CDECL_OBJLAST);

	/** Vec3 **/
	//Properties
	System::asEngine->RegisterObjectType("Vec3", sizeof(Vec3), asOBJ_VALUE | asGetTypeTraits<Vec3>() | asOBJ_APP_CLASS_ALLFLOATS);
	System::asEngine->RegisterObjectProperty("Vec3", "float x", asOFFSET(Vec3, x));
	System::asEngine->RegisterObjectProperty("Vec3", "float y", asOFFSET(Vec3, y));
	System::asEngine->RegisterObjectProperty("Vec3", "float z", asOFFSET(Vec3, z));

	//Constructors
	System::asEngine->RegisterObjectBehaviour("Vec3", asBEHAVE_CONSTRUCT, "void f()", asFUNCTION(ASBindings::Vec3DefCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Vec3", asBEHAVE_CONSTRUCT, "void f(const Vec3 &in)", asFUNCTION(ASBindings::Vec3CopyCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Vec3", asBEHAVE_CONSTRUCT, "void f(float x = 0, float y = 0, float z = 0)", asFUNCTION(ASBindings::Vec3InitCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Vec3", asBEHAVE_DESTRUCT, "void f()", asFUNCTION(ASBindings::Vec3Dtor), asCALL_CDECL_OBJLAST);

	//Operator Overloads
	System::asEngine->RegisterObjectMethod("Vec3", "float& opIndex(int)", asMETHODPR(Vec3, operator[], (int32_t), float&), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec3", "Vec3 opSub() const", asFUNCTIONPR(glm::operator-, (const Vec3 &), Vec3), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec3", "Vec3 & opAssign(const Vec3 &in)", asMETHODPR(Vec3, operator=, (const Vec3 &), Vec3 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec3", "Vec3 opAdd(const Vec3 &in)", asFUNCTIONPR(glm::operator+, (const Vec3 &, const Vec3 &), Vec3), asCALL_CDECL_OBJLAST);
	///System::asEngine->RegisterObjectMethod("Vec3", "Vec3 opAdd(const float &in)", asFUNCTIONPR(glm::operator+, (const float &, const Vec3 &), Vec3), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec3", "Vec3 opSub(const Vec3 &in)", asFUNCTIONPR(glm::operator-, (const Vec3 &, const Vec3 &in), Vec3), asCALL_CDECL_OBJLAST);
	//System::asEngine->RegisterObjectMethod("Vec3", "Vec3 opSub(const float &in)", asFUNCTIONPR(glm::operator-, (const float &, const Vec3 &), Vec3), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec3", "Vec3 opMul(const Vec3 &in)", asFUNCTIONPR(glm::operator*, (const Vec3 &, const Vec3 &), Vec3), asCALL_CDECL_OBJLAST);
	//System::asEngine->RegisterObjectMethod("Vec3", "Vec3 opMul(const float &in)", asFUNCTIONPR(glm::operator*, (const float &, const Vec3 &), Vec3), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec3", "Vec3 opDiv(const Vec3 &in)", asFUNCTIONPR(glm::operator/, (const Vec3 &, const Vec3 &), Vec3), asCALL_CDECL_OBJLAST);
	//System::asEngine->RegisterObjectMethod("Vec3", "Vec3 opDiv(const float &in)", asFUNCTIONPR(glm::operator/, (const float &, const Vec3 &), Vec3), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec3", "Vec3 & opAddAssign(const Vec3 &in) const", asMETHODPR(Vec3, operator+=, (const Vec3 &), Vec3 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec3", "Vec3 & opSubAssign(const Vec3 &in) const", asMETHODPR(Vec3, operator-=, (const Vec3 &), Vec3 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec3", "Vec3 & opMulAssign(const Vec3 &in) const", asMETHODPR(Vec3, operator*=, (const Vec3 &), Vec3 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec3", "Vec3 & opDivAssign(const Vec3 &in) const", asMETHODPR(Vec3, operator/=, (const Vec3 &), Vec3 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec3", "bool opEquals(const Vec3 &in)", asFUNCTIONPR(glm::operator==, (const Vec3 &, const Vec3 &), bool), asCALL_CDECL_OBJLAST);

	//Methods
	System::asEngine->RegisterGlobalFunction("float dot(const Vec3 &in, const Vec3 &in)", asFUNCTIONPR(glm::dot, (const Vec3 &, const Vec3 &), float), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Vec3", "float dot(const Vec3 &in)", asFUNCTIONPR(glm::dot, (const Vec3 &, const Vec3 &), float), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterGlobalFunction("Vec3 cross(const Vec3 &in, const Vec3 &in)", asFUNCTIONPR(glm::cross, (const Vec3 &, const Vec3 &), Vec3), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Vec3", "Vec3 cross(const Vec3 &in)", asFUNCTIONPR(glm::cross, (const Vec3 &, const Vec3 &), Vec3), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterGlobalFunction("float distance(const Vec3 &in, const Vec3 &in)", asFUNCTIONPR(glm::distance, (const Vec3 &, const Vec3 &), float), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Vec3", "float distance(const Vec3 &in)", asFUNCTIONPR(glm::distance, (const Vec3 &, const Vec3 &), float), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterGlobalFunction("Vec3 normalize(const Vec3 &in)", asFUNCTIONPR(glm::normalize, (const Vec3 &), Vec3), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Vec3", "Vec3 normal()", asFUNCTIONPR(glm::normalize, (const Vec3 &), Vec3), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterGlobalFunction("float magnitude(const Vec3 &in)", asFUNCTIONPR(glm::length, (const Vec3 &), float), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Vec3", "float magnitude()", asFUNCTIONPR(glm::length, (const Vec3 &), float), asCALL_CDECL_OBJLAST);

	/** Vec4 **/
	//Properties
	System::asEngine->RegisterObjectType("Vec4", sizeof(Vec4), asOBJ_VALUE | asGetTypeTraits<Vec4>() | asOBJ_APP_CLASS_ALLFLOATS);
	System::asEngine->RegisterObjectProperty("Vec4", "float x", asOFFSET(Vec4, x));
	System::asEngine->RegisterObjectProperty("Vec4", "float y", asOFFSET(Vec4, y));
	System::asEngine->RegisterObjectProperty("Vec4", "float z", asOFFSET(Vec4, z));
	System::asEngine->RegisterObjectProperty("Vec4", "float w", asOFFSET(Vec4, w));

	//Constructors
	System::asEngine->RegisterObjectBehaviour("Vec4", asBEHAVE_CONSTRUCT, "void f()", asFUNCTION(ASBindings::Vec4DefCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Vec4", asBEHAVE_CONSTRUCT, "void f(const Vec4 &in)", asFUNCTION(ASBindings::Vec4CopyCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Vec4", asBEHAVE_CONSTRUCT, "void f(float x = 0, float y = 0, float z = 0, float w = 0)", asFUNCTION(ASBindings::Vec4InitCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Vec4", asBEHAVE_DESTRUCT, "void f()", asFUNCTION(ASBindings::Vec4Dtor), asCALL_CDECL_OBJLAST);

	//Operator Overloads
	System::asEngine->RegisterObjectMethod("Vec4", "float& opIndex(int)", asMETHODPR(Vec4, operator[], (int32_t), float&), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 opSub()", asFUNCTIONPR(glm::operator-, (const Vec4 &), Vec4), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 & opAssign(const Vec4 &in)", asMETHODPR(Vec4, operator=, (const Vec4 &), Vec4 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 opAdd(const Vec4 &in)", asFUNCTIONPR(glm::operator+, (const Vec4 &, const Vec4 &), Vec4), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 opAdd(float)", asFUNCTIONPR(glm::operator+, (float, const Vec4 &), Vec4), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 opSub(const Vec4 &in)", asFUNCTIONPR(glm::operator-, (const Vec4 &, const Vec4 &in), Vec4), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 opSub(float)", asFUNCTIONPR(glm::operator-, (float, const Vec4 &), Vec4), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 opMul(const Vec4 &in)", asFUNCTIONPR(glm::operator*, (const Vec4 &, const Vec4 &), Vec4), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 opMul(float)", asFUNCTIONPR(glm::operator*, (float, const Vec4 &), Vec4), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 opDiv(const Vec4 &in)", asFUNCTIONPR(glm::operator/, (const Vec4 &, const Vec4 &), Vec4), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 opDiv(float)", asFUNCTIONPR(glm::operator/, (float, const Vec4 &), Vec4), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 & opAddAssign(const Vec4 &in) const", asMETHODPR(Vec4, operator+=, (const Vec4 &), Vec4 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 & opSubAssign(const Vec4 &in) const", asMETHODPR(Vec4, operator-=, (const Vec4 &), Vec4 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 & opMulAssign(const Vec4 &in) const", asMETHODPR(Vec4, operator*=, (const Vec4 &), Vec4 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 & opDivAssign(const Vec4 &in) const", asMETHODPR(Vec4, operator/=, (const Vec4 &), Vec4 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Vec4", "bool opEquals(const Vec4 &in)", asFUNCTIONPR(glm::operator==, (const Vec4 &, const Vec4 &), bool), asCALL_CDECL_OBJLAST);

	//Methods
	System::asEngine->RegisterGlobalFunction("float dot(const Vec4 &in, const Vec4 &in)", asFUNCTIONPR(glm::dot, (const Vec4 &, const Vec4 &), float), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Vec4", "float dot(const Vec4 &in)", asFUNCTIONPR(glm::dot, (const Vec4 &, const Vec4 &), float), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterGlobalFunction("float distance(const Vec4 &in, const Vec4 &in)", asFUNCTIONPR(glm::distance, (const Vec4 &, const Vec4 &), float), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Vec4", "float distance(const Vec4 &in)", asFUNCTIONPR(glm::distance, (const Vec4 &, const Vec4 &), float), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterGlobalFunction("Vec4 normalize(const Vec4 &in)", asFUNCTIONPR(glm::normalize, (const Vec4 &), Vec4), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Vec4", "Vec4 normal()", asFUNCTIONPR(glm::normalize, (const Vec4 &), Vec4), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterGlobalFunction("float magnitude(const Vec4 &in)", asFUNCTIONPR(glm::length, (const Vec4 &), float), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Vec4", "float magnitude()", asFUNCTIONPR(glm::length, (const Vec4 &), float), asCALL_CDECL_OBJLAST);

	/** Quat **/
	//Properties
	System::asEngine->RegisterObjectType("Quat", sizeof(Quat), asOBJ_VALUE | asGetTypeTraits<Quat>() | asOBJ_APP_CLASS_ALLFLOATS);
	System::asEngine->RegisterObjectProperty("Quat", "float w", asOFFSET(Quat, w));
	System::asEngine->RegisterObjectProperty("Quat", "float x", asOFFSET(Quat, x));
	System::asEngine->RegisterObjectProperty("Quat", "float y", asOFFSET(Quat, y));
	System::asEngine->RegisterObjectProperty("Quat", "float z", asOFFSET(Quat, z));

	//Constructors
	System::asEngine->RegisterObjectBehaviour("Quat", asBEHAVE_CONSTRUCT, "void f()", asFUNCTION(ASBindings::QuatDefCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Quat", asBEHAVE_CONSTRUCT, "void f(const Quat &in)", asFUNCTION(ASBindings::QuatCopyCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Quat", asBEHAVE_CONSTRUCT, "void f(float w = 0, float x = 0, float y = 0, float z = 0)", asFUNCTION(ASBindings::QuatInitCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Quat", asBEHAVE_DESTRUCT, "void f()", asFUNCTION(ASBindings::QuatDtor), asCALL_CDECL_OBJLAST);

	//Operator Overloads
	System::asEngine->RegisterObjectMethod("Quat", "float& opIndex(int)", asMETHODPR(Quat, operator[], (int32_t), float&), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Quat", "Quat opSub() const", asFUNCTIONPR(glm::operator-, (const Quat &), Quat), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Quat", "Quat & opAssign(const Quat &in)", asMETHODPR(Quat, operator=, (const Quat &), Quat &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Quat", "Quat opAdd(const Quat &in)", asFUNCTIONPR(glm::operator+, (const Quat &, const Quat &), Quat), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Quat", "Quat opMul(const Quat &in)", asFUNCTIONPR(glm::operator*, (const Quat &, const Quat &), Quat), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Quat", "Quat opMul(const float &in)", asFUNCTIONPR(glm::operator*, (const float &, const Quat &), Quat), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Quat", "Quat opDiv(const float &in)", asFUNCTIONPR(glm::operator/, (const Quat &, const float &), Quat), asCALL_CDECL_OBJFIRST);
	System::asEngine->RegisterObjectMethod("Quat", "Quat & opAddAssign(const Quat &in) const", asMETHODPR(Quat, operator+=, (const Quat &), Quat &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Quat", "Quat & opMulAssign(const Quat &in) const", asMETHODPR(Quat, operator*=, (const Quat &), Quat &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Quat", "Quat & opDivAssign(const float &in) const", asMETHODPR(Quat, operator/=, (const float &), Quat &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Quat", "bool opEquals(const Quat &in)", asFUNCTIONPR(glm::operator==, (const Quat &, const Quat &), bool), asCALL_CDECL_OBJLAST);

	//Methods
	System::asEngine->RegisterGlobalFunction("Quat angleAxis(const float &in, const Vec3 &in)", asFUNCTIONPR(glm::angleAxis, (const float &, const Vec3 &), Quat), asCALL_CDECL);
	System::asEngine->RegisterGlobalFunction("Quat axisAngle(const Vec3 &in, const float &in)", asFUNCTIONPR(glm::axisAngle, (const Vec3 &, const float &), Quat), asCALL_CDECL);
	System::asEngine->RegisterGlobalFunction("Quat yaw(const float &in)", asFUNCTIONPR(glm::yaw, (const float &), Quat), asCALL_CDECL);
	System::asEngine->RegisterGlobalFunction("Quat pitch(const float &in)", asFUNCTIONPR(glm::pitch, (const float &), Quat), asCALL_CDECL);
	System::asEngine->RegisterGlobalFunction("Quat roll(const float &in)", asFUNCTIONPR(glm::roll, (const float &), Quat), asCALL_CDECL);
	System::asEngine->RegisterGlobalFunction("Quat normalize(const Quat &in)", asFUNCTIONPR(glm::normalize, (const Quat &), Quat), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Quat", "Quat normal()", asFUNCTIONPR(glm::normalize, (const Quat &), Quat), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterGlobalFunction("float magnitude(const Quat &in)", asFUNCTIONPR(glm::length, (const Quat &), float), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Quat", "float magnitude()", asFUNCTIONPR(glm::length, (const Quat &), float), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterGlobalFunction("Quat conjugate(const Quat &in)", asFUNCTIONPR(glm::conjugate, (const Quat &), Quat), asCALL_CDECL);
	System::asEngine->RegisterObjectMethod("Quat", "Quat conjugate()", asFUNCTIONPR(glm::conjugate, (const Quat &), Quat), asCALL_CDECL_OBJLAST);

	/** Mat4 **/
	//Properties
	System::asEngine->RegisterObjectType("Mat4", sizeof(Mat4), asOBJ_VALUE | asGetTypeTraits<Mat4>() | asOBJ_APP_CLASS_ALLFLOATS);

	//Constructors
	System::asEngine->RegisterObjectBehaviour("Mat4", asBEHAVE_CONSTRUCT, "void f()", asFUNCTION(ASBindings::Mat4DefCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Mat4", asBEHAVE_CONSTRUCT, "void f(const Mat4 &in)", asFUNCTION(ASBindings::Mat4CopyCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Mat4", asBEHAVE_CONSTRUCT,
									  "void f(float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float)",
									  asFUNCTION(ASBindings::Mat4InitCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Mat4", asBEHAVE_DESTRUCT, "void f()", asFUNCTION(ASBindings::Mat4Dtor), asCALL_CDECL_OBJLAST);

	//Operator Overloads
	System::asEngine->RegisterObjectMethod("Mat4", "Vec4 & opIndex(int)", asMETHODPR(Mat4, operator[], (int32_t), Vec4 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Mat4", "Mat4 & opAssign(const Mat4 &in)", asMETHODPR(Mat4, operator=, (const Mat4 &), Mat4 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Mat4", "Mat4 opMul(const Mat4 &in)", asFUNCTIONPR(glm::operator*, (const Mat4 &, const Mat4 &), Mat4), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Mat4", "Mat4 opMul(const float &in)", asFUNCTIONPR(glm::operator*, (const float &, const Mat4 &), Mat4), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Mat4", "Vec4 opMul(const Vec4 &in, const Mat4 &in)", asFUNCTIONPR(glm::operator*, (const Vec4 &, const Mat4 &), Vec4), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectMethod("Mat4", "Mat4 & opMulAssign(const Mat4 &in)", asMETHODPR(Mat4, operator*=, (const Mat4 &), Mat4 &), asCALL_THISCALL);

	//Methods
	System::asEngine->RegisterGlobalFunction("Mat4 ortho(float, float, float, float)", asFUNCTIONPR(glm::ortho, (float, float, float, float), Mat4), asCALL_CDECL);
	System::asEngine->RegisterGlobalFunction("Mat4 ortho(float, float, float, float, float, float)", asFUNCTIONPR(glm::ortho, (float, float, float, float, float, float), Mat4), asCALL_CDECL);
	System::asEngine->RegisterGlobalFunction("Mat4 persective(float, float, float, float)", asFUNCTIONPR(glm::perspective, (float, float, float, float), Mat4), asCALL_CDECL);
	System::asEngine->RegisterGlobalFunction("Mat4 translation(const Vec3 &in)", asFUNCTIONPR(glm::translate, (const Mat4 &, const Vec3 &), Mat4), asCALL_CDECL);
	System::asEngine->RegisterGlobalFunction("Mat4 scale(const Vec3 &in)", asFUNCTIONPR(glm::scale, (const Mat4 &, const Vec3 &), Mat4), asCALL_CDECL);
	System::asEngine->RegisterGlobalFunction("Mat4 lookAt(const Vec3 &in, const Vec3 &in, const Vec3 &in)", asFUNCTIONPR(glm::lookAt, (const Vec3 &, const Vec3 &, const Vec3 &), Mat4), asCALL_CDECL);
	System::asEngine->RegisterGlobalFunction("Mat4 yawPitchRoll(float, float, float)", asFUNCTIONPR(glm::yawPitchRoll, (const float &, const float &, const float &), Mat4), asCALL_CDECL);

	/** Transform **/
	//Properties
	System::asEngine->RegisterObjectType("Transform", sizeof(Transform), asOBJ_VALUE | asGetTypeTraits<Transform>() | asOBJ_APP_CLASS_ALLFLOATS);

	//Constructors
	System::asEngine->RegisterObjectBehaviour("Transform", asBEHAVE_CONSTRUCT, "void f()", asFUNCTION(Transform::DefCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Transform", asBEHAVE_CONSTRUCT, "void f(const Transform &in)", asFUNCTION(Transform::CopyCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Transform", asBEHAVE_CONSTRUCT, "void f(const Vec3 &in)", asFUNCTION(Transform::InitCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("Transform", asBEHAVE_DESTRUCT, "void f()", asFUNCTION(Transform::Dtor), asCALL_CDECL_OBJLAST);

	//Operator Overloads
	System::asEngine->RegisterObjectMethod("Transform", "const Transform & opAssign(const Transform &in)", asMETHODPR(Transform, operator=, (const Transform&), const Transform&), asCALL_THISCALL);

	//Methods
	System::asEngine->RegisterObjectMethod("Transform", "void update()", asMETHODPR(Transform, update, (), void), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Transform", "const Vec3 & getPos() const", asMETHODPR(Transform, getPos, () const, const Vec3 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Transform", "void setPos(const Vec3 &in)", asMETHODPR(Transform, setPos, (const Vec3 &), void), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Transform", "const Vec3 & getScale() const", asMETHODPR(Transform, getScale, () const, const Vec3 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Transform", "void setScale(const Vec3 &in)", asMETHODPR(Transform, setScale, (const Vec3 &), void), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Transform", "const Quat & getRot() const", asMETHODPR(Transform, getRot, () const, const Quat &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Transform", "void setRot(const Quat &in)", asMETHODPR(Transform, setRot, (const Quat &), void), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Transform", "const Vec3 & getFront() const", asMETHODPR(Transform, getFront, () const, const Vec3 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Transform", "const Vec3 & getRight() const", asMETHODPR(Transform, getRight, () const, const Vec3 &), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("Transform", "const Vec3 & getUp() const", asMETHODPR(Transform, getUp, () const, const Vec3 &), asCALL_THISCALL);

	/** RigidBody **/
	System::asEngine->RegisterObjectType("RigidBody", sizeof(RigidBody), asOBJ_VALUE | asGetTypeTraits<RigidBody>() | asOBJ_APP_CLASS_ALLFLOATS);
	System::asEngine->RegisterObjectProperty("RigidBody", "Vec3 vel", asOFFSET(RigidBody, vel));
	System::asEngine->RegisterObjectProperty("RigidBody", "Vec3 force", asOFFSET(RigidBody, force));
	System::asEngine->RegisterObjectProperty("RigidBody", "Vec3 aVel", asOFFSET(RigidBody, aVel));
	System::asEngine->RegisterObjectProperty("RigidBody", "Vec3 torq", asOFFSET(RigidBody, torq));
	System::asEngine->RegisterObjectProperty("RigidBody", "Vec3 grav", asOFFSET(RigidBody, grav));
	System::asEngine->RegisterObjectProperty("RigidBody", "float mass", asOFFSET(RigidBody, mass));
	System::asEngine->RegisterObjectProperty("RigidBody", "float speed", asOFFSET(RigidBody, speed));
	System::asEngine->RegisterObjectProperty("RigidBody", "float drag", asOFFSET(RigidBody, drag));
	System::asEngine->RegisterObjectProperty("RigidBody", "float inert", asOFFSET(RigidBody, inert));
	System::asEngine->RegisterObjectProperty("RigidBody", "float aSpeed", asOFFSET(RigidBody, aSpeed));
	System::asEngine->RegisterObjectProperty("RigidBody", "float aDrag", asOFFSET(RigidBody, aDrag));
	System::asEngine->RegisterObjectProperty("RigidBody", "float maxSpeed", asOFFSET(RigidBody, maxSpeed));
	System::asEngine->RegisterObjectProperty("RigidBody", "float maxASpeed", asOFFSET(RigidBody, maxASpeed));

	//Constructors + dtor
	System::asEngine->RegisterObjectBehaviour("RigidBody", asBEHAVE_CONSTRUCT, "void f()", asFUNCTION(RigidBody::DefCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("RigidBody", asBEHAVE_CONSTRUCT, "void f(const RigidBody &in)", asFUNCTION(RigidBody::CopyCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("RigidBody", asBEHAVE_CONSTRUCT, "void f(Transform &in)", asFUNCTION(RigidBody::InitCtor), asCALL_CDECL_OBJLAST);
	System::asEngine->RegisterObjectBehaviour("RigidBody", asBEHAVE_DESTRUCT, "void f()", asFUNCTION(RigidBody::Dtor), asCALL_CDECL_OBJLAST);

	//Operator Overloads
	System::asEngine->RegisterObjectMethod("RigidBody", "const RigidBody & opAssign(const RigidBody &in)", asMETHODPR(RigidBody, operator=, (const RigidBody&), const RigidBody &), asCALL_THISCALL);

	//Methods
	//System::asEngine->RegisterObjectMethod("RigidBody", "void setTransform(Transform &in)", asMETHODPR(RigidBody, setTransform, (Transform&), void), asCALL_THISCALL);
	System::asEngine->RegisterObjectMethod("RigidBody", "void update()", asMETHODPR(RigidBody, update, (), void), asCALL_THISCALL);

	printf("ok\n");
}

//Callbacks
/*! \brief Angelscript message callback
 *
 * \param (const asSMessageInfo*) msg - The message from AngelScript
 * \param (void*) param - The parameters
 */
void viv::System::asMessageCallback(const asSMessageInfo* msg, void* param)
{
	const char* type = "ERR: ";

	if(msg->type == asMSGTYPE_WARNING)
		type = "WARN: ";
	else if (msg->type ==  asMSGTYPE_INFORMATION)
		type = "INFO: ";

	printf("%s (%d, %d) : %s%s\n", msg->section, msg->row, msg->col, type, msg->message);
}

/*! \brief System destrutor
 */
viv::System::~System()
{

}

/*! \brief AngelScript print
 *
 * \param (const std::string&) str - The string to print
 */
void viv::System::asPrint(const std::string &str)
{
	printf("%s", str.c_str());
}
