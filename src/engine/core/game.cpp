/*! \file game.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "game.h"

viv::Game::GameState viv::Game::curState = Game::GameState::INGAME;
std::unique_ptr<viv::Game> viv::Game::instance = std::unique_ptr<viv::Game>(nullptr);
viv::Camera* viv::Game::activeCam = nullptr;
viv::Node* viv::Game::base = nullptr;
std::vector<std::shared_ptr<viv::DirectionalLight>> viv::Game::sceneDirectionalLights;
std::vector<std::shared_ptr<viv::PointLight>> viv::Game::scenePointLights;
std::vector<std::shared_ptr<viv::SpotLight>> viv::Game::sceneSpotLights;
std::shared_ptr<viv::Skybox> viv::Game::skybox;
const viv::RenderBackend* viv::Game::backend = nullptr; //! The rendering backend

/*! \brief Default game constructor
 */
viv::Game::Game()
{

}

/*! \brief Gets the instance of the game singleton
 *
 * \return (Game*) Pointer to the game instance
 */
viv::Game* viv::Game::getInstance()
{
	if(Game::instance.get() == nullptr)
		Game::instance = std::unique_ptr<Game>(new Game);

	return Game::instance.get();
}

/*! \brief Used to call asset loading methods
 */
void viv::Game::loadAssets()
{
	std::string roughnessName = "default_roughness";
	std::string metalName = "default_metal";
	std::string matName = "default";

	uint8_t albedo[3] = { 255, 255, 255 }; //1px, 3 channel white texture
	uint8_t normal[3] = { 128, 128, 255 }; //1px, 3 channel half red, half green, full blue texture
	uint8_t metal = 0;
	uint8_t roughness = 0;
	uint8_t ao = 255; //1px, 1 channel white texture
	uint8_t disp = 0; //1px, 1 channel black texture

	AssetManager::createTexture("default_albedo", 1, 1, 3, 8, GL_UNSIGNED_BYTE, GL_REPEAT, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, (const void*)albedo, false);
	AssetManager::createTexture("default_normal", 1, 1, 3, 8, GL_UNSIGNED_BYTE, GL_REPEAT, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, (const void*)normal, false);
	AssetManager::createTexture("default_ao", 1, 1, 1, 8, GL_UNSIGNED_BYTE, GL_REPEAT, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, (const void*)&ao, false);
	AssetManager::createTexture("default_disp", 1, 1, 1, 8, GL_UNSIGNED_BYTE, GL_REPEAT, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, (const void*)&disp, false);

	for(uint32_t i = 0; i < 10; i++) {
		roughness = std::round((i * 0.1112f) * 255.0f);
		AssetManager::createTexture((roughnessName + std::to_string(i)).c_str(), 1, 1, 1, 8, GL_UNSIGNED_BYTE, GL_REPEAT, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, (const void*)&roughness, false);
	}

	for(uint32_t i = 0; i < 2; i++) {
		metal = i * 255;
		AssetManager::createTexture((metalName + std::to_string(i)).c_str(), 1, 1, 1, 8, GL_UNSIGNED_BYTE, GL_REPEAT, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, (const void*)&metal, false);
		for(uint32_t j = 0; j < 10; j++) {
			AssetManager::createMaterial((matName + std::to_string(i) + std::to_string(j)).c_str(), "default_albedo",
										 "default_normal",
										 ("default_metal" + std::to_string(i)).c_str(),
										 ("default_roughness" + std::to_string(j)).c_str(),
										 "default_ao",
										 "default_disp");
		}
	}
	AssetManager::loadDirectory("../assets/shaders");
	AssetManager::loadDirectory("../assets/textures");
	AssetManager::loadDirectory("../assets/materials");
	AssetManager::loadDirectory("../assets/models");
	AssetManager::loadDirectory("../assets/scripts");
	AssetManager::createMesh("quad", "quad", "defaultShader");
}

/*! \brief Used to unload the assets
 */
void viv::Game::unloadAssets()
{
	//AssetManager::unloadAllAssets();
}

/*! \brief Sets the rendering backend and initialized gameobjects
 */
void viv::Game::start(const RenderBackend* backend)
{
	Game::backend = backend;

	std::shared_ptr<Script> test = AssetManager::getScript("test");
	test->callFunc("vec3Test");

	base = new Node("Base");

	Camera* cam = dynamic_cast<Camera*>(Game::base->addSubNode(std::unique_ptr<Camera>(new Camera(45.0f, WindowData::innerSize.x / WindowData::innerSize.y, 0.01f, 1000.0f, Vec3(), "cam"))));

	AssetManager::createMesh("sphere2", "sphere", "defaultShader");
	AssetManager::createMesh("sphere3", "sphere", "defaultShader");
	AssetManager::createMesh("sphere4", "sphere", "defaultShader");

	float y = 0.5f;
	bool odd;
	bool mod2;
	for(uint32_t i = 0; i < 2; i++) {
		float x = -2.5f;
		(i == 0) ? odd = false : odd = true;
		for(uint32_t j = 0; j < 10; j++) {
			Transform* sphere = dynamic_cast<Transform*>(Game::base->addSubNode(std::unique_ptr<RigidBody>(new RigidBody(false, Vec3(x, y, -5.0f), Vec3(-1.0f, -1.0f, -1.0f), Vec3(1.0f, 1.0f, 1.0f), "sphere"))));
			sphere->setScale(Vec3(0.5f, 0.5f, 0.5f));
			x++;
			std::string mName = "sphere";
			(j < 5) ? mod2 = false : mod2 = true;

			if(!mod2 && !odd)
				mName += "2";
			else if(!mod2 && odd)
				mName += "3";
			else if(mod2 && !odd)
				mName += "4";

			MeshInstance* mi = dynamic_cast<MeshInstance*>(sphere->addSubNode(std::unique_ptr<MeshInstance>(new MeshInstance(AssetManager::getMesh(mName.c_str())))));

			mi->setMaterial(AssetManager::getMaterial(("default" + std::to_string(i) + std::to_string(j)).c_str()));
		}
		y--;
	}

	Node* cube = Game::base->addSubNode(std::unique_ptr<RigidBody>(new RigidBody(false, Vec3(5.0f, 0.0f, -1.0f), Vec3(-1.0f, -1.0f, -1.0f), Vec3(1.0f, 1.0f, 1.0f), "cube")));
	MeshInstance* quadMesh = dynamic_cast<MeshInstance*>(cube->addSubNode(std::unique_ptr<MeshInstance>(new MeshInstance(AssetManager::getMesh("cube")))));
	quadMesh->setMaterial(AssetManager::getMaterial("harsh_brick"));

	Node* cerb = Game::base->addSubNode(std::unique_ptr<RigidBody>(new RigidBody(false, Vec3(0.0f, 0.0f, 5.0f), Vec3(-1.0f, -1.0f, -1.0f), Vec3(1.0f, 1.0f, 1.0f), "cerberus")));
	MeshInstance* cerbMesh = dynamic_cast<MeshInstance*>(cerb->addSubNode(std::unique_ptr<MeshInstance>(new MeshInstance(AssetManager::getMesh("cerberus")))));
	cerbMesh->setScale(Vec3(0.01f, 0.01f, 0.01f));
	cerbMesh->setRot(glm::axisAngle(Vec3(1.0f, 0.0f, 0.0f), Math::TO_RAD * 90.0f));
	cerbMesh->setMaterial(AssetManager::getMaterial("cerberus"));

	Node* hel = Game::base->addSubNode(std::unique_ptr<RigidBody>(new RigidBody(false, Vec3(-4.0f, -1.0f, 0.0f), Vec3(-1.0f, -1.0f, -1.0f), Vec3(1.0f, 1.0f, 1.0f), "helix")));
	MeshInstance* helMesh = dynamic_cast<MeshInstance*>(hel->addSubNode(std::unique_ptr<MeshInstance>(new MeshInstance(AssetManager::getMesh("helix")))));
	helMesh->setMaterial(AssetManager::getMaterial("metal_grid"));

	//Dir light 1
	sceneDirectionalLights.push_back(std::shared_ptr<DirectionalLight>(new DirectionalLight(backend, AssetManager::getShader("dirLightShader"))));
	sceneDirectionalLights[0]->l.color = Vec3(1.0f, 1.0f, 1.0f);
	//sceneDirectionalLights[0]->l.intensity = 0.5;
	Transform* light1 = dynamic_cast<Transform*>(Game::base->addSubNode(std::unique_ptr<DirectionalLightInstance>(new DirectionalLightInstance(std::shared_ptr<DirectionalLight>(sceneDirectionalLights[0])))));
	light1->setPos(Vec3(0.0f, 1.0f, 5.0f));

	//Dir light 2
	sceneDirectionalLights.push_back(std::shared_ptr<DirectionalLight>(new DirectionalLight(backend, AssetManager::getShader("dirLightShader"))));
	sceneDirectionalLights[1]->l.color = Vec3(0.0f, 1.0f, 0.0f);
	Transform* light2 = dynamic_cast<Transform*>(Game::base->addSubNode(std::unique_ptr<DirectionalLightInstance>(new DirectionalLightInstance(std::shared_ptr<DirectionalLight>(sceneDirectionalLights[1])))));
	light2->setPos(Vec3(0.0f, -5.0f, -15.0f));

	//Point light 1
	scenePointLights.push_back(std::shared_ptr<PointLight>(new PointLight(backend, AssetManager::getShader("pointLightShader"))));
	scenePointLights[0]->l.color = Vec3(1.0f, 0.0f, 0.0f);
	scenePointLights[0]->l.radius = 2;
	Transform* light3 = dynamic_cast<Transform*>(Game::base->addSubNode(std::unique_ptr<PointLightInstance>(new PointLightInstance(std::shared_ptr<PointLight>(scenePointLights[0])))));
	light3->setPos(Vec3(0.0f, 0.0f, -5.0f));

	scenePointLights.push_back(std::shared_ptr<PointLight>(new PointLight(backend, AssetManager::getShader("pointLightShader"))));
	scenePointLights[1]->l.color = Vec3(0.0f, 0.0f, 1.0f);
	scenePointLights[1]->l.radius = 2;
	Transform* light4 = dynamic_cast<Transform*>(Game::base->addSubNode(std::unique_ptr<PointLightInstance>(new PointLightInstance(std::shared_ptr<PointLight>(scenePointLights[1])))));
	light4->setPos(Vec3(4.5f, 0.0f, -5.0f));

	Game::skybox = std::shared_ptr<Skybox>(new Skybox(backend, AssetManager::getShader("skyboxShader")));
	Game::skybox->setTextureFromEquiRect(Game::backend, AssetManager::getTexture("helipad"));
	//Game::skybox->setTextureFromEquiRect(Game::backend, AssetManager::getTexture("arches"));

	//sceneSpotLights.push_back(std::shared_ptr<SpotLight>(new SpotLight(backend)));
	//sceneSpotLights[0]->l.ambColor = Vec3(0.0f, 1.0f, 0.0f);
	//sceneSpotLights[0]->l.diffColor = Vec3(0.0f, 1.0f, 0.0f);
	//sceneSpotLights[0]->l.specColor = Vec3(0.0f, 1.0f, 0.0f);
	//sceneSpotLights[0]->l.radius = 2;
	//Transform* light3 = dynamic_cast<Transform*>(Game::base->addSubNode(std::unique_ptr<SpotLightInstance>(new SpotLightInstance(std::shared_ptr<SpotLight>(sceneSpotLights[0])))));
	//light3->setPos(Vec3(2.0f, 6.0f, -5.0f));

	Game::activeCam = cam;
}

/*! \brief Updates the game
 */
void viv::Game::update()
{
	if(Game::curState == Game::GameState::INGAME) {
		//dynamic_cast<Transform*>(base.getChildByTag("cube2"))->pos.z -= 0.1;

		//RigidBody* cube = dynamic_cast<RigidBody*>(base->getChildByTag("cube1"));
		//cube->aSpeed = 0.5f;
		//cube->aVel = Vec3(1.0f, 0.0f, 0.0f); //getPos() - cube->getRight()) * Time::deltaTime();

		if(Input::isKeyActive(Qt::Key_W))
			Game::activeCam->setPos(Game::activeCam->getPos() + Game::activeCam->getFront() * 5.0f * Time::deltaTime());
		if(Input::isKeyActive(Qt::Key_S))
			Game::activeCam->setPos(Game::activeCam->getPos() - Game::activeCam->getFront() * 5.0f * Time::deltaTime());
		if(Input::isKeyActive(Qt::Key_A))
			Game::activeCam->setPos(Game::activeCam->getPos() + Game::activeCam->getRight() * 5.0f * Time::deltaTime());
		if(Input::isKeyActive(Qt::Key_D))
			Game::activeCam->setPos(Game::activeCam->getPos() - Game::activeCam->getRight() * 5.0f * Time::deltaTime());

		if(Input::isMouseButtonDown(Qt::MiddleButton)) {
			Input::setMousePosition(WindowData::center);
		}

		else if(Input::isMouseButtonActive(Qt::MiddleButton)) {
			Input::lockMouse(true);
			Input::disableCursor(true);
			Vec2 offset = Input::getCurrentMousePosition() - Input::getPrevMousePosition();
			Game::getActiveCam()->setRot(glm::yaw(offset.x * Time::deltaTime()) * Game::getActiveCam()->getRot() * glm::pitch(offset.y * Time::deltaTime()));
		}

		else {
			Input::lockMouse(false);
			Input::disableCursor(false);
		}

		Game::base->update();
	}
}

/*! \brief Gets the current game state
 *
 * \return (GameState) the current game state
 */
viv::Game::GameState viv::Game::getCurState()
{
	return Game::curState;
}

/*! \brief Gets the active camera
 *
 * \return (Camera*) The active camera
 */
viv::Camera* viv::Game::getActiveCam()
{
	return Game::activeCam;
}

/*! \brief Sets the active camera
 *
 * \param (Camera*) cam - The camera to become the active camera
 */
void viv::Game::setActiveCam(Camera* cam)
{
	Game::activeCam = cam;
}

/*! \brief Deletes the instance of the game singleton
 */
void viv::Game::deleteInstance()
{
	if(Game::instance.get() != nullptr)
		Game::instance.reset();
}

/*! \brief destructor for game class
 */
viv::Game::~Game()
{
	delete Game::base;
}
