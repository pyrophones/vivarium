/*! \file main.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#include <iostream>
#include <QApplication>
#include "vivarium.h"

/*! \brief The main method
 */
int main(int argc, char* argv[])
{
    QApplication app(argc, argv);
    viv::Vivarium window;
    window.show();

	return window.startSystem();
}

