/*! \file texture.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#include "texture.h"

/*! \brief Texture constructor
*
 * \param (int32_t) width - The width of the texture
 * \param (int32_t) height - The height of the texture
 * \param (int32_t) channels - The amount of channels the texture has
 */
viv::Texture::Texture(int32_t width, int32_t height, int32_t channels)
{
	this->width = width;
	this->height = height;
	this->channels = channels;
}

/*! \brief Texture Copy Constructor
 *
 * \param (const Texture&) t - The texture to copy
 */
viv::Texture::Texture(const Texture& t)
{
	this->width = t.width;
	this->height = t.height;
	this->channels = t.channels;
}

/*! \brief Gets the width of the texture
 *
 * \return (int32_t) The texture width
 */
const int32_t & viv::Texture::getWidth() const
{
	return this->width;
}

/*! \brief Gets the height of the texture
 *
 * \return (int32_t) The texture height
 */
const int32_t & viv::Texture::getHeight() const
{
	return this->height;
}

/*! \brief Gets the number of channels the texture has
 *
 * \return (int32_t) The number of channels
 */
const int32_t & viv::Texture::getChannels() const
{
	return this->channels;
}

/*! \brief Texture equality operator
 *
 * \param (const Texture &) t - The texture to copy
 *
 * \return (const Texture &) - This texture after copying
 */
const viv::Texture & viv::Texture::operator=(const Texture &t)
{
	this->width = t.width;
	this->height = t.height;
	this->channels = t.channels;

	return *this;
}

/*! \brief Texture Destructor
 */
viv::Texture::~Texture()
{

}
