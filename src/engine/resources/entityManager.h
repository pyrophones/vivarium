/*! \file entity.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef ENTITYMANAGER_H
#define ENTITYMANAGER_H

#include <string>
#include <vector>
#include <memory>
#include <algorithm>
#include <utility>
#include <cstdio>
#include <cstdint>
#include "component.h"

namespace viv
{
	class Entity;

	/*! \class EntityManager
	 *  \brief Manages entites and their allocation
	 */
	class EntityManager
	{
		public:
			EntityManager(const EntityManager &) = delete;
			void operator=(const EntityManager &) = delete;
			static const EntityManager* getInstance();
			static void refresh();
			static void setEntityCount(uint32_t entityCount);
			static uint32_t createEntity(std::string tag = "entity");
			static Entity getEntity(uint32_t index);
			static void destroyEntity(uint32_t entityNum, bool instant = false);
			static void clearEntities();
			static void deleteInstance();
			~EntityManager();
		private:
			EntityManager();
			static void entityDelete(uint32_t handle);

			static std::unique_ptr<EntityManager> instance;
			static std::vector<Entity> entities;
			//static std::vector<uint32_t> entityRefs;
			static std::vector<uint32_t> deletedEntities;
			std::vector<ComponentManager> componentManagers;
	};
};

#endif

