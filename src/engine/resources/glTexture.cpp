/*! \file texture.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#include "glTexture.h"

/*! \brief Texture constructor
 *
 * \param (int32_t) width - The width of the texture
 * \param (int32_t) height - The height of the texture
 * \param (int32_t) channels - The amount of channels for the texture
 * \param (uint32_t) bits - The bit depth for the texture
 * \param (uint32_t) type - The data type of the texture
 * \param (uint32_t) clampType - The type of clamping the texture should use
 * \param (uint32_t) minFilter - The minification filter of the texture
 * \param (uint32_t) magFilter - The magnification filter of the texture
 * \param (const void*) data - The data of the texture
 * \param (bool) generateMipMaps - If mipmaps should be generated
 */
viv::GLTexture::GLTexture(int32_t width, int32_t height, int32_t channels, uint32_t bits, uint32_t type, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void* data, bool genMipMaps) : Texture(width, height, channels)
{
	this->textureType = GL_TEXTURE_2D;
	this->type = type;
	this->setupTexture(clampType, minFilter, magFilter);

	GLTexture::getTextureFormat(type, channels, bits, &this->iFormat, &this->format);
	this->createTexture(data);

	if(genMipMaps)
		this->genMipMaps();

	glBindTexture(this->textureType, 0);
}

/*! \brief Texture constructor
 *
 * \param (int32_t) width - The width of the texture
 * \param (int32_t) height - The height of the texture
 * \param (int32_t) channels - The amount of channels for the texture
 * \param (uint32_t) type - The data type of the texture
 * \param (uint32_t) iFormat - The internal format of the texture
 * \param (uint32_t) format - The OpenGL format for the texture
 * \param (uint32_t) clampType - The type of clamping the texture should use
 * \param (uint32_t) minFilter - The minification filter of the texture
 * \param (uint32_t) magFilter - The magnification filter of the texture
 * \param (const void*) data - The data of the texture
 * \param (bool) generateMipMaps - If mipmaps should be generated
 */
viv::GLTexture::GLTexture(int32_t width, int32_t height, int32_t channels, uint32_t type, uint32_t iFormat, uint32_t format, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void* data, bool generateMipMaps) : Texture(width, height, channels)
{
	this->textureType = GL_TEXTURE_2D;
	this->type = type;
	this->iFormat = iFormat;
	this->format = format;
	this->setupTexture(clampType, minFilter, magFilter);

	this->createTexture(data);

	if(generateMipMaps)
		this->genMipMaps();

	glBindTexture(this->textureType, 0);
}

/*! \brief Creates a cubemap texture
 *
 * \param (int32_t) width - The width of a single face of the cubemap
 * \param (int32_t) height - The height of a single face of the cubemap
 * \param (in32_t) channels - The amount of channels for the cubemap
 * \param (uint32_t) bits - The bit depth for the cubemap
 * \param (uint32_t) type - The data type of the cubemap
 * \param (uint32_t) clampType - The type of clamping the cubemap should use
 * \param (uint32_t) minFilter - The minification filter of the cubemap
 * \param (uint32_t) magFilter - The magnification filter of the cubemap
 * \param (const void**) data - The data of the cubemap
 * \param (bool) generateMipMaps - If mipmaps should be generated
 */
viv::GLTexture::GLTexture(int32_t width, int32_t height, int32_t channels, uint32_t bits, uint32_t type, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void** data, bool genMipMaps) : Texture(width, height, channels)
{
	this->textureType = GL_TEXTURE_CUBE_MAP;
	this->type = type;
	this->setupTexture(clampType, minFilter, magFilter);

	GLTexture::getTextureFormat(type, channels, bits, &this->iFormat, &this->format);

	this->createCubeMap(data);

	if(genMipMaps)
		this->genMipMaps();

	glBindTexture(this->textureType, 0);
}

/*! \brief GLTexture Copy Constructor
 *
 * \param (const GLTexture &) glt - The OpenGL texture to copy
 */
viv::GLTexture::GLTexture(const GLTexture &glt) : Texture(glt)
{
	this->texture = glt.texture;
	this->textureType = glt.textureType;
}

/*! \brief Generates mipmaps for the texture
 */
void viv::GLTexture::genMipMaps()
{
	glBindTexture(this->textureType, this->texture);
	glGenerateMipmap(this->textureType);
	glBindTexture(this->textureType, 0);
}

/*! \brief Binds the texture
 *
 * \param (uint32_t) bindingPoint - The active texture to set for binding
 */
void viv::GLTexture::bind(uint32_t bindingPoint) const
{
	glActiveTexture(GL_TEXTURE0 + bindingPoint);
	glBindTexture(this->textureType, this->texture);
}

/*! \brief Unbinds the texture
 *
 * \param (uint32_t) bindingPoint - The active texture to set for unbinding
 */
void viv::GLTexture::unbind(uint32_t bindingPoint) const
{
	glActiveTexture(GL_TEXTURE0 + bindingPoint);
	glBindTexture(this->textureType, 0);
}

/*! \brief Resizes a texture
 *
 * \param (uint32_t) width - The new width of the texture
 * \param (uint32_t) height - The new height of the texture
 * \param (const void*) data - The data for the texture
 */
void viv::GLTexture::resize(uint32_t width, uint32_t height, const void* data)
{
	this->width = width;
	this->height = height;
	this->createTexture(data);
}

/*! \brief Resizes a cubemap
 *
 * \param (uint32_t) width - The new width of a single face of the cubemap
 * \param (uint32_t) height - The new height of a single face of the cubemap
 * \param (const void**) data - The data for the cubemap
 */
void viv::GLTexture::resize(uint32_t width, uint32_t height, const void** data)
{
	this->width = width;
	this->height = height;
	this->createCubeMap(data);
}

/*! \brief Used to cleanup the texture at a point other then where the dtor is called
 */
void viv::GLTexture::destroy()
{
	glDeleteTextures(1, &this->texture);
}

/*! \brief Helper function that creates a texture from data
 *
 * \param (const void*) data - The data for the texture
 */
void viv::GLTexture::createTexture(const void* data)
{
	glBindTexture(this->textureType, this->texture);
	glTexImage2D(GL_TEXTURE_2D, 0, this->iFormat, this->width, this->height, 0, this->format, this->type, data);
	glBindTexture(this->textureType, 0);
}

/*! \brief Helper function that creates a cubemap from data
 *
 * \param (const void**) data - The data for the cubemap
 */
void viv::GLTexture::createCubeMap(const void** data)
{
	glBindTexture(this->textureType, this->texture);
	for(uint32_t i = 0; i < 6; i++) {
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, this->iFormat, this->width, this->height, 0, this->format, this->type, (data ? data[i] : nullptr));
	}
	glBindTexture(this->textureType, 0);
}

/*! \brief Helper function that setups up the basic parameters of the texture
 *
 * \param (uint32_t) clampType - The clamping of the texture
 * \param (uint32_t) minFilter - The minification filter of the texture
 * \param (uint32_t) magFilter - The magnification filter of the texture
 */
void viv::GLTexture::setupTexture(uint32_t clampType, uint32_t minFilter, uint32_t magFilter)
{
	//Texture methods. Generate and create the texture.
	glGenTextures(1, &this->texture);
	glBindTexture(this->textureType, this->texture);
	glTexParameteri(this->textureType, GL_TEXTURE_WRAP_S, clampType);
	glTexParameteri(this->textureType, GL_TEXTURE_WRAP_T, clampType);
	glTexParameteri(this->textureType, GL_TEXTURE_WRAP_R, clampType);
	glTexParameteri(this->textureType, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(this->textureType, GL_TEXTURE_MAG_FILTER, magFilter);
	glBindTexture(this->textureType, 0);
}

/*! \brief Gets the OpenGL texture
 *
 * \return (uint32_t) The OpenGL texture
 */
uint32_t viv::GLTexture::getTexture() const
{
	return this->texture;
}

/*! \brief Gets the texture type
 *
 * \return (uint32_t) The texture type
 */
uint32_t viv::GLTexture::getTextureType() const
{
	return this->textureType;
}

/*! \brief Gets the data type of the texture
 *
 * \return (uint32_t) The data type of the texture
 */
uint32_t viv::GLTexture::getType() const
{
	return this->type;
}

/*! \brief Gets the internal format of the texture
 *
 * \return (uint32_t) The internal format of the texture
 */
uint32_t viv::GLTexture::getIFormat() const
{
	return this->iFormat;
}

/*! \brief Gets the format of the texture
 *
 * \return (uint32_t) The format of the texture
 */
uint32_t viv::GLTexture::getFormat() const
{
	return this->format;
}

/*! \brief GLTexture assignment operator
 *
 * \param (const GLTexture &) glt - The OpenGL texture to copy
 *
 * \return (const GLTexture &) This OpenGL texture after assignment
 */
const viv::GLTexture & viv::GLTexture::operator=(const GLTexture &glt)
{
	Texture::operator=(glt);
	this->texture = glt.texture;
	this->textureType = glt.textureType;

	return *this;
}

/*! \brief GLTexture Destructor
 */
viv::GLTexture::~GLTexture()
{
	glDeleteTextures(1, &this->texture);
}

/*! \brief Helper funtion that gets the textures internal format and format
 *
 * \param (uint32_t) type - The texture type
 * \param (int32_t) channels - The amount of channels of the texture
 * \param (uint32_t) bits - The bit depth of the texture
 * \param (uint32_t*) iFormat - The variable to set as the internal format
 * \param (uint32_t*) format - The variable to set as the format
 */
void viv::GLTexture::getTextureFormat(uint32_t type, int32_t channels, uint32_t bits, uint32_t* iFormat, uint32_t* format)
{
	if(iFormat != nullptr) {
		switch(type) {
			case GL_UNSIGNED_BYTE:
				switch (channels) {
					case 1:
						switch(bits) {
							case 8:
								*iFormat = GL_R8;
								break;
							case 16:
								*iFormat = GL_R16;
								break;
							default:
								*iFormat = GL_RED;
								break;
						}
						break;
					case 2:
						switch(bits) {
							case 8:
								*iFormat = GL_RG8;
								break;
							case 16:
								*iFormat = GL_RG16;
								break;
							default:
								*iFormat = GL_RG;
								break;
						}
						break;
					case 3:
						switch(bits) {
							case 4:
								*iFormat = GL_RGB4;
								break;
							case 8:
								*iFormat = GL_RGB8;
								break;
							default:
								*iFormat = GL_RGB;
								break;
						}
						break;
					default:
						switch(bits) {
							case 4:
								*iFormat = GL_RGBA4;
								break;
							case 8:
								*iFormat = GL_RGBA8;
								break;
							case 16:
								*iFormat = GL_RGBA16;
								break;
							default:
								*iFormat = GL_RGBA;
								break;
						}
						break;
				}
				break;
			case GL_FLOAT:
				switch (channels) {
					case 1:
						switch(bits) {
							case 16:
								*iFormat = GL_R16F;
								break;
							case 32:
								*iFormat = GL_R32F;
								break;
							default:
								*iFormat = GL_RED;
								break;
						}
						break;
					case 2:
						switch(bits) {
							case 16:
								*iFormat = GL_RG16F;
								break;
							case 32:
								*iFormat = GL_RG32F;
								break;
							default:
								*iFormat = GL_RG;
								break;
						}
						break;
					case 3:
						switch(bits) {
							case 16:
								*iFormat = GL_RGB16F;
								break;
							case 32:
								*iFormat = GL_RGB32F;
								break;
							default:
								*iFormat = GL_RGB;
								break;
						}
						break;
					default:
						switch(bits) {
							case 16:
								*iFormat = GL_RGBA16F;
								break;
							case 32:
								*iFormat = GL_RGBA32F;
								break;
							default:
								*iFormat = GL_RGBA;
								break;
						}
						break;
				}
				break;
			case GL_INT:
				switch (channels) {
					case 1:
						switch(bits) {
							case 8:
								*iFormat = GL_R8I;
								break;
							case 16:
								*iFormat = GL_R16I;
								break;
							case 32:
								*iFormat = GL_R32I;
								break;
							default:
								*iFormat = GL_RED;
								break;
						}
						break;
					case 2:
						switch(bits) {
							case 8:
								*iFormat = GL_RG8I;
								break;
							case 16:
								*iFormat = GL_RG16I;
								break;
							case 32:
								*iFormat = GL_RG32I;
								break;
							default:
								*iFormat = GL_RG;
								break;
						}
						break;
					case 3:
						switch(bits) {
							case 8:
								*iFormat = GL_RGB8I;
								break;
							case 16:
								*iFormat = GL_RGB16I;
								break;
							case 32:
								*iFormat = GL_RGB32I;
								break;
							default:
								*iFormat = GL_RGB;
								break;
						}
						break;
					default:
						switch(bits) {
							case 8:
								*iFormat = GL_RGBA8I;
								break;
							case 16:
								*iFormat = GL_RGBA16I;
								break;
							case 32:
								*iFormat = GL_RGBA32I;
								break;
							default:
								*iFormat = GL_RGBA;
								break;
						}
						break;
				}
				break;
			default:
				switch (channels) {
					case 1:
						switch(bits) {
							case 8:
								*iFormat = GL_R8UI;
								break;
							case 16:
								*iFormat = GL_R16UI;
								break;
							case 32:
								*iFormat = GL_R32UI;
								break;
							default:
								*iFormat = GL_RED;
								break;
						}
						break;
					case 2:
						switch(bits) {
							case 8:
								*iFormat = GL_RG8UI;
								break;
							case 16:
								*iFormat = GL_RG16UI;
								break;
							case 32:
								*iFormat = GL_RG32UI;
								break;
							default:
								*iFormat = GL_RG;
								break;
						}
						break;
					case 3:
						switch(bits) {
							case 8:
								*iFormat = GL_RGB8UI;
								break;
							case 16:
								*iFormat = GL_RGB16UI;
								break;
							case 32:
								*iFormat = GL_RGB32UI;
								break;
							default:
								*iFormat = GL_RGB;
								break;
						}
						break;
					default:
						switch(bits) {
							case 8:
								*iFormat = GL_RGBA8UI;
								break;
							case 16:
								*iFormat = GL_RGBA16UI;
								break;
							case 32:
								*iFormat = GL_RGBA32UI;
								break;
							default:
								*iFormat = GL_RGBA;
								break;
						}
						break;
				}
				break;
		}
	}

	if(format != nullptr) {
		switch(channels) {
			case 1:
				*format = GL_RED;
				break;
			case 2:
				*format = GL_RG;
				break;
			case 3:
				*format = GL_RGB;
				break;
			default:
				*format = GL_RGBA;
				break;
		}
	}
}
