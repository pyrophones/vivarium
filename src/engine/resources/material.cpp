/*! \file material.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#include "material.h"

/*! \brief Material constructor
 *
 * \param (std::string) name - The name of the material
 * \param (std::shared_ptr<Textue>) albedo - The albedo texture of the material
 * \param (std::shared_ptr<Textue>) normal - The normal texture of the material
 * \param (std::shared_ptr<Textue>) metal - The metalness texture of the material
 * \param (std::shared_ptr<Textue>) rough - The roughness texture of the material
 * \param (std::shared_ptr<Textue>) ao - The ao texture of the material
 * \param (std::shared_ptr<Textue>) disp - The disp texture of the material
 */
viv::Material::Material(std::string name, std::shared_ptr<Texture> albedo, std::shared_ptr<Texture> normal, std::shared_ptr<Texture> metal, std::shared_ptr<Texture> rough, std::shared_ptr<Texture> ao, std::shared_ptr<Texture> disp, float parallaxStrength, float parallaxBias)
{
	this->name = name;
	this->albedo = albedo;
	this->normal = normal;
	this->metal = metal;
	this->rough = rough;
	this->ao = ao;
	this->disp = disp;
	this->parallaxStrength = parallaxStrength;
	this->parallaxBias = parallaxBias;
}

/*! \brief Material copy constructor
 *
 * \param (const Material &) mat - The material to copy
 */
viv::Material::Material(const Material &mat)
{
	this->name = mat.name;
	this->albedo = mat.albedo;
	this->normal = mat.normal;
	this->metal = mat.metal;
	this->rough = mat.rough;
	this->ao = mat.ao;
	this->disp = mat.disp;
	this->parallaxStrength = mat.parallaxStrength;
	this->parallaxBias = mat.parallaxBias;
}

/*! \brief Gets the material name
 *
 * \return (std::string) The name of the shader
 */
std::string viv::Material::getName() const
{
	return this->name;
}

/*! \brief Gets the materials albedo texture
 *
 * \return (Texture*) The albedo texture of the material
 */
const viv::Texture* viv::Material::getAlbedoTexture()
{
	return this->albedo.get();
}

/*! \brief Sets the materials albedo texture
 *
 * \param (std::shared_ptr<Texture>) texture - The texture to set the materials albedo texture to
 */
void viv::Material::setAlbedoTexture(std::shared_ptr<Texture> texture)
{
	this->albedo = texture;
}

/*! \brief Gets the materials normal texture
 *
 * \return (Texture*) The normal texture of the material
 */
const viv::Texture* viv::Material::getNormalTexture()
{
	return this->normal.get();
}

/*! \brief Sets the materials normal texture
 *
 * \param (std::shared_ptr<Texture>) texture - The texture to set the materials normal texture to
 */
void viv::Material::setNormalTexture(std::shared_ptr<Texture> texture)
{
	this->normal = texture;
}

/*! \brief Gets the materials metal texture
 *
 * \return (Texture*) The metal texture of the material
 */
const viv::Texture* viv::Material::getMetalTexture()
{
	return this->metal.get();
}

/*! \brief Sets the materials metal texture
 *
 * \param (std::shared_ptr<Texture>) texture - The texture to set the materials metal texture to
 */
void viv::Material::setMetalTexture(std::shared_ptr<Texture> texture)
{
	this->metal = texture;
}

/*! \brief Gets the materials roughness texture
 *
 * \return (Texture*) The roughness texture of the material
 */
const viv::Texture* viv::Material::getRoughnessTexture()
{
	return this->rough.get();
}

/*! \brief Sets the materials roughness texture
 *
 * \param (std::shared_ptr<Texture>) texture - The texture to set the materials roughness texture to
 */
void viv::Material::setRoughnessTexture(std::shared_ptr<Texture> texture)
{
	this->rough = texture;
}

/*! \brief Gets the materials ao texture
 *
 * \return (Texture*) The ao texture of the material
 */
const viv::Texture* viv::Material::getAoTexture()
{
	return this->ao.get();
}

/*! \brief Sets the materials ao texture
 *
 * \param (std::shared_ptr<Texture>) texture - The texture to set the materials ao texture to
 */
void viv::Material::setAoTexture(std::shared_ptr<Texture> texture)
{
	this->ao = texture;
}

/*! \brief Gets the materials displacement texture
 *
 * \return (Texture*) The displacement texture of the material
 */
const viv::Texture* viv::Material::getDispTexture()
{
	return this->disp.get();
}

/*! \brief Sets the materials displacement texture
 *
 * \param (std::shared_ptr<Texture>) texture - The texture to set the materials displacement texture to
 */
void viv::Material::setDispTexture(std::shared_ptr<Texture> texture)
{
	this->disp = texture;
}

/*! \brief Gets the parallax strength
 *
 * \return (float) The strength of the parallax effect
 */
float viv::Material::getParallaxStrength()
{
	return this->parallaxStrength;
}

/*! \brief Sets the parallax strength
 *
 * \param (float) parallaxStrength - The strength of the parallax effect
 */
void viv::Material::setParallaxStrength(float parallaxStrength)
{
	this->parallaxStrength = parallaxStrength;
}

/*! \brief Gets the parallax strength
 *
 * \return (float) The parallax bias
 */
float viv::Material::getParallaxBias()
{
	return this->parallaxBias;
}

/*! \brief Sets the parallax bias
 *
 * \param (float) parallaxBias - The parallax bias
 */
void viv::Material::setParallaxBias(float parallaxBias)
{
	this->parallaxBias = parallaxBias;
}

/*! \brief Material copy assigment operator
 *
 * \param (const Material &) mat - The material to copy
 *
 * \return (const Material &) This material after copying
 */
const viv::Material & viv::Material::operator=(const Material &mat)
{
	this->name = mat.name;
	this->albedo = mat.albedo;
	this->normal = mat.normal;
	this->metal = mat.metal;
	this->rough = mat.rough;
	this->ao = mat.ao;
	this->disp = mat.disp;
	this->parallaxStrength = mat.parallaxStrength;
	this->parallaxBias = mat.parallaxBias;

	return *this;
}

/*! \brief Material destructor
 */
viv::Material::~Material()
{

}
