/*! \file drawable.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "drawable.h"

/*! \brief Drawable default constructor
 */
viv::Drawable::Drawable()
{
	this->buffer = [](void) {
		printf("Function definition not set. Please set a function definition for buffer\n");
	};

	this->preDraw = [](void) {
		printf("Function definition not set. Please set a function definition for preparing to draw\n");
	};

	this->postDraw = [](void) {
		printf("Function definition not set. Please set a function definition for preparing to draw\n");
	};
}

/*! \brief Drawable data and shader constructor
 *
 * \param (Vertex*) vertices - The vertices of the drawable
 * \param (uint32_t*) indices - The indices of the drawable
 * \param (uint32_t*) offsets - The index offsets of the drawable
 * \param (uint32_t*) amounts - The index amounts of the drawable
 * \param (uint32_t) vertexCount - The vertex count
 * \param (uint32_t) indexCount - The index count
 * \param (std::shared_ptr<Shader>) shader - The shader to use for the drawable
 */
viv::Drawable::Drawable(Vertex* vertices, uint32_t* indices, uint32_t* offsets, uint32_t* amounts, uint32_t vertexCount, uint32_t indexCount, uint32_t offsetCount, uint32_t amountCount, std::shared_ptr<Shader> shader)
{
	this->md = std::shared_ptr<MeshData>(new MeshData(std::shared_ptr<Vertex[]>(vertices), std::shared_ptr<uint32_t[]>(indices), std::shared_ptr<uint32_t[]>(offsets), std::shared_ptr<uint32_t[]>(amounts), vertexCount, indexCount, offsetCount, amountCount));
	this->shad = std::shared_ptr<Shader>(shader);

	this->buffer = [](void) {
		printf("Function definition not set. Please set a function definition for buffer");
	};

	this->preDraw = [](void) {
		printf("Function definition not set. Please set a function definition for draw");
	};

	this->postDraw = [](void) {
		printf("Function definition not set. Please set a function definition for preparing to draw\n");
	};
}

/*! \brief Drawable mesh data, function, and shader constructor
 *
 * \param (std::shared_ptr<MeshData>) md - Shared pointer to the mesh data to pass
 * \param (std::function<void(void)>) bufferFunc - The buffer funtion for the drawable
 * \param (std::function<void(void)>) drawFunc - The draw funtion for the drawable
 * \param (std::shared_ptr<Shader>) shader - Pointer to the shader to use
 */
viv::Drawable::Drawable(std::shared_ptr<MeshData> md, std::function<void(void)> bufferFunc, std::function<void(void)>  preDrawFunc, std::function<void(void)> postDrawFunc, std::shared_ptr<Shader> shader)
{
	this->md = std::shared_ptr<MeshData>(md);
	this->buffer = bufferFunc;
	this->preDraw = preDrawFunc;
	this->postDraw = postDrawFunc;
	this->shad = std::shared_ptr<Shader>(shader);
}

/*! \brief Drawable mesh data and shader constructor
 *
 * \param (std::share_ptr<MeshData>) md - Shared pointer to the mesh data to pass
 * \param (std::shared_ptr<Shader>) shader - Pointer to the shader to use
 */
viv::Drawable::Drawable(std::shared_ptr<MeshData> md, std::shared_ptr<Shader> shader)
{
	this->md = std::shared_ptr<MeshData>(md);
	this->shad = shader;

	this->buffer = [](void) {
		printf("Function definition not set. Please set a function definition for buffer\n");
	};

	this->preDraw = [](void) {
		printf("Function definition not set. Please set a function definition for preDraw\n");
	};

	this->postDraw = [](void) {
		printf("Function definition not set. Please set a function definition for to postDraw\n");
	};
}

/*! \brief Drawable copy constructor
 *
 * \param (const Drawable &) d - The drawable to copy
 */
viv::Drawable::Drawable(const Drawable &d)
{
	this->md = std::shared_ptr<MeshData>(d.md);
	this->buffer = d.buffer;
	this->preDraw = d.preDraw;
	this->postDraw = d.postDraw;
	this->shad = std::shared_ptr<Shader>(d.shad);
	this->normalMats = d.normalMats;
	this->objTints = d.objTints;
	std::copy(d.mats, d.mats + 5, this->mats);
	std::memcpy(this->parallaxStrengths, d.parallaxStrengths, sizeof(float) * 5);
	std::memcpy(this->parallaxBiases, d.parallaxBiases, sizeof(float) * 5);
	this->matNums = d.matNums;
}

/** \brief Adds a world matrix to the list of matrices to render
 *
 * \param (Mat4 &) wMat - The world matrix to add
 * \param (Mat3 &) bMat - The normal matrix to add
 */
void viv::Drawable::addWorldAndNormalMat(Mat4 &wMat, Mat3 &nMat)
{
	this->worldMats.push_back(&wMat);
	this->normalMats.push_back(&nMat);

	if(this->shouldRender == false)
		this->shouldRender = true;
}

/** \brief Removes a world matrix from the list of matrices for rendering
 *
 * \param (Mat4 &) mat - The world matrix to remove
 */
//void viv::Drawable::removeWorldMat(Mat4 &mat)
//{
//	for(uint32_t i = 0; i < this->worldMats.size(); i++) {
//		if(this->worldMats[i] == &mat) {
//			this->worldMats.erase(this->worldMats.bthis->textureHandles = d.textureHandles;egin() + i);
//			break;
//		}
//	}
//}

/*! \brief Removes the last world matrix from the list of matrices for rendering
 */
void viv::Drawable::removeLastWorldAndNormalMat()
{
	this->worldMats.pop_back();
	this->normalMats.pop_back();
}

/*! \brief Adds a tint to the object
 *
 * \param (const Vec3 &) tint - The tint of the object
 */
void viv::Drawable::addObjectTint(const Vec3 &tint)
{
	this->objTints.push_back(&tint);
}

/*! \brief Removes the last tint from the list
 */
void viv::Drawable::removeLastTint()
{
	this->objTints.pop_back();
}

/*! \brief Add material to the mesh
 *
 * \param (std::shared_ptr<Material>) mat - The material to add
 */
void viv::Drawable::addMaterial(std::shared_ptr<Material> mat)
{
	for(uint32_t i = 0; i < 5; i++) {
		if(this->mats[i] == nullptr) {
			this->mats[i] = mat;
			this->parallaxStrengths[i] = mat->getParallaxStrength();
			this->parallaxBiases[i] = mat->getParallaxBias();
		}

		if(this->mats[i] == mat) {
			this->matNums.push_back(i);
			return;
		}
	}

	printf("Max amount of materials for mesh already met! Defaulting to material 0");
	this->matNums.push_back(0);
}

/*! \brief Remove material from the mesh
 *
 * \param (std::shared_ptr<Material>) mat - The material to remove
 */
void viv::Drawable::removeMaterial(std::shared_ptr<Material> mat)
{
	for(uint32_t i = 0; i < this->matNums.size(); i++) {
		if(this->mats[this->matNums[i]] == mat) {
			this->matNums.erase(this->matNums.begin() + i);
			i--;
		}
	}

	for(uint32_t i = 0; i < 5; i++) {
		if(this->mats[i] == mat) {
			this->mats[i] = nullptr;
			this->parallaxStrengths[i] = 0.0f;
			this->parallaxBiases[i] = 0.0f;
		}
	}
}

/*! \brief Gets the MeshData of the drawable
 *
 * \return (const MeshData*) - Pointer to the MeshData of the drawable
 */
const viv::MeshData* viv::Drawable::getMeshData() const
{
	return this->md.get();
}

/*! \brief Gets the shader of the drawable
 *
 * \return (Shader*) - The shader of the drawable
 */
viv::Shader* viv::Drawable::getShader() const
{
	return this->shad.get();
}

/*! \brief Gets the BufferCollection of the drawable
 *
 * \return (const BufferCollection*) - The BufferCollection of the drawable
 */
const viv::BufferCollection* viv::Drawable::getBufferCollection() const
{
	return this->bufferCollection;
}

/*! \brief Gets the count of the world matrices
 *
 * \return (uint32_t) The count of the world matrices
 */
uint32_t viv::Drawable::getWorldMatCount()
{
	return this->worldMats.size();
}

/*! \brief Sets the mesh data
 *
 * \param (std::shared_ptr<MeshData>) md - Shared pointer to the mesh data to set
 */
void viv::Drawable::setMeshData(std::shared_ptr<MeshData> md)
{
	this->md = std::shared_ptr<MeshData>(md);
}

/*! \brief Sets the drawable's shader
 *
 * \param (std::shared_ptr<Shader>) shader - Pointer to the shader to use
 */
void viv::Drawable::setShader(std::shared_ptr<Shader> shader)
{
	this->shad = std::shared_ptr<Shader>(shader);
}

/*! \brief Sets the drawables buffer collection
 *
 * \param (BufferCollection*) bc - Pointer to the buffer collection to set
 */
void viv::Drawable::setBufferCollection(BufferCollection* bc)
{
	this->bufferCollection = bc;
}

/*! \brief Drawable copy assignment operator
 *
 * \param (const Drawable &) d - The drawable to copy
 *
 * \return (const Drawable &) This drawable after copying
 */
const viv::Drawable & viv::Drawable::operator=(const Drawable &d)
{
	this->md = std::shared_ptr<MeshData>(d.md);
	this->buffer = d.buffer;
	this->preDraw = d.preDraw;
	this->postDraw = d.postDraw;
	this->shad = std::shared_ptr<Shader>(d.shad);
	this->worldMats = d.worldMats;
	this->normalMats = d.normalMats;
	this->objTints = d.objTints;
	std::copy(d.mats, d.mats + 5, this->mats);
	std::memcpy(this->parallaxStrengths, d.parallaxStrengths, sizeof(float) * 5);
	std::memcpy(this->parallaxBiases, d.parallaxBiases, sizeof(float) * 5);
	this->matNums = d.matNums;

	return *this;
}

/*! \brief Drawable destructor
 */
viv::Drawable::~Drawable()
{
	if(this->bufferCollection != nullptr)
		delete this->bufferCollection;
}
