/*! \file postProcess.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */
#include "postProcess.h"

//TODO: REMOVE THIS
#include "assetManager.h"

/*! \brief Post-process constructor
 *
 * \param (Shader*) s - The post processing shader to use
 * \param (uint32_t) priority - The priority of the post-process
 */
viv::PostProcess::PostProcess(const RenderBackend* backend, std::shared_ptr<Shader> s, int32_t priority, bool useDefaultDrawFunctions) : Drawable(AssetManager::getMeshData("fsQuad"), s)
{
	this->shouldRender = true;
	this->bufferCollection = backend->createBufferCollection(1, 1, 0);
	this->priority = priority;

	if(useDefaultDrawFunctions) {
		this->buffer = [this](void) {
			//Get buffer pointers
			const VertexBuffer* vbo = this->bufferCollection->getVertexBuffer(0);
			const IndexBuffer* ebo = this->bufferCollection->getIndexBuffer(0);
			//const UniformBuffer* ubo = this->bufferCollection->getUniformBuffer(0);

			//Bind the buffer colelction, Bind and set the EBO
			this->getBufferCollection()->bind();
			ebo->bind();
			ebo->bufferData(this->md->getIndexCount() * sizeof(uint32_t), this->md->getIndices(), GL_STATIC_DRAW);

			//Bind VBO1 (vertex data) and set it's attributes
			vbo->bind();
			vbo->bufferData(this->md->getVertexCount() * sizeof(Vertex), this->md->getVertices(), GL_STATIC_DRAW);
			vbo->bufferAttribute(this->shad->getAttributes()["vertex"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0, 0);
			vbo->bufferAttribute(this->shad->getAttributes()["uv"].index, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), offsetof(Vertex, uv), 0);
			vbo->unbind();

			this->bufferCollection->unbind(); //Unbind the buffer collection

			//Bind and set UBO1 (Camera matrices)
			//if(ubo != nullptr) {
			//	ubo->bind();
			//	ubo->bufferData(sizeof(Vec4), nullptr, GL_DYNAMIC_DRAW);
			//	ubo->bindBufferBase(this->getShader()->getUniformBlocks()["ScreenInfo"].blockBinding);
			//	ubo->unbind();
			//}
		};

		this->preDraw = [this](void) {
			this->shad->use();

			//Update the camera matrix buffer
			//const UniformBuffer* ubo = this->getBufferCollection()->getUniformBuffer(0);
			//if(ubo != nullptr) {
			//}

			this->bufferCollection->bind();
		};

		this->postDraw = [this](void) {
			this->bufferCollection->unbind();
		};

		this->buffer();
	}
}

/*! \brief Post-process copy constructor
 *
 * \param (const PostProcess &) pp - The post process to copy
 */
viv::PostProcess::PostProcess(const PostProcess &pp) : Drawable(pp)
{
	this->priority = pp.priority;
}

/*! \brief Gets the post processes priority
 *
 * \return (int32_t) The priority of the post-process
 */
int32_t viv::PostProcess::getPriority()
{
	return priority;
}

/*! \brief Sets the post-processes priority
 *
 * \return (int32_t) priority - The priority of the post-process
 */
void viv::PostProcess::setPriority(int32_t priority)
{
	this->priority = priority;
}

/*! \brief Post-process copy assignment operator
 *
 * \param (const PostProcess &) pp - The post-process to copy
 *
 * \return (const PostProcess &) This post-process after assignment
 */
const viv::PostProcess & viv::PostProcess::operator=(const PostProcess &pp)
{
	Drawable::operator=(pp);
	this->priority = pp.priority;
	return *this;
}

/*! \brief Post-process equality operator
 *
 * \param (const PostProcess &) pp - The post-process to check against
 *
 * \return (bool) Whether the post-processes are equal
 */
bool viv::PostProcess::operator==(const PostProcess &pp) const
{
	return (this->priority == pp.priority);
}

/*! \brief Post-process inequality operator
 *
 * \param (const PostProcess &) pp - The post-process to check against
 *
 * \return (bool) Whether the post-processes are not equal
 */
bool viv::PostProcess::operator!=(const PostProcess &pp) const
{
	return !(*this == pp);
}

/*! \brief Post-process greater than operator
 *
 * \param (const PostProcess &) pp - The post-process to check against
 *
 * \return (bool) Whether the post-processes is greater than this
 */
bool viv::PostProcess::operator>(const PostProcess &pp) const
{
	return (this->priority > pp.priority);
}

/*! \brief Post-process less than operator
 *
 * \param (const PostProcess &) pp - The post-process to check against
 *
 * \return (bool) Whether the post-process is less than this
 */
bool viv::PostProcess::operator<(const PostProcess &pp) const
{
	return (this->priority < pp.priority);
}

/*! \brief Post-process greater than or equal to operator
 *
 * \param (const PostProcess &) pp - The post-process to check against
 *
 * \return (bool) Whether the post-processes is greater than or equal to this
 */
bool viv::PostProcess::operator>=(const PostProcess &pp) const
{
	return (this->priority >= pp.priority);
}

/*! \brief Post-process less than or equal to operator
 *
 * \param (const PostProcess &) pp - The post-process to check against
 *
 * \return (bool) Whether the post-process is less than or equal to this
 */
bool viv::PostProcess::operator<=(const PostProcess &pp) const
{
	return (this->priority <= pp.priority);
}

/*! \brief Post-process destructor
 */
viv::PostProcess::~PostProcess()
{

}

/*! \brief Compares two post processes priorities
 *
 * \param (std::shared_ptr<PostProcess>) pp1 - The first post process to compare
 * \param (std::shared_ptr<PostProcess>) pp2 - The second post process to compare
 */
bool viv::PostProcess::compare(std::shared_ptr<PostProcess> pp1, std::shared_ptr<PostProcess> pp2)
{
	return (pp1->priority < pp2->priority);
}
