/*! \file svg.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#include "svg.h"

viv::Svg::Svg() : Model()
{
	isTextured = false;
}

viv::Svg::Svg(const char* path) : Model()
{
	isTextured = false;

	for(uint32_t i = 0; i < this->vertices.size(); i++) {
		this->verts[i] = this->vertices[i];
	}

	this->read(path);
	this->buffer();
}

void viv::Svg::read(const char* path)
{
	std::ifstream stream;
	size_t size;
	char* src;

	stream.exceptions(std::ios::badbit | std::ios::failbit);

	stream.open(path, std::ios::binary);

	if(!stream.is_open()) {
		printf("\tFailed to open svg file!\n");
		return;
	}

	stream.seekg(0, std::ios::end);
	size = stream.tellg();
	stream.seekg(0, std::ios::beg);

	src = new char[size + 1];
	stream.read(src, size);
	src[size] = 0;

	stream.close();

	std::string srcStr = src;
	delete[] src;

	Vec2 tmpVerts;
	std::vector<Vec2> verts;
	Mat4 tmpMat;
	char tmpChar;
	std::string tmpStr;
	float sWidth;
	float sHeight;
	std::stringstream ss;

	rapidxml::xml_document<> doc;
	doc.parse<0>(&srcStr[0]);
	rapidxml::xml_node<>* node = doc.first_node("svg");
	rapidxml::xml_attribute<>* attr = node->first_attribute();

	do {
		if(attr->name() ==  "width")
			sscanf(attr->value(), "%f", &sWidth);
		if(attr->name() ==  "height")
			sscanf(attr->value(), "%f", &sHeight);
	}
	while(attr = attr->next_attribute());

	node = node->first_node("g");
	rapidxml::xml_node<>* subNode = node->first_node("path");
	do {
		do {
			attr = subNode->first_attribute();
			do {
					if((std::string)attr->name() ==  "transform") {
						ss.str(attr->value());
						while(ss.get(tmpChar)) {
							if(tmpChar ==  '(') {
								ss >> tmpVerts.x;
								ss >> tmpChar;
								ss >> tmpVerts.y;
								tmpMat = Mat4::translation(Vec3(tmpVerts.x, tmpVerts.y, 0));
								break;
							}
						}
					}

					if((std::string)attr->name() ==  "fill") {
						tmpStr = attr->value();
						ss.get(tmpChar);

						char[3] hexChar;
						char[2] = 0;

						hexChar[0] = ss.get();
						hexChar[1] = ss.get();
						tmpVerts.x = strtol(hexChar, nullptr, 16);
						hexChar[0] = ss.get();
						hexChar[1] = ss.get();
						tmpVerts.y = strtol(hexChar, nullptr, 16);
						hexChar[0] = ss.get();
						hexChar[1] = ss.get();
						tmpVerts.z = strtol(hexChar, nullptr, 16);
						this->color = tmpVerts;
					}

					if((std::string)attr->name() ==  "d") {
						tmpStr = attr->value();

					}
			} while(attr = attr->next_attribute());
		} while(subNode = subNode->next_sibling());
	} while(node = node->next_sibling());

	//ss.str("");
	//ss.clear();

	for(uint32_t i = 0; i < verts.size(); i++) {
		this->vertices.push_back(verts[i].x);
		this->vertices.push_back(verts[i].y);
		this->vertices.push_back(0.0f);

		this->vertices.push_back(0.0f);
		this->vertices.push_back(0.0f);

		this->vertices.push_back(0.0f);
		this->vertices.push_back(0.0f);
		this->vertices.push_back(0.0f);
	}

	this->count = indicies.size();

	this->verts = new float[this->vertices.size()];
}

