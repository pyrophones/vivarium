/*! \file script.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "script.h"

#include "system.h"
std::unordered_map<std::string, std::unordered_map<std::string, asIScriptFunction*>> viv::Script::controllers;
std::unordered_map<std::string, asIScriptContext*> viv::Script::contexts;

/*! \brief Scipt constructor
 *
 * \param (const char*) path - The path to the script
 * \param (const char*) name - The name for the script
 */
viv::Script::Script(const char* data, const char* name)
{
	this->name = name;
	this->loadScript(data);
}

/*! \brief Scipt copy constructor
 *
 * \param (const Script &) s - The script to copy
 */
viv::Script::Script(const Script &s)
{
	this->name = s.name;
}

/*! \brief Loads a script
 *
 * \param (const char*) path - The path to the script
 */
void viv::Script::loadScript(const char* data)
{
	asIScriptModule* mod = System::getEngine()->GetModule(this->name.c_str(), asGM_ONLY_IF_EXISTS);

	if(mod) {
		printf("module already exists, skipping\n");
		return;
	}

	mod = System::getEngine()->GetModule(this->name.c_str(), asGM_CREATE_IF_NOT_EXISTS);
	mod->AddScriptSection(this->name.c_str(), data);

	if(mod->Build() < 0) {
		printf("file failed to compile!\n");
		return;
	}

	for(uint32_t i = 0; i < mod->GetFunctionCount(); i++) {
		asIScriptFunction* func = mod->GetFunctionByIndex(i);
		Script::controllers[this->name][std::string(func->GetName())] = func;
	}

	//asITypeInfo* type = mod->GetTypeInfo("ClassName");

	//for(uint32_t i = 0; i < mod->GetMethodCount(); i++) {
	//	asIScriptFunction* method = mod->GetMethodByIndex(i);
	//	controllers[scriptName][(char*)method->GetName()] = method;
	//}

	Script::contexts[this->name] = System::getEngine()->CreateContext();
}

/*! \brief Calls the specified script function within the script
 *
 * \param (const char*) funcName - The name of the function to execute
 */
void viv::Script::callFunc(const char* funcName)
{
	if(Script::controllers[this->name].find(funcName) == Script::controllers[this->name].end()) {
		printf("Error: Function '%s' could not be loaded from script '%s'", funcName, this->name.c_str());
		return;
	}

	Script::contexts[this->name]->Prepare(Script::controllers[this->name][funcName]);
	Script::contexts[this->name]->Execute();
}

/*! \brief Script copy-assignment operator
 *
 * \param (const Script &) s - The script to copy
 *
 * \return (const Script &) This script after copying
 */
const viv::Script & viv::Script::operator=(const Script &s)
{
	this->name = s.name;

	return *this;
}

/*! \brief Script destructor
 */
viv::Script::~Script()
{
	if(Script::controllers.find(this->name) != Script::controllers.end()) {
		for(auto i : Script::controllers[this->name]) {
				i.second->Release();
		}
	}

	if(Script::contexts.find(this->name) != Script::contexts.end())
		Script::contexts[this->name]->Release();
}
