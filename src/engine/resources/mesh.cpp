/*! \file mesh.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "mesh.h"
#include "game.h"

/*! \brief Constructor with path for a mesh
 *
 * \param (const RenderBackend*) backend - The rendering backend to use
 * \param (std::shared_ptr<MeshData> md) md - The meshdata for the mesh to use
 */
viv::Mesh::Mesh(const RenderBackend* backend, std::shared_ptr<Shader> shader, std::shared_ptr<MeshData> md) : Drawable(md, shader)
{
	this->bufferCollection = backend->createBufferCollection(5, 1, 2);

	this->buffer = [this](void) {
		//Get buffer pointers
		const VertexBuffer* vbo1 = this->bufferCollection->getVertexBuffer(0);
		const VertexBuffer* vbo2 = this->bufferCollection->getVertexBuffer(1);
		const VertexBuffer* vbo3 = this->bufferCollection->getVertexBuffer(2);
		const VertexBuffer* vbo4 = this->bufferCollection->getVertexBuffer(3);
		const VertexBuffer* vbo5 = this->bufferCollection->getVertexBuffer(4);
		const IndexBuffer* ebo = this->bufferCollection->getIndexBuffer(0);
		const UniformBuffer* ubo1 = this->bufferCollection->getUniformBuffer(0);
		const UniformBuffer* ubo2 = this->bufferCollection->getUniformBuffer(1);

		//Bind the buffer colelction, Bind and set the EBO
		this->bufferCollection->bind();
		ebo->bind();
		ebo->bufferData(this->md->getIndexCount() * sizeof(uint32_t), this->md->getIndices(), GL_STATIC_DRAW);

		//Bind VBO1 (vertex data) and set it's attributes
		vbo1->bind();
		vbo1->bufferData(this->md->getVertexCount() * sizeof(Vertex), this->md->getVertices(), GL_STATIC_DRAW);
		vbo1->bufferAttribute(this->shad->getAttributes()["position"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0, 0);
		vbo1->bufferAttribute(this->shad->getAttributes()["uv"].index, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), offsetof(Vertex, uv), 0);
		vbo1->bufferAttribute(this->shad->getAttributes()["normal"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), offsetof(Vertex, norm), 0);
		vbo1->bufferAttribute(this->shad->getAttributes()["tan"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), offsetof(Vertex, tan), 0);
		vbo1->bufferAttribute(this->shad->getAttributes()["bitan"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), offsetof(Vertex, bitan), 0);
		vbo1->unbind();

		//Bind VBO2 (Instance material number) and set it's attributes
		vbo2->bind();
		vbo2->bufferData(sizeof(uint32_t), nullptr, GL_DYNAMIC_DRAW);
		vbo2->bufferAttribute(this->shad->getAttributes()["matNum"].index, 1, GL_UNSIGNED_INT, GL_FALSE, sizeof(uint32_t), 0, 1);
		vbo2->unbind();

		//Bind VBO3 (Instance material properties) and set it's attributes
		vbo3->bind();
		vbo3->bufferData(sizeof(Vec3), nullptr, GL_DYNAMIC_DRAW);
		vbo3->bufferAttribute(this->shad->getAttributes()["tint"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vec3), 0, 1);
		vbo3->unbind();

		//Get the world matrix locations
		if(this->shad->getAttributes().find("worldMat") != this->shad->getAttributes().end() && vbo4 != nullptr) {
			int32_t worldLoc = this->shad->getAttributes()["worldMat"].index;

			//Bind VBO4 (World Matrix instance data) and set it's attributes
			vbo4->bind();
			vbo4->bufferData(sizeof(Mat4), nullptr, GL_DYNAMIC_DRAW);
			for(uint32_t i = 0; i < 4; i++) {
				vbo4->bufferAttribute(worldLoc + i, 4, GL_FLOAT, GL_FALSE, sizeof(Mat4), i * sizeof(Vec4), 1);
			}
			vbo4->unbind();
		}

		if(this->shad->getAttributes().find("normalMat") != this->shad->getAttributes().end() && vbo5 != nullptr) {
			int32_t normalLoc = this->shad->getAttributes()["normalMat"].index;

			//Bind VBO5 (Normal Matrix instance data) and set it's attributes
			vbo5->bind();
			vbo5->bufferData(sizeof(Mat3), nullptr, GL_DYNAMIC_DRAW);
			for(uint32_t i = 0; i < 3; i++) {
				 vbo5->bufferAttribute(normalLoc + 3 + i, 3, GL_FLOAT, GL_FALSE, sizeof(Mat3), i * sizeof(Vec3), 1);
			}
			vbo5->unbind();
		}

		this->bufferCollection->unbind(); //Unbind the buffer collection

		//Bind and set UBO1 (Camera matrices)
		if(ubo1 != nullptr) {
			ubo1->bind();
			ubo1->bufferData(2 * sizeof(Mat4), nullptr, GL_DYNAMIC_DRAW);
			ubo1->bindBufferBase(this->shad->getUniformBlocks()["CamMats"].blockBinding);
			ubo1->unbind();
		}

		//Bind and set UBO2 (Cam position)
		if(ubo2 != nullptr) {
			ubo2->bind();
			ubo2->bufferData(11 * sizeof(Vec4), nullptr, GL_DYNAMIC_DRAW);
			ubo2->bindBufferBase(this->shad->getUniformBlocks()["ParallaxInfo"].blockBinding);
			ubo2->unbind();
		}
	};

	this->preDraw = [this](void) {
		this->shad->use();

		Vec3 tints[this->objTints.size()];
		Mat4 wMats[this->worldMats.size()];
		Mat3 nMats[this->normalMats.size()];

		for(uint32_t i = 0; i < this->objTints.size(); i++)
			tints[i] = *this->objTints[i];

		for(uint32_t i = 0; i < this->worldMats.size(); i++)
			wMats[i] = *this->worldMats[i];

		for(uint32_t i = 0; i < this->normalMats.size(); i++)
			nMats[i] = *this->normalMats[i];

		//Update the camera matrix buffer
		const UniformBuffer* ubo1 = this->bufferCollection->getUniformBuffer(0);
		if(ubo1 != nullptr) {
			ubo1->bindBufferBase(this->shad->getUniformBlocks()["CamMats"].blockBinding);
			ubo1->bind();
			ubo1->bufferSubData(0, sizeof(Mat4), &Game::getActiveCam()->getViewMat());
			ubo1->bufferSubData(sizeof(Mat4), sizeof(Mat4), &Game::getActiveCam()->getProjMat());
			ubo1->unbind();
		}

		const UniformBuffer* ubo2 = this->bufferCollection->getUniformBuffer(1);
		if(ubo2 != nullptr) {
			ubo2->bindBufferBase(this->shad->getUniformBlocks()["ParallaxInfo"].blockBinding);
			ubo2->bind();
			ubo2->bufferSubData(0, sizeof(Vec3), (const void*)&Game::getActiveCam()->getPos());
			//std140 pads each value a float array to make it sizeof(Vec4)
			ubo2->bufferSubData(sizeof(Vec4), sizeof(float), (const void*)&this->parallaxStrengths[0]);
			ubo2->bufferSubData(2 * sizeof(Vec4), sizeof(float), (const void*)&this->parallaxStrengths[1]);
			ubo2->bufferSubData(3 * sizeof(Vec4), sizeof(float), (const void*)&this->parallaxStrengths[2]);
			ubo2->bufferSubData(4 * sizeof(Vec4), sizeof(float), (const void*)&this->parallaxStrengths[3]);
			ubo2->bufferSubData(5 * sizeof(Vec4), sizeof(float), (const void*)&this->parallaxStrengths[4]);
			ubo2->bufferSubData(6 * sizeof(Vec4), sizeof(float), (const void*)&this->parallaxBiases[0]);
			ubo2->bufferSubData(7 * sizeof(Vec4), sizeof(float), (const void*)&this->parallaxBiases[1]);
			ubo2->bufferSubData(8 * sizeof(Vec4), sizeof(float), (const void*)&this->parallaxBiases[2]);
			ubo2->bufferSubData(9 * sizeof(Vec4), sizeof(float), (const void*)&this->parallaxBiases[3]);
			ubo2->bufferSubData(10 * sizeof(Vec4), sizeof(float), (const void*)&this->parallaxBiases[4]);
			ubo2->unbind();
		}

		//Bind and update the instance data buffers
		this->bufferCollection->bind();
		const VertexBuffer* vbo2 = this->bufferCollection->getVertexBuffer(1);
		if(vbo2 != nullptr) {
			vbo2->bind();
			vbo2->bufferData(this->matNums.size() * sizeof(uint32_t), (const void*)&this->matNums[0], GL_DYNAMIC_DRAW);
			vbo2->unbind();
		}

		const VertexBuffer* vbo3 = this->bufferCollection->getVertexBuffer(2);
		if(vbo3 != nullptr) {
			vbo3->bind();
			vbo3->bufferData(this->objTints.size() * sizeof(Vec3), (const void*)&tints[0], GL_DYNAMIC_DRAW);
			vbo3->unbind();
		}

		const VertexBuffer* vbo4 = this->bufferCollection->getVertexBuffer(3);
		if(vbo4 != nullptr) {
			vbo4->bind();
			vbo4->bufferData(this->worldMats.size() * sizeof(Mat4), (const void*)&wMats[0], GL_DYNAMIC_DRAW);
			vbo4->unbind();
		}

		const VertexBuffer* vbo5 = this->bufferCollection->getVertexBuffer(4);
		if(vbo5 != nullptr) {
			vbo5->bind();
			vbo5->bufferData(this->normalMats.size() * sizeof(Mat3), (const void*)&nMats[0], GL_DYNAMIC_DRAW);
			vbo5->unbind();
		}

		//Bind Textures
		for(uint32_t i = 0; i < 5; i++) {
			if(mats[i] == nullptr)
				continue;
			mats[i]->getAlbedoTexture()->bind(i);
			mats[i]->getNormalTexture()->bind(i + 5);
			mats[i]->getMetalTexture()->bind(i + 10);
			mats[i]->getRoughnessTexture()->bind(i + 15);
			mats[i]->getAoTexture()->bind(i + 20);
			mats[i]->getDispTexture()->bind(i + 25);
		}

		//Set Texture Uniforms
		for(uint32_t i = 0; i < 5; i++) {
			this->shad->setUniformInt1("albedoTex[" + std::to_string(i) + "]", i);
			this->shad->setUniformInt1("normalTex[" + std::to_string(i) + "]", i + 5);
			this->shad->setUniformInt1("metalTex[" + std::to_string(i) + "]", i + 10);
			this->shad->setUniformInt1("roughTex[" + std::to_string(i) + "]", i + 15);
			this->shad->setUniformInt1("aoTex[" + std::to_string(i) + "]", i + 20);
			this->shad->setUniformInt1("dispTex[" + std::to_string(i) + "]", i + 25);
		}
	};

	this->postDraw = [this](void) {
		for(uint32_t i = 0; i < 5; i++) {
			if(mats[i] == nullptr)
				continue;
			mats[i]->getAlbedoTexture()->unbind(i);
			mats[i]->getNormalTexture()->unbind(i + 5);
			mats[i]->getMetalTexture()->unbind(i + 10);
			mats[i]->getRoughnessTexture()->unbind(i + 15);
			mats[i]->getAoTexture()->unbind(i + 20);
			mats[i]->getDispTexture()->unbind(i + 25);
		}
		this->bufferCollection->unbind();
	};

	this->buffer();
}

/*! \brief Mesh Copy Constructor
 *
 * \param (const Mesh &) m - The mesh to copy
 */
viv::Mesh::Mesh(const Mesh &m) : Drawable(m)
{
	this->buffer();
}

/*! \brief Mesh copy constructor
 *
 * \param (const Mesh &) m - The mesh to copy
 *
 * \return (Mesh &) This instance
 */
const viv::Mesh & viv::Mesh::operator=(const Mesh &m)
{
	Drawable::operator=(m);
	this->buffer();

	return *this;
}

/*! \brief Mesh destructor
 */
viv::Mesh::~Mesh()
{

}
