/*! \file entity.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "entityManager.h"
#include "entity.h"

std::unique_ptr<viv::EntityManager> viv::EntityManager::instance = std::unique_ptr<viv::EntityManager>(nullptr);
std::vector<viv::Entity> viv::EntityManager::entities;
std::vector<int32_t> viv::EntityManager::entityRef;
std::vector<uint32_t> viv::EntityManager::deletedEntities;

const viv::EntityManager* viv::EntityManager::getInstance()
{
	if(EntityManager::instance == nullptr) {
		printf("Creating entity manager singleton. . .");
		EntityManager::instance = std::unique_ptr<EntityManager>(new EntityManager());
		printf("ok\n");
	}

	return EntityManager::instance.get();
}

void viv::EntityManager::refresh()
{
	for(uint32_t i = 0; i < EntityManager::deletedEntities.size(); i++)
		EntityManager::entityDelete(EntityManager::deletedEntities[i]);
}

void viv::EntityManager::setEntityCount(uint32_t entityCount)
{
	EntityManager::entities.clear();
	EntityManager::entityRef.clear();
	EntityManager::entities.reserve(entityCount);
	EntityManager::entityRef.reserve(entityCount);
}

viv::Entity viv::EntityManager::createEntity(std::string tag)
{
	EntityManager::entities.push_back(Entity(tag));

	for(uint32_t i = 0; i < EntityManager::entityRef.size(); i++) {
		if(EntityManager::entityRef[i] == -1) {
			EntityManager::entityRef[i] = entities.size() - 1;
			EntityManager::entities.back().setHandle(i);
			return EntityManager::entities.back();
		}
	}

	EntityManager::entityRef.push_back(entities.size() - 1);
	EntityManager::entities.back().setHandle(entityRef.size() - 1);
	return EntityManager::entities.back();

	//for(int i = 0; i < entities.size(); i++) {
	//	if(entities[i].getComponentMask() == Components::COMPONENT_NONE) {
	//		return entities[i];
	//	}
	//}

	//printf("Error: Max entities created!")
	//return entities.size();
}

viv::Entity viv::EntityManager::getEntity(uint32_t handle)
{
	if(handle > EntityManager::entityRef.size() || EntityManager::entityRef[handle] == -1)
		return Entity();

	return EntityManager::entities[EntityManager::entityRef[handle]];
}

void viv::EntityManager::destroyEntity(uint32_t handle, bool instant)
{
	if(!instant) {
		EntityManager::deletedEntities.push_back(handle);
		return;
	}

	EntityManager::entityDelete(handle);
}

void viv::EntityManager::clearEntities()
{
	EntityManager::entities.clear();
}

void viv::EntityManager::deleteInstance()
{
	if(EntityManager::instance != nullptr)
		EntityManager::instance.reset();
}

void viv::EntityManager::entityDelete(uint32_t handle)
{
	if(EntityManager::entityRef[handle] == -1)
		return;

	if(entityNum != EntityManager::entities.size() - 1) {
		std::swap(EntityManager::entities[entityRef[handle]], EntityManager::entities.back());
	}

	EntityManager::entities.pop_back();
	EntityManager::entityRef.back() = entityRef[handle];
	//EntityManager::entities[entityRef.back()].setHandle(handle);
	EntityManager::entityRef[handle] = -1;
}

viv::EntityManager::~EntityManager()
{

}