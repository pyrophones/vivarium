/*! \file assetManager.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "assetManager.h"
#include "system.h"

std::unordered_map<std::string, std::shared_ptr<viv::MeshData>> viv::AssetManager::loadedMeshData; //Holds loaded mesh data
std::unordered_map<std::string, std::shared_ptr<viv::Mesh>> viv::AssetManager::loadedMeshes; //Holds loaded meshes
std::unordered_map<std::string, std::shared_ptr<viv::Texture>> viv::AssetManager::loadedTextures; //Holds loaded textures
std::unordered_map<std::string, std::shared_ptr<viv::Script>> viv::AssetManager::loadedScripts; //Holds loaded scripts
std::unordered_map<std::string, std::shared_ptr<viv::Material>> viv::AssetManager::loadedMaterials; //Holds loaded material
std::vector<std::shared_ptr<viv::PostProcess>> viv::AssetManager::loadedPostProcesses; //Holds loaded post processes
std::unordered_map<std::string, std::shared_ptr<viv::Shader>> viv::AssetManager::loadedShaders; //Holds loaded material
std::unique_ptr<viv::AssetManager> viv::AssetManager::instance = std::unique_ptr<viv::AssetManager>(nullptr); //The current AssetManager instance
const viv::RenderBackend* viv::AssetManager::backend = nullptr;

/*! \brief Asset loader constructor
 */
viv::AssetManager::AssetManager()
{

}

/*! \brief Initializes the assetmanager and sets the rendering backend
 *
 * \param (const RenderBackend*) backend - The rendering backend to use
 */
void viv::AssetManager::init(const RenderBackend* backend)
{
	AssetManager::backend = backend;
	AssetManager::createPrimatives();
	FileLoader::setLoaderProperties();
}

/*! \brief Creates the game engines primatives
 */
void viv::AssetManager::createPrimatives()
{
	MeshData fsQuad(std::shared_ptr<Vertex[]>(new Vertex[3] {
								{ Vec3(-1.0f,  1.0f, 0.0f), Vec2( 0.0f,  1.0f), Vec3(), Vec3(), Vec3() },
								{ Vec3(-1.0f, -3.0f, 0.0f), Vec2( 0.0f, -1.0f), Vec3(), Vec3(), Vec3() },
								{ Vec3( 3.0f,  1.0f, 0.0f), Vec2( 2.0f,  1.0f), Vec3(), Vec3(), Vec3() } }),
					std::shared_ptr<uint32_t[]>(new uint32_t[3] { 0, 1, 2 }),
					std::shared_ptr<uint32_t[]>(new uint32_t[1] { 0 }),
					std::shared_ptr<uint32_t[]>(new uint32_t[1] { 3 }),
					3, 3, 1, 1);
	AssetManager::createMeshData("fsQuad", fsQuad);

	Vertex quadVerts[4] = { { Vec3(-1.0f, 1.0f, 0.0f), Vec2(0.0f, 1.0f), Vec3(0.0f, 0.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f) },
							{ Vec3(-1.0f,-1.0f, 0.0f), Vec2(0.0f, 0.0f), Vec3(0.0f, 0.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f) },
							{ Vec3( 1.0f,-1.0f, 0.0f), Vec2(1.0f, 0.0f), Vec3(0.0f, 0.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f) },
							{ Vec3( 1.0f, 1.0f, 0.0f), Vec2(1.0f, 1.0f), Vec3(0.0f, 0.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f) } };

	//Vertex quadVerts[4] = { { Vec3(-1.0f, 1.0f, 0.0f), Vec2(0.0f, 0.0f), Vec3(0.0f, 0.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f) },
	//						{ Vec3(-1.0f,-1.0f, 0.0f), Vec2(0.0f, 1.0f), Vec3(0.0f, 0.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f) },
	//						{ Vec3( 1.0f,-1.0f, 0.0f), Vec2(1.0f, 1.0f), Vec3(0.0f, 0.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f) },
	//						{ Vec3( 1.0f, 1.0f, 0.0f), Vec2(1.0f, 0.0f), Vec3(0.0f, 0.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f) } };

	Vertex* v1 = &quadVerts[0];
	Vertex* v2 = &quadVerts[1];
	Vertex* v3 = &quadVerts[2];
	Vertex* v4 = &quadVerts[3];

	Vec3 vec1 = v2->pos - v1->pos;
	Vec3 vec2 = v3->pos - v1->pos;

	float s1 = v2->uv.x - v1->uv.x;
	float t1 = v2->uv.y - v1->uv.y;

	float s2 = v3->uv.x - v1->uv.x;
	float t2 = v3->uv.y - v1->uv.y;

	float r = 1.0f / (s1 * t2 - s2 * t1);

	Vec3 tan1 = (vec1 * t2 - vec2 * t1) * r;
	Vec3 bitan1 = (vec2 * s1 - vec1 * s2) * r;

	quadVerts[0].tan += tan1;
	quadVerts[1].tan += tan1;
	quadVerts[2].tan += tan1;

	quadVerts[0].bitan += bitan1;
	quadVerts[1].bitan += bitan1;
	quadVerts[2].bitan += bitan1;

	vec1 = v3->pos - v1->pos;
	vec2 = v4->pos - v1->pos;

	s1 = v3->uv.x - v1->uv.x;
	t1 = v3->uv.y - v1->uv.y;

	s2 = v4->uv.x - v1->uv.x;
	t2 = v4->uv.y - v1->uv.y;

	r = 1.0f / (s1 * t2 - s2 * t1);

	Vec3 tan2 = (vec1 * t2 - vec2 * t1) * r;
	Vec3 bitan2 = (vec2 * s1 - vec1 * s2) * r;

	quadVerts[0].tan += tan2;
	quadVerts[2].tan += tan2;
	quadVerts[3].tan += tan2;

	quadVerts[0].bitan += bitan2;
	quadVerts[2].bitan += bitan2;
	quadVerts[3].bitan += bitan2;

	for(uint32_t i = 0; i < 4; i++) {
		quadVerts[i].tan = glm::normalize(quadVerts[i].tan - glm::dot(quadVerts[i].tan, quadVerts[i].norm) * quadVerts[i].norm);

		if(quadVerts[i].tan != quadVerts[i].tan) {
			quadVerts[i].tan = glm::cross(quadVerts[i].norm, Vec3(1.0f, 0.0, 0.0f));
		}

		quadVerts[i].bitan = glm::normalize(quadVerts[i].bitan - glm::dot(quadVerts[i].bitan, quadVerts[i].norm) * quadVerts[i].norm - glm::dot(quadVerts[i].bitan, quadVerts[i].tan) * quadVerts[i].tan);

		if(quadVerts[i].bitan != quadVerts[i].bitan) {
			quadVerts[i].bitan = glm::cross(quadVerts[i].norm, quadVerts[i].tan);
		}
	}

	MeshData quad(std::shared_ptr<Vertex[]>(new Vertex[4] {
											{ quadVerts[0] },
											{ quadVerts[1] },
											{ quadVerts[2] },
											{ quadVerts[3] } }),
				  std::shared_ptr<uint32_t[]>(new uint32_t[6] { 0, 1, 2, 0, 2, 3 }),
				  std::shared_ptr<uint32_t[]>(new uint32_t[1] { 0 }),
				  std::shared_ptr<uint32_t[]>(new uint32_t[1] { 6 }),
				  4, 6, 1, 1);

	AssetManager::createMeshData("quad", quad);
}

/*! \brief Gets the current AssetManager instance
 *
 * \return (std::unique_ptr<AssetManager>) the AssetManager instance
 */
viv::AssetManager* viv::AssetManager::getInstance()
{
	if(AssetManager::instance == nullptr) {
		printf("Creating AssetManager singleton . . . ");
		AssetManager::instance = std::unique_ptr<AssetManager>(new AssetManager);
	}

	printf("ok\n");
	return AssetManager::instance.get();
}

/*! \brief Gets the mesh data with the specified id
 *
 * \param (char*) id - The id of the mesh data in the map
 *
 * \return (std::shared_ptr<MeshData>) Pointer to the retrieved mesh data
 */
std::shared_ptr<viv::MeshData> viv::AssetManager::getMeshData(const char* id)
{
	auto meshData = AssetManager::loadedMeshData.find(id);
	if(meshData != AssetManager::loadedMeshData.end())
		return meshData->second;

	return nullptr;
}

/*! \brief Gets the mesh with the specified id
 *
 * \param (char*) id - The id of the mesh in the map
 *
 * \return (std::shared_ptr<Mesh>) Pointer to the retrieved mesh
 */
std::shared_ptr<viv::Mesh> viv::AssetManager::getMesh(const char* id)
{
	auto mesh = AssetManager::loadedMeshes.find(id);
	if(mesh != AssetManager::loadedMeshes.end())
		return mesh->second;

	return nullptr;
}

/*! \brief Gets the texture with the specified id
 *
 * \param (char*) id - The id of the texture in the map
 *
 * \return (std::shared_ptr<Texture>) Pointer to the retrieved texture
 */
std::shared_ptr<viv::Texture> viv::AssetManager::getTexture(const char* id)
{
	auto tex = AssetManager::loadedTextures.find(id);
	if(tex != AssetManager::loadedTextures.end())
		return tex->second;

	return nullptr;
}

/*! \brief Gets the script with the specified id
 *
 * \param (char*) id - The id of the script in the map
 *
 * \return (std::shared_ptr<Script>) Pointer to the retrieved script
 */
std::shared_ptr<viv::Script> viv::AssetManager::getScript(const char* id)
{
	auto script = AssetManager::loadedScripts.find(id);
	if(script != AssetManager::loadedScripts.end())
		return script->second;

	return nullptr;
}

/*! \brief Gets the material with the specified id
 *
 * \param (char*) id - The id of the material in the map
 *
 * \return (std::shared_ptr<Material>) Pointer to the retrieved material
 */
std::shared_ptr<viv::Material> viv::AssetManager::getMaterial(const char* id)
{
	auto mat = AssetManager::loadedMaterials.find(id);
	if(mat != AssetManager::loadedMaterials.end())
		return mat->second;

	return nullptr;
}

/*! \brief Gets the shader with the specified id
 *
 * \param (char*) id - The id of the shader in the map
 *
 * \return (std::shared_ptr<Shader>) Pointer to the retrieved Shader
 */
std::shared_ptr<viv::Shader> viv::AssetManager::getShader(const char* id)
{
	auto shad = AssetManager::loadedShaders.find(id);
	if(shad != AssetManager::loadedShaders.end())
		return shad->second;

	return nullptr;
}

/*! \brief Return the map of meshs
 *
 * \return (std::unordered_map<std::string, std::shared_ptr<viv::Mesh>>) The unordered map of meshs
 */
std::unordered_map<std::string, std::shared_ptr<viv::Mesh>> viv::AssetManager::getAllMeshes()
{
	return AssetManager::loadedMeshes;
}

/*! \brief Return the map of post processes
 *
 * \return (std::vector<std::shared_ptr<viv::PostProcess>>) The unordered map of post processes
 */
std::vector<std::shared_ptr<viv::PostProcess>> viv::AssetManager::getAllPostProcesses()
{
	return AssetManager::loadedPostProcesses;
}

/*! \brief UNIMPLEMENTED. Loads a dynamic library.
 *
 * \param (char*) path - The path to the library
 */
void viv::AssetManager::loadLibrary(const char* path)
{
//	void* so = dlopen(path, RTLD_NOW);
//	if(so != nullptr) {
//		loadedLibs.push_back(so);
//
//		LibUpdate up = dlsym(so, "update");
//		libUpdates.push_back(up);
//	}
}

/*! \brief Loads MeshData into the asset manager
 *
 * \param (const char*) name - The name for the mesh data
 * \param (const MeshData &) md - The mesh data to store
 */
void viv::AssetManager::createMeshData(const char* name, const MeshData &md)
{
	AssetManager::loadedMeshData[name] = std::shared_ptr<MeshData>(new MeshData(md));
}

/*! \brief Loads a Mesh into the asset manager
 *
 * \param (const char*) name - The name for the mesh
 * \param (const char*) meshDataName - The name of the mesh data to use
 * \param (const char*) shaderName - The name of the shader to use
 */
void viv::AssetManager::createMesh(const char* name, const char* meshDataName, const char* shaderName)
{
	AssetManager::loadedMeshes[name] = std::shared_ptr<Mesh>(new Mesh(AssetManager::backend, AssetManager::loadedShaders[shaderName], AssetManager::loadedMeshData[meshDataName]));
}

/*! \brief Loads a Texture into the asset manager
 *
 * \param (const char*) name - The name for the texture
 * \param (int32_t) width - The width of the texture
 * \param (int32_t) height - The height of the texture
 * \param (int32_t) channels - The amount of channels the texture has
 * \param (uint32_t) bits - The amount of bits the texture uses
 * \param (uint32_t) type - The data type of the texture
 * \param (uint32_t) clampType - The type of clamping the texture should use
 * \param (uint32_t) minFilter - The type of minification filtering the texture should use
 * \param (uint32_t) magFilter - The type of magnification filtering the texture should use
 * \param (const void*) data - The actual texture data
 * \param (bool) genMipMaps - If mipmaps should be generated for the texture
 */
void viv::AssetManager::createTexture(const char* name, int32_t width, int32_t height, int32_t channels, uint32_t bits, uint32_t type, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void* data, bool genMipMaps)
{
	AssetManager::loadedTextures[name] = std::shared_ptr<Texture>(AssetManager::backend->createTexture(width, height, channels, bits, type, clampType, minFilter, magFilter, data, genMipMaps));
}

/*! \brief Loads a Material into the asset manager
 *
 * \param (const char*) name - The name for the material
 * \param (const char*) albedo - The name of the albedo texture to use
 * \param (const char*) normal - The name of the normal texture to use
 * \param (const char*) metal - The name of the metal texture to use
 * \param (const char*) rough - The name of the roughness texture to use
 * \param (const char*) ao - The name of the ambient occlusion texture to use
 * \param (const char*) disp - The name of the displacement texture to use
 */
void viv::AssetManager::createMaterial(const char* name, const char* albedo, const char* normal, const char* metal, const char* roughness, const char* ao, const char* disp)
{
	AssetManager::loadedMaterials[name] = std::shared_ptr<Material>(new Material(name,
																				 AssetManager::loadedTextures[albedo],
																				 AssetManager::loadedTextures[normal],
																				 AssetManager::loadedTextures[metal],
																				 AssetManager::loadedTextures[roughness],
																				 AssetManager::loadedTextures[ao],
																				 AssetManager::loadedTextures[disp]));
}

/*! \brief Loads a PostProcess into the asset manager
 *
 * \param (const char*) shaderName - The name for the shader and the name of the post-process
 * \param (int32_t) priority - The priority of the post-process in the queue
 * \param (bool) useDefaultDrawFunctions - If the postprocess should use default draw functions
 */
void viv::AssetManager::createPostProcess(const char* shaderName, int32_t priority, bool useDefaultDrawFunctions)
{
	AssetManager::loadedPostProcesses.push_back(std::shared_ptr<PostProcess>(new PostProcess(AssetManager::backend, AssetManager::loadedShaders[shaderName], priority, useDefaultDrawFunctions)));
	std::sort(AssetManager::loadedPostProcesses.begin(), AssetManager::loadedPostProcesses.end(), PostProcess::compare);
}

/*! \brief Loads selected file and puts it in the right map
 *
 * \param (char*) path - The path of the file
 * \param (FileType) f - The type of file to be loaded
 * \param (std::string) name - The name that will be used to store it in the map
 */
void viv::AssetManager::loadAsset(const char* path, FileType f, std::string name)
{
	switch(f) {
		case FileType::MESH: {
			if(AssetManager::loadedMeshes.find(name) == AssetManager::loadedMeshes.end()) {
				std::shared_ptr<MeshData> data = FileLoader::loadMeshData(path);

				if(data != nullptr) {
					AssetManager::loadedMeshData[name] = data;
					AssetManager::createMesh(name.c_str(), name.c_str(), "defaultShader");
				}
			}
			break;
		}

		case FileType::TEXTURE: {
			if(AssetManager::loadedTextures.find(name) == AssetManager::loadedTextures.end()) {
				int32_t width, height, channels;
				unsigned char* data = nullptr;
				FileLoader::loadTextureData(path, data, &width, &height, &channels);
				AssetManager::createTexture(name.c_str(), width, height, channels, 8, GL_UNSIGNED_BYTE, GL_REPEAT, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, (const void*)data, true);
				FileLoader::deleteTextureData(data);
			}
			break;
		}

		case FileType::SCRIPT: {
			if(AssetManager::loadedScripts.find(name) == AssetManager::loadedScripts.end()) {
				char* data = nullptr;
				FileLoader::loadText(path, data);
				AssetManager::loadedScripts[name] = std::shared_ptr<Script>(new Script(data, name.c_str()));
				FileLoader::deleteText(data);
			}
			break;
		}

		case FileType::TEXTURE_HDR: {
			if(AssetManager::loadedTextures.find(name) == AssetManager::loadedTextures.end()) {
				int32_t width, height, channels;
				float* data = nullptr;
				FileLoader::loadTextureHDR(path, data, &width, &height, &channels);
				AssetManager::createTexture(name.c_str(), width, height, channels, 16, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, (const void*)data, true);
				FileLoader::deleteTextureHDR(data);
			}
			break;
		}

		case FileType::MATERIAL: {
			std::map<std::string, std::pair<std::string, std::string>> matProps;
			std::string matName;
			FileLoader::loadMaterialData(path, &matName, &matProps);
			if(AssetManager::loadedMaterials.find(matName) == AssetManager::loadedMaterials.end()) {
				for(auto i = matProps.begin(); i != matProps.end(); i++) {
					if(i->second.first == "null" ||
						(AssetManager::loadedTextures.find(i->second.first) == AssetManager::loadedTextures.end() &&
						 i->second.second == "null")) {
						if(i->first == "albedo")
							i->second.first = "default_albedo";
						else if(i->first == "normal")
							i->second.first = "default_normal";
						else if(i->first == "metal")
							i->second.first = "default_metal0";
						else if(i->first == "rough")
							i->second.first = "default_roughness9";
						else if(i->first == "ao")
							i->second.first = "default_ao";
						else if(i->first == "disp")
							i->second.first = "default_disp";
					}

					else if(AssetManager::loadedTextures.find(i->second.first) == AssetManager::loadedTextures.end()) {
						int32_t width, height, channels;
						unsigned char* data = nullptr;
						FileLoader::loadTextureData(i->second.second.c_str(), data, &width, &height, &channels);
						AssetManager::createTexture(i->second.first.c_str(), width, height, channels, 8, GL_UNSIGNED_BYTE, GL_REPEAT, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, (const void*)data, true);
						FileLoader::deleteTextureData(data);
					}
				}

				AssetManager::loadedMaterials[matName] = std::shared_ptr<Material>(new Material(matName,
																				   AssetManager::loadedTextures[matProps["albedo"].first],
																				   AssetManager::loadedTextures[matProps["normal"].first],
																				   AssetManager::loadedTextures[matProps["metal"].first],
																				   AssetManager::loadedTextures[matProps["rough"].first],
																				   AssetManager::loadedTextures[matProps["ao"].first],
																				   AssetManager::loadedTextures[matProps["disp"].first]));
			}
			break;
		}

		case FileType::SHADER: {
			std::string shadName;
			std::map<std::string, std::string> shaderFiles;
			FileLoader::loadShader(path, &shadName, &shaderFiles);
			if(AssetManager::loadedShaders.find(shadName) == AssetManager::loadedShaders.end()) {
				AssetManager::loadedShaders[shadName] = std::shared_ptr<Shader>(new Shader(shadName));

				Shader::ShaderType type;
				for(auto i = shaderFiles.begin(); i != shaderFiles.end(); i++) {
					if(i->second == "null")
						continue;

					if(i->first == "vert")
						type = Shader::ShaderType::VERT;
					else if(i->first == "tesc")
						type = Shader::ShaderType::TESC;
					else if(i->first == "tese")
						type = Shader::ShaderType::TESE;
					else if(i->first == "geom")
						type = Shader::ShaderType::GEOM;
					else if(i->first == "frag")
						type = Shader::ShaderType::FRAG;
					else if(i->first == "comp")
						type = Shader::ShaderType::COMP;

					char* text = nullptr;
					FileLoader::loadText(i->second.c_str(), text);
					AssetManager::loadedShaders[shadName]->loadShader(text, type);
					FileLoader::deleteText(text);
				}
				AssetManager::loadedShaders[shadName]->linkProgram();
			}
			break;
		}

		case FileType::SCENE: {

			break;
		}

		default:
			break;
	}
}

/*! \brief Loads the specified file / folder
 *
 * \param (char*) path - The path of the file or folder
 */
void viv::AssetManager::loadDirectory(const char* path)
{
	char* modPath = (char*)malloc(sizeof(char) * (strlen(path) + 1));
	strcpy(modPath, path);
	DIR* inDir = opendir(path);

	if(inDir != nullptr) {
		struct dirent* dFile;
		while((dFile = readdir(inDir))) {
			if(strcmp(dFile->d_name, ".") == 0 || strcmp(dFile->d_name, "..") == 0)
				continue;
			size_t size = strlen(modPath) + strlen(dFile->d_name) + 2;
			char tmp[size];
			std::fill(tmp, tmp + size - 1, '\0');
			strcat(tmp, modPath);
			strcat(tmp, "/");
			strcat(tmp, dFile->d_name);
			tmp[size - 1] = 0;

			AssetManager::loadDirectory(tmp);
		}
	}

	else {
		char* ext = strrchr(modPath, '.');
		char* tmp = strrchr(modPath, '/');
		char name[strlen(tmp)];
		strcpy(name, tmp);
		strtok(name, ".");
		memmove(name, name + 1, strlen(name));

		for(uint32_t i = 1; ext[i]; i++){
			ext[i] = std::tolower(ext[i]);
		}

		if(strcmp(ext, ".obj") == 0 || strcmp(ext, ".fbx") == 0 || strcmp(ext, ".dae") == 0 || strcmp(ext, ".ogex") == 0 ||
		   strcmp(ext, ".blend") == 0) {
			AssetManager::loadAsset(modPath, FileType::MESH, name);
		}

		else if(strcmp(ext, ".png") == 0 || strcmp(ext, ".jpg") == 0 || strcmp(ext, ".tga") == 0) {
			AssetManager::loadAsset(modPath, FileType::TEXTURE, name);
		}

		else if(strcmp(ext, ".hdr") == 0) {
			AssetManager::loadAsset(modPath, FileType::TEXTURE_HDR, name);
		}

		else if(strcmp(ext, ".as") == 0) {
			AssetManager::loadAsset(modPath, FileType::SCRIPT, name);
		}

		else if(strcmp(ext, ".mat") == 0) {
			AssetManager::loadAsset(modPath, FileType::MATERIAL, name);
		}

		else if(strcmp(ext, ".sdr") == 0) {
			AssetManager::loadAsset(modPath, FileType::SHADER, name);
		}

		else if(strcmp(ext, ".scn") == 0 || strcmp(ext, ".scene") == 0) {
			AssetManager::loadAsset(modPath, FileType::SCENE, name);
		}
	}

	closedir(inDir);
	free(modPath);
}

/*! \brief Deletes the AssetManager instance
 */
void viv::AssetManager::deleteInstance()
{
	if(AssetManager::instance != nullptr)
		AssetManager::instance.reset();
}

/*! \brief Asset loader destructor
 */
viv::AssetManager::~AssetManager()
{

}