/*! \file renderBackend.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "renderBackend.h"

/*! \brief Default constructor for RenderBackend
 */
viv::RenderBackend::RenderBackend()
{

}

/*! \brief RenderBackend copy constructor
 *
 * \param (const RenderBackend &) rb - The RenderBackend to copy
 */
viv::RenderBackend::RenderBackend(const RenderBackend &rb)
{
	this->curShader = rb.curShader;
}

/*! \brief Sets the current shader to use
 *
 * \param (Shader*) shader - The current active shader
 */
void viv::RenderBackend::setCurrentShader(Shader* shader)
{
	this->curShader = shader;
}

/*! \brief RenderBackend copy assigment operator
 *
 * \param (const RenderBackend &) rb - The RenderBackend to copy
 *
 * \return (const viv::RenderBackend &) This render backend after assignment
 */
const viv::RenderBackend & viv::RenderBackend::operator=(const RenderBackend &rb)
{
	this->curShader = rb.curShader;

	return *this;
}

/*! \brief RenderBackend default destructor
 */
viv::RenderBackend::~RenderBackend()
{

}
