/*! \file shader.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "shader.h"

uint32_t viv::Shader::bindPoint = 0;

/*! \brief Shader constructor
 */
viv::Shader::Shader(std::string name)
{
	this->name = name;
	this->program = glCreateProgram();
}

/*! \brief Shader copy constructor
 *
 * \param (const Shader &) shader - The shader to copy
 */
viv::Shader::Shader(const Shader &shader)
{
	this->name = shader.name;
	this->program = shader.program;
}

/*! \brief Loads a shader
 *
 * \param (const char*) shaderCode - The code of the vertex shader
 *
 */
void viv::Shader::loadShader(const char* shaderCode, ShaderType type)
{
	uint32_t shader;
	int32_t success;
	char infoLog[512];

	switch (type) {
		case ShaderType::VERT:
			shader = glCreateShader(GL_VERTEX_SHADER);
			break;
		case ShaderType::TESC:
			shader = glCreateShader(GL_TESS_CONTROL_SHADER);
			break;
		case ShaderType::TESE:
			shader = glCreateShader(GL_TESS_EVALUATION_SHADER);
			break;
		case ShaderType::GEOM:
			shader = glCreateShader(GL_GEOMETRY_SHADER);
			break;
		case ShaderType::FRAG:
			shader = glCreateShader(GL_FRAGMENT_SHADER);
			break;
		case ShaderType::COMP:
			shader = glCreateShader(GL_COMPUTE_SHADER);
			break;
	}

	glShaderSource(shader, 1, &shaderCode, nullptr);
	glCompileShader(shader);

	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

	if(!success) {
		glGetShaderInfoLog(shader, 512, nullptr, infoLog);
		printf("\tError: Shader compilation error: %s\n", infoLog);
	}

	glAttachShader(this->program, shader);
	glDeleteShader(shader);
}

/*! \brief Links the shader program
 */
void viv::Shader::linkProgram()
{
	int32_t success;
	char infoLog[512];

	glLinkProgram(this->program);
	glGetProgramiv(this->program, GL_LINK_STATUS, &success);

	if(!success) {
		glGetProgramInfoLog((this->program), 512, nullptr, infoLog);
		printf("\tError: Program linking failed: %s\n", infoLog);
	}

	this->updateLocations();
}

/*! \brief Updates the locations of variables in the shader
 */
void viv::Shader::updateLocations()
{
	this->attributes.clear();
	this->uniformBlocks.clear();
	this->uniforms.clear();

	//Get vertex data and add it to a map
	glGetProgramiv(this->program, GL_ACTIVE_ATTRIBUTES, &this->attributeCount);
	char name[50];

	this->attributes.reserve(this->attributeCount);
	size_t prevSize = 0;
	size_t prevOffset = 0;
	for(int32_t i = 0; i < this->attributeCount; i++) {
		AttributeData data = {};
		data.index = i;
		glGetActiveAttrib(this->program, i, 50, nullptr, &data.size, nullptr, name);
		data.offset = prevSize * sizeof(float) + prevOffset;

		this->attributes[name] = data;
		prevSize = data.size;
		prevOffset = data.offset;
	}

	//TODO: Fix uniforms not being properly updated
	glGetProgramiv(this->program, GL_ACTIVE_UNIFORMS, &this->uniformCount);
	this->uniforms.reserve(this->uniformCount);
	for(int32_t i = 0; i < this->uniformCount; i++) {
		UniformData data = {};
		uint32_t type;
		glGetActiveUniform(this->program, i, 0, nullptr, &data.size, &type, name);
		data.location = glGetUniformLocation(this->program, name);
		this->uniforms[name] = data;
	}

	glGetProgramiv(this->program, GL_ACTIVE_UNIFORM_BLOCKS, &this->uniformBlockCount);
	this->uniformBlocks.reserve(this->uniformBlockCount);
	for(int32_t i = 0; i < this->uniformBlockCount; i++) {
		BufferBlockData data = {};
		data.blockBinding = Shader::bindPoint++;
		glGetActiveUniformBlockName(this->program, i, 50, nullptr, name);
		glGetActiveUniformBlockiv(this->program, i, GL_UNIFORM_BLOCK_DATA_SIZE, &data.size);
		glUniformBlockBinding(this->program, i, data.blockBinding);
		this->uniformBlocks[name] = data;
	}

	glGetProgramInterfaceiv(this->program, GL_SHADER_STORAGE_BLOCK, GL_ACTIVE_RESOURCES, &this->shaderStorageBlockCount);
	this->shaderStorageBlocks.reserve(this->shaderStorageBlockCount);
	for(int32_t i = 0; i < this->shaderStorageBlockCount; i++) {
		const uint32_t propNums = { GL_BUFFER_DATA_SIZE };
		int32_t props = 0;
		glGetProgramResourceName(this->program, GL_SHADER_STORAGE_BLOCK, i, 50, nullptr, name);
		glGetProgramResourceiv(this->program, GL_SHADER_STORAGE_BLOCK, i, 1, &propNums, 1, nullptr, &props);
		BufferBlockData data = {};
		data.blockBinding = Shader::bindPoint++;
		data.size = props;
		glShaderStorageBlockBinding(this->program, i, data.blockBinding);
		this->shaderStorageBlocks[name] = data;
	}
}

/*! \brief Sets a float uniform variable
 *
 * \param (std::string) name - The name of the uniform variable
 * \param (uint32_t) size - The size of the data
 * \param (const float*) data - Pointer to the data
 */
void viv::Shader::setUniformFloat1(std::string name, float data)
{
	this->use();
	glUniform1f(glGetUniformLocation(this->program, name.c_str()), data);
}

/*! \brief Sets an int uniform variable
 *
 * \param (std::string) name - The name of the uniform variable
 * \param (uint32_t) size - The size of the data
 * \param (const int32_t*) data - Pointer to the data
 */
void viv::Shader::setUniformInt1(std::string name, int32_t data)
{
	this->use();
	int32_t loc = glGetUniformLocation(this->program, name.c_str());
	glUniform1i(loc, data);
}

/*! \brief Sets an unsigned int uniform variable
 *
 * \param (std::string) name - The name of the uniform variable
 * \param (uint32_t) size - The size of the data
 * \param (const uint32_t*) data - Pointer to the data
 */
void viv::Shader::setUniformUint1(std::string name, uint32_t data)
{
	this->use();
	glUniform1ui(glGetUniformLocation(this->program, name.c_str()), data);
}

/*! \brief Sets a float pointer uniform variable
 *
 * \param (std::string) name - The name of the uniform variable
 * \param (uint32_t) size - The size of the data
 * \param (const float*) data - Pointer to the data
 */
void viv::Shader::setUniformFloatPtr(std::string name, uint32_t uniformNum, uint32_t size, const float* data)
{
	switch(uniformNum){
		case 2:
			glUniform2fv(glGetUniformLocation(this->program, name.c_str()), size, data);
			break;
		case 3:
			glUniform3fv(glGetUniformLocation(this->program, name.c_str()), size, data);
			break;
		case 4:
			glUniform4fv(glGetUniformLocation(this->program, name.c_str()), size, data);
			break;
		case 1: //This is down here to make use of the fallthrough
		default:
			glUniform1fv(glGetUniformLocation(this->program, name.c_str()), size, data);
			break;
	}
}

/*! \brief Sets an int pointer uniform variable
 *
 * \param (std::string) name - The name of the uniform variable
 * \param (uint32_t) size - The size of the data
 * \param (const int32_t*) data - Pointer to the data
 */
void viv::Shader::setUniformIntPtr(std::string name, uint32_t uniformNum, uint32_t size, const int32_t* data)
{
	switch(uniformNum){
		case 2:
			glUniform2iv(glGetUniformLocation(this->program, name.c_str()), size, data);
			break;
		case 3:
			glUniform3iv(glGetUniformLocation(this->program, name.c_str()), size, data);
			break;
		case 4:
			glUniform4iv(glGetUniformLocation(this->program, name.c_str()), size, data);
			break;
		case 1: //This is down here to make use of the fallthrough
		default:
			glUniform1iv(glGetUniformLocation(this->program, name.c_str()), size, data);
			break;
	}
}

/*! \brief Sets an unsigned int pointer uniform variable
 *
 * \param (std::string) name - The name of the uniform variable
 * \param (uint32_t) size - The size of the data
 * \param (const uint32_t*) data - Pointer to the data
 */
void viv::Shader::setUniformUintPtr(std::string name, uint32_t uniformNum, uint32_t size, const uint32_t* data)
{
	switch(uniformNum){
		case 2:
			glUniform2uiv(glGetUniformLocation(this->program, name.c_str()), size, data);
			break;
		case 3:
			glUniform3uiv(glGetUniformLocation(this->program, name.c_str()), size, data);
			break;
		case 4:
			glUniform4uiv(glGetUniformLocation(this->program, name.c_str()), size, data);
			break;
		case 1: //This is down here to make use of the fallthrough
		default:
			glUniform1uiv(glGetUniformLocation(this->program, name.c_str()), size, data);
			break;
	}
}

/*! \brief Gets the map of vertex attributes
 *
 * \return (std::unordered_map<std::string, Shader::AttributeData> &) The attribute map
 */
std::unordered_map<std::string, viv::Shader::AttributeData> & viv::Shader::getAttributes()
{
	return this->attributes;
}

/*! \brief Gets the map of uniform buffers
 *
 * \return (std::unordered_map<std::string, BufferBlockData> &) The uniform block map
 */
std::unordered_map<std::string, viv::Shader::BufferBlockData> & viv::Shader::getUniformBlocks()
{
	return this->uniformBlocks;
}

/*! \brief Gets the map of shader storage block buffers
 *
 * \return (std::unordered_map<std::string, BufferBlockData> &) The shader storage block map
 */
std::unordered_map<std::string, viv::Shader::BufferBlockData> & viv::Shader::getShaderStorageBlocks()
{
	return this->shaderStorageBlocks;
}

/*! \brief Gets the shader program
 *
 * \return (uint32_t) The shader program
 */
uint32_t viv::Shader::getProgram() const
{
	return this->program;
}

/*! \brief Gets the shader name
 *
 * \return (std::string) The name of the shader
 */
std::string viv::Shader::getName() const
{
	return this->name;
}

/*! \brief Uses the shader program
 */
void viv::Shader::use()
{
	glUseProgram(this->program);
}

/*! \brief Shader copy assignment operator
 *
 * \param (const Shader &) shader - The shader to copy
 *
 * \return (const Shader &) This shader after copying
 */
const viv::Shader & viv::Shader::operator=(const Shader &shader)
{
	this->name = shader.name;
	this->program = shader.program;

	return *this;
}

/*! \brief Shader destructor
 */
viv::Shader::~Shader()
{
	uint32_t shaders[6] = {};
	int32_t count = 0;

	glGetAttachedShaders(this->program, 6, &count, shaders);

	for(int i = 0; i < count; i++) {
		glDetachShader(this->program, shaders[i]);
	}

	glDeleteProgram(this->program);
}
