/*! \file glVertexBuffer.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */
#include "glVertexBuffer.h"

/*! \brief Default GLVertexBuffer constructor
 *
 * \param (const void*) data - The data for the OpenGL vertex buffer to use
 */
viv::GLVertexBuffer::GLVertexBuffer() : Buffer(), GLBuffer(), VertexBuffer()
{

}

/*! \brief GLVertexBuffer copy constructor
 *
 * \param (const GLVertexBuffer &) b - The OpenGL vertex buffer to copy from
 */
viv::GLVertexBuffer::GLVertexBuffer(const GLVertexBuffer &gvb) : Buffer(gvb), GLBuffer(gvb), VertexBuffer(gvb)
{

}

/*! \brief Binds the vertex buffer
 */
void viv::GLVertexBuffer::bind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, this->bufferId);
}

/*! \brief unbinds the buffer
 */
void viv::GLVertexBuffer::unbind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

/*! \brief Adds data to the vertex buffers
 */
void viv::GLVertexBuffer::bufferData(size_t size, const void* data, bool dynamic) const
{
	glBufferData(GL_ARRAY_BUFFER, size, data, dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
}

/*! \brief Adds Subdata to the buffer
 *
 * \param (uint32_t) offset - The offset of the data
 * \param (uint32_t) size - The size of the data
 * \param (const void*) subData - The subdata
 */
void viv::GLVertexBuffer::bufferSubData(uint32_t offset, uint32_t size, const void* subData) const
{
	glBufferSubData(GL_ARRAY_BUFFER, offset, size, subData);
}

/*! \brief Sets an attribute for the buffer using the location
 *
 * \param (uint32_t) location - The location of the attribute in the shader
 * \param (uint32_t) size - The size of the attribute in bytes
 * \param (uint32_t) type - The type of the attribute as a GLenum
 * \param (bool) normalized - If the attribute should be normalized
 * \param (uint32_t) stride - The stride of attribute in bytes
 * \param (uint64_t) offset - The offset of the variable in bytes
 * \param (uint32_t) divisor - The attribute divisor for instancing
 */
void viv::GLVertexBuffer::bufferAttribute(uint32_t location, uint32_t size, uint32_t type, bool normalized, uint32_t stride, uint64_t offset, uint32_t divisor) const
{
	glEnableVertexAttribArray(location);
	if(type == GL_DOUBLE || type == GL_UNSIGNED_INT64_ARB)
		glVertexAttribLPointer(location, size, type, stride, (const void*)offset);
	else if(type == GL_UNSIGNED_INT)
		glVertexAttribIPointer(location, size, type, stride, (const void*)offset);
	else
		glVertexAttribPointer(location, size, type, normalized ? GL_FALSE : GL_TRUE, stride, (const void*)offset);
	glVertexAttribDivisor(location, divisor);
}

/*! \brief GLVertexBuffer copy assigment operator
 *
 * \param (const GLVertexBuffer &) b - The OpenGL vertex buffer to copy from
 *
 * \return (const GLVertexBuffer &) This OpenGL vertex buffer after copying
 */
const viv::GLVertexBuffer & viv::GLVertexBuffer::operator=(const GLVertexBuffer &gvb)
{
	Buffer::operator=(gvb);
	GLBuffer::operator=(gvb);
	VertexBuffer::operator=(gvb);

	return *this;
}

/*! \brief Default GLVertexBuffer destructor
 */
viv::GLVertexBuffer::~GLVertexBuffer()
{

}

