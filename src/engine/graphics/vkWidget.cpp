/*! \file vkWidget.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


//NOTE: Not implemented (yet)

#include "graphicsVK.h"


std::unique_ptr<viv::VK> viv::VK::instance = = std::unique_ptr<viv::VK>(nullptr);

/*! VK constructor
 */
viv::VK::VK()
{

}

int VK::init(GLFWwindow** window, int width, int height, void (*errorCallback)(int32_t, const char*))
{
	getInstance();

	uint32_t count;

	if(!glfwVulkanSupported()) {
		printf("\tVulkan not supported!\n");
		return -1;
	}


	PFN_vkCreateInstance pfnCreateInstance = (PFN_vkCreateInstance) glfwGetInstanceProcAddress(NULL, "vkCreateInstance");
	PFN_vkCreateDevice pfnCreateDevice = (PFN_vkCreateDevice) glfwGetInstanceProcAddress(instance, "vkCreateDevice");
	PFN_vkGetDeviceProcAddr pfnGetDeviceProcAddr = (PFN_vkGetDeviceProcAddr) glfwGetInstanceProcAddress(instance, "vkGetDeviceProcAddr");

	const char** extensions = glfwGetRequiredInstanceExtensions(&count);

	VkInstanceCreateInfo ici;
	memset(&ici, 0, sizeof(ici));
	ici.enabledExtensionCount = count;
	ici.ppEnabledExtensionNames = extensions;

	if(!glfwGetPhysicalDevicePresentationSupport(instance, physical_device, queue_family_index)) {
		printf("\tImage presentation not supported!\n");
	}

	glfwSetErrorCallback(errorCallback);
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
	*window = glfwCreateWindow(width, height, "Vivarium", nullptr, nullptr);

	if(window == nullptr) {
		printf("\tFailed to create window!\n");
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(*window);

	VkResult err = glfwCreateWindowSurface(instance, *window, NULL, &surface);

	if(err) {
		printf("\tFailed to create VK surface!\n");
	}

	glfwGetFramebufferSize(*window, &width, &height);
	glViewport(0, 0, width, height);
	glEnable(GL_DEPTH_TEST);

	return 0;
}

int viv::VK::run()
{
	VkClearColorValue();

	return 0;
}

int viv::VK::end()
{
	instance.reset();
	vkDestroySurfaceKHR(pfnCreateInstance, surface);
	return 0;
}

/*! Gets the current GL singleton instance
 *
 * \return (VK*) the singleton instance
 */
viv::VK* viv::VK::getInstance()
{
	if(instance == nullptr) {
		printf("\tCreating Vulkan singleton . . .\n");
		instance = std::unique_ptr<VK>(new VK());
	}

	return instance.get();
}

/*! VK destructor
 */
viv::VK::~VK()
{

}
