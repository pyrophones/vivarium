/*! \file buffer.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "buffer.h"

/*! \brief Default buffer constructor
 *
 * \param (const void*) data - The data for the buffer to use
 */
viv::Buffer::Buffer()
{

}

/*! \brief Buffer copy constructor
 *
 * \param (const Buffer &) b - The buffer to copy from
 */
viv::Buffer::Buffer(const Buffer &b)
{

}

/*! \brief Buffer copy assigment operator
 *
 * \param (const Buffer &) b - The buffer to copy from
 *
 * \return (const Buffer &) This buffer after copying
 */
const viv::Buffer & viv::Buffer::operator=(const Buffer &b)
{
	return *this;
}

/*! \brief Default buffer destructor
 */
viv::Buffer::~Buffer()
{

}
