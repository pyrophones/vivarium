/*! \file glIndexBuffer.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */
#include "glIndexBuffer.h"

/*! \brief Default GLIndexBuffer constructor
 *
 * \param (const void*) data - The data for the OpenGL index buffer to use
 */
viv::GLIndexBuffer::GLIndexBuffer() : Buffer(), GLBuffer(), IndexBuffer()
{


}

/*! \brief GLIndexBuffer copy constructor
 *
 * \param (const GLIndexBuffer &) b - The OpenGL index buffer to copy from
 */
viv::GLIndexBuffer::GLIndexBuffer(const GLIndexBuffer &gib) : Buffer(gib), GLBuffer(gib), IndexBuffer(gib)
{

}

/*! \brief Binds the index buffer
 */
void viv::GLIndexBuffer::bind() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->bufferId);
}

/*! \brief Binds the index buffer
 */
void viv::GLIndexBuffer::unbind() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}


/*! \brief Adds data to the index buffers
 */
void viv::GLIndexBuffer::bufferData(size_t size, const void* data, bool dynamic) const
{
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
}

/*! \brief Adds Subdata to the buffer
 *
 * \param (uint32_t) offset - The offset of the data
 * \param (uint32_t) size - The size of the data
 * \param (const void*) subData - The subdata
 */
void viv::GLIndexBuffer::bufferSubData(uint32_t offset, uint32_t size, const void* subData) const
{
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, size, subData);
}

/*! \brief GLIndexBuffer copy assigment operator
 *
 * \param (const GLIndexBuffer &) b - The OpenGL index buffer to copy from
 *
 * \return (const GLIndexBuffer &) This OpenGL index buffer after copying
 */
const viv::GLIndexBuffer & viv::GLIndexBuffer::operator=(const GLIndexBuffer &gib)
{
	Buffer::operator=(gib);
	GLBuffer::operator=(gib);
	IndexBuffer::operator=(gib);

	return *this;
}

/*! \brief Default GLIndexBuffer destructor
 */
viv::GLIndexBuffer::~GLIndexBuffer()
{

}
