/*! \file glBufferCollection.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "glBufferCollection.h"

/*! \brief GLBufferCollection default constructor
 *
 * \param (uint32_t) vertexBufferAmount - The amount of vertex buffers to create
 * \param (uint32_t) indexBufferAmount - The amount of index buffers to create
 * \param (uint32_t) uniformBufferAmount - The amount of uniform buffers to create
 */
viv::GLBufferCollection::GLBufferCollection(uint32_t vertexBufferAmount, uint32_t indexBufferAmount, uint32_t uniformBufferAmount)
						: BufferCollection()
{
	this->vertexBuffers.reserve(vertexBufferAmount);
	this->indexBuffers.reserve(indexBufferAmount);
	this->uniformBuffers.reserve(uniformBufferAmount);
	glGenVertexArrays(1, &this->vao);

	this->createBuffer(BufferType::VERTEX, vertexBufferAmount);
	this->createBuffer(BufferType::INDEX, indexBufferAmount);
	this->createBuffer(BufferType::UNIFORM, uniformBufferAmount);
}

/*! \brief GLBufferCollection copy constructor
 *
 * \param (const GLBufferCollection &) bc - The OpenGL buffer collection to copy
 */
viv::GLBufferCollection::GLBufferCollection(const GLBufferCollection &gbc) : BufferCollection(gbc)
{
	this->vertexBuffers.reserve(gbc.vertexBuffers.capacity());
	this->indexBuffers.reserve(gbc.indexBuffers.capacity());
	this->uniformBuffers.reserve(gbc.uniformBuffers.capacity());
	glGenVertexArrays(1, &this->vao);

	this->createBuffer(BufferType::VERTEX, this->vertexBuffers.size());
	this->createBuffer(BufferType::INDEX, this->indexBuffers.size());
	this->createBuffer(BufferType::UNIFORM, this->uniformBuffers.size());
}

/*! \brief Creates a buffer of the specified type
 *
 * \param (BufferCollection::BufferType) type - The type of buffer to create
 */
void viv::GLBufferCollection::createBuffer(BufferCollection::BufferType type, uint32_t amount)
{
	switch(type) {
		case BufferCollection::BufferType::INDEX:
			for(uint32_t i = 0; i < amount; i++)
				this->indexBuffers.push_back(new GLIndexBuffer());
			break;

		case BufferCollection::BufferType::UNIFORM:
			for(uint32_t i = 0; i < amount; i++)
				this->uniformBuffers.push_back(new GLUniformBuffer());
			break;
		case BufferCollection::BufferType::VERTEX:
		default:
			for(uint32_t i = 0; i < amount; i++)
				this->vertexBuffers.push_back(new GLVertexBuffer());
			break;
	}
}

/*! \brief Binds the buffer collection
 */
void viv::GLBufferCollection::bind() const
{
	glBindVertexArray(this->vao);
}

/*! \brief Unbinds the buffer collection
 */
void viv::GLBufferCollection::unbind() const
{
	glBindVertexArray(0);
}

/*! \brief Gets the vertex buffer at a specific index
 *
 * \param (uint32_t) index - The index of the vertex buffer
 *
 * \return (GLVertexBuffer*) The vertex buffer at the index
 */
const viv::GLVertexBuffer* viv::GLBufferCollection::getVertexBuffer(uint32_t index) const
{
	if(index >= this->vertexBuffers.size() || index < 0) {
		printf("Error: Buffer index out of range\n");
		return nullptr;
	}

	return this->vertexBuffers[index];
}

/*! \brief Gets the index buffer at a specific index
 *
 * \param (uint32_t) index - The index of the index buffer
 *
 * \return (GLIndexBuffer*) The index buffer at the index
 */
const viv::GLIndexBuffer* viv::GLBufferCollection::getIndexBuffer(uint32_t index) const
{
	if(index >= this->indexBuffers.size() || index < 0) {
		printf("Error: Buffer index out of range\n");
		return nullptr;
	}

	return this->indexBuffers[index];
}

/*! \brief Gets the uniform buffer at a specific index
 *
 * \param (uint32_t) index - The index of the uniform buffer
 *
 * \return (GLUniformBuffer*) The uniform buffer at the index
 */
const viv::GLUniformBuffer* viv::GLBufferCollection::getUniformBuffer(uint32_t index) const
{
	if(index >= this->uniformBuffers.size() || index < 0) {
		printf("Error: Buffer index out of range\n");
		return nullptr;
	}

	return this->uniformBuffers[index];
}

/*! \brief Deletes a buffer
 *
 * \param (BufferType) type - The type of buffer to delete
 * \param (uint32_t) index - The index of the buffer
 */
void viv::GLBufferCollection::deleteBuffer(BufferCollection::BufferType type, uint32_t index)
{
	if(type == BufferCollection::BufferType::VERTEX)
		this->vertexBuffers.erase(this->vertexBuffers.begin() + index);

	else if(type == BufferCollection::BufferType::INDEX)
		this->indexBuffers.erase(this->indexBuffers.begin() + index);

	else
		this->uniformBuffers.erase(this->uniformBuffers.begin() + index);
}

/*! \brief GLBufferCollection copy assignment operator
 *
 * \param (const GLBufferCollection &) bc - The OpenGL buffer collection to copy
 *
 * \return (const GLBufferCollection &) This BufferCollection after copying
 */
const viv::GLBufferCollection & viv::GLBufferCollection::operator=(const GLBufferCollection &gbc)
{
	BufferCollection::operator=(gbc);

	this->vertexBuffers.reserve(gbc.vertexBuffers.capacity());
	this->indexBuffers.reserve(gbc.indexBuffers.capacity());
	this->uniformBuffers.reserve(gbc.uniformBuffers.capacity());

	glGenVertexArrays(1, &this->vao);

	this->createBuffer(BufferType::VERTEX, this->vertexBuffers.size());
	this->createBuffer(BufferType::INDEX, this->indexBuffers.size());
	this->createBuffer(BufferType::UNIFORM, this->uniformBuffers.size());

	return *this;
}

/*! \brief GLBufferCollection default destructor
 */
viv::GLBufferCollection::~GLBufferCollection()
{
	for(uint32_t i = 0; i < this->vertexBuffers.size(); i++)
		delete this->vertexBuffers[i];

	for(uint32_t i = 0; i < this->indexBuffers.size(); i++)
		delete this->indexBuffers[i];

	for(uint32_t i = 0; i < this->uniformBuffers.size(); i++)
		delete this->uniformBuffers[i];

	glDeleteVertexArrays(1, &this->vao);
}

