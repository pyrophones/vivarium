/*! \file uniformBuffer.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "uniformBuffer.h"

/*! \brief Default uniform buffer constructor
 */
viv::UniformBuffer::UniformBuffer() : Buffer()
{

}

/*! \brief UniformBuffer copy constructor
 *
 * \param (const UniformBuffer &) b - The uniform buffer to copy from
 */
viv::UniformBuffer::UniformBuffer(const UniformBuffer &ub) : Buffer(ub)
{

}

/*! \brief UniformBuffer copy assigment operator
 *
 * \param (const UniformBuffer &) b - The uniform buffer to copy from
 *
 * \return (const UniformBuffer &) This uniform buffer after copying
 */
const viv::UniformBuffer & viv::UniformBuffer::operator=(const UniformBuffer &ub)
{
	Buffer::operator=(ub);
	return *this;
}

/*! \brief Default uniform buffer destructor
 */
viv::UniformBuffer::~UniformBuffer()
{

}

