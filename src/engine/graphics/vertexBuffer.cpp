/*! \file vertexBuffer.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "vertexBuffer.h"

/*! \brief Default vertex buffer constructor
 */
viv::VertexBuffer::VertexBuffer() : Buffer()
{

}

/*! \brief VertexBuffer copy constructor
 *
 * \param (const VertexBuffer &) b - The vertex buffer to copy from
 */
viv::VertexBuffer::VertexBuffer(const VertexBuffer &vb) : Buffer(vb)
{

}

/*! \brief VertexBuffer copy assigment operator
 *
 * \param (const VertexBuffer &) b - The vertex buffer to copy from
 *
 * \return (const VertexBuffer &) This vertex buffer after copying
 */
const viv::VertexBuffer & viv::VertexBuffer::operator=(const VertexBuffer &vb)
{
	Buffer::operator=(vb);
	return *this;
}

/*! \brief Default vertex buffer destructor
 */
viv::VertexBuffer::~VertexBuffer()
{

}
