/*! \file bufferCollection.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "bufferCollection.h"

/*! \brief BufferCollection default constructor
 *
 * \param (uint32_t) vertexBufferAmount - The amount of vertex buffers to create
 * \param (uint32_t) indexBufferAmount - The amount of index buffers to create
 * \param (uint32_t) uniformBufferAmount - The amount of uniform buffers to create
 */
viv::BufferCollection::BufferCollection()
{

}

/*! \brief BufferCollection copy constructor
 *
 * \param (const BufferCollection &) bc - The buffer collection to copy
 */
viv::BufferCollection::BufferCollection(const BufferCollection &bc)
{

}

/*! \brief BufferCollection copy assignment operator
 *
 * \param (const BufferCollection &) bc - The buffer collection to copy
 *
 * \return (const BufferCollection &) This BufferCollection after copying
 */
const viv::BufferCollection & viv::BufferCollection::operator=(const BufferCollection &bc)
{
	return *this;
}

/*! \brief BufferCollection default destructor
 */
viv::BufferCollection::~BufferCollection()
{

}
