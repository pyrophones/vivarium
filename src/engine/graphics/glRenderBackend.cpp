/*! \file glRenderBackend.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#include "glRenderBackend.h"

/*! \brief GLRenderBackend constructor
 *
 */
viv::GLRenderBackend::GLRenderBackend() : RenderBackend()
{

}

/*! \brief GLRenderBackend copy constructor
 *
 * \param (const GLRenderBackend &) grb - The GLRenderBackend to copy
 */
viv::GLRenderBackend::GLRenderBackend(const GLRenderBackend &grb) : RenderBackend(grb)
{

}

/*! \brief Initializes the rendering backend
 *
 * \param (WId) winId - The Qt window ID
 */
int32_t viv::GLRenderBackend::init(WId winId)
{
	#if defined __linux__
	const int32_t displayAttribs[] = { GLX_RGBA,
									   GLX_DOUBLEBUFFER,
									   GLX_RED_SIZE, 4,
									   GLX_GREEN_SIZE, 4,
									   GLX_BLUE_SIZE, 4,
									   GLX_ALPHA_SIZE, 4,
									   GLX_DEPTH_SIZE, 24,
									   None };
	XVisualInfo* xvi = nullptr;
	//if((glXQueryExtensionsString(QX11Info::display(), DefaultScreen(QX11Info::display())), "GLX_ARB_create_context")) {
		if (glXQueryExtension(QX11Info::display(), NULL, NULL)) {
			typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
			glXCreateContextAttribsARBProc glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((const GLubyte*)"glXCreateContextAttribsARB");

			int32_t fbConfsNum;
			GLXFBConfig* fbConfigs = glXChooseFBConfig(QX11Info::display(), QX11Info::appScreen(), displayAttribs, &fbConfsNum);
			GLXFBConfig fbConfig = 0;

			for(int32_t i = 0; i < fbConfsNum; i++) {
				xvi = glXGetVisualFromFBConfig(QX11Info::display(), fbConfigs[i]);
				if(!xvi) {
					continue;
				}

				XRenderPictFormat* format = XRenderFindVisualFormat(QX11Info::display(), xvi->visual);
				XFree(xvi);
				if(!format)
					continue;

				fbConfig = fbConfigs[i];
				if(format->direct.alphaMask > 0) {
					break;
				}
			}

			const int32_t contextAttribs[] = { GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
											   GLX_CONTEXT_MINOR_VERSION_ARB, 5,
											   GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
											   None };

			//TODO: ES stuff eventually
			//const int32_t contextAttribs[] = { GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
			//								   GLX_CONTEXT_MINOR_VERSION_ARB, 2,
			//								   GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_ES_PROFILE_BIT_EXT,
			//								   None };

			if(glXCreateContextAttribsARB)
				this->context = glXCreateContextAttribsARB(QX11Info::display(), fbConfig, 0, true, contextAttribs);

			XFree(fbConfigs);
		}

		//TODO: Implement legacy window creation stuff... ?
		//else {
		//	xvi = glXChooseVisual(QX11Info::display(), QX11Info::appScreen(), displayAttribs);
		//}
	//}

	if(this->context == NULL) {
		printf("Failed to create GL context!\n");
		return -1;
	}

	glXMakeCurrent(QX11Info::display(), winId, this->context);
#elif defined _WIN32
//TODO: Windows stuff
	const int32_t attribList[] =
	{
		WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
		WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
		WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
		WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
		WGL_COLOR_BITS_ARB, 32,
		WGL_DEPTH_BITS_ARB, 24,
		WGL_STENCIL_BITS_ARB, 8,
		0, // End
	};

	int32_t pixelFormat;
	UINT numFormats;

	wglChoosePixelFormatARB(hdc, attribList, NULL, 1, &pixelFormat, &numFormats);

	const int32_t contextAttribs[] = { WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
									   WGL_CONTEXT_MINOR_VERSION_ARB, 5,
									   WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB
									   0 };

	this->hdc = GetDC(HWND(winID));

	if(this->hdc == NULL) {
		printf("Failed to get device context!\n");
		return -1;
	}

	this->context = wglCreateContextAttribsARB(this->hdc, 0, contextAttribs);

	if(this->context == NULL) {
		printf("Failed to create OpenGL context!\n");
		return -1;
	}

	wglMakeCurrent(this->hdc, this->context);
#endif

	printf("\tInitializing GLEW . . . ");
	glewExperimental = GL_TRUE;

	//Check if glew initializes
	if(glewInit() != GLEW_OK) {
		printf("Failed to initialize GLEW!\n");
		return -1;
	}

	//OpenGL functions
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	printf("ok\n");
	return 0;
}

/*! \brief Creates a buffer collection
 *
 * \param (uint32_t) vertexBufferAmount - The amount of vertex buffers
 * \param (uint32_t) indexBufferAmount - The amount of index buffers
 * \param (uint32_t) uniformBufferAmount - The amount of uniform buffers
 *
 * \return (BufferCollection*) The newly created buffer collection
 */
viv::BufferCollection* viv::GLRenderBackend::createBufferCollection(uint32_t vertexBufferAmount, uint32_t indexBufferAmount, uint32_t uniformBufferAmount) const
{
	return new GLBufferCollection(vertexBufferAmount, indexBufferAmount, uniformBufferAmount);
}

/*! \brief Creates a framebuffer with a render buffer
 *
 * \param (uint32_t) width - The width of the framebuffer renderbuffer
 * \param (uint32_t) height - The height of the framebuffer renderbuffer
 * \param (bool) bool - If the framebuffer should have a renderbuffer
 *
 * \return (Framebuffer*) The newly created Framebuffer
 */
viv::Framebuffer* viv::GLRenderBackend::createFramebuffer(uint32_t width, uint32_t height, bool renderbuffer) const
{
	return new GLFramebuffer(width, height, renderbuffer);
}

/*! \brief Creates a texture
 *
 * \param (int32_t) width - The width of the texture
 * \param (int32_t) height - The height of the texture
 * \param (in32_t) channels - The amount of channels for the texture
 * \param (uint32_t) bits - The bit depth for the texture
 * \param (uint32_t) type - The type of the texture
 * \param (uint32_t) clampType - The type of clamping the texture should use
 * \param (uint32_t) minFilter - The minification filter of the texture
 * \param (uint32_t) magFilter - The magnification filter of the texture
 * \param (const void*) data - The data of the texture
 * \param (bool) generateMipMaps - If mipmaps should be generated
 *
 * \return (Texture*) The newly created Texture
 */
viv::Texture* viv::GLRenderBackend::createTexture(int32_t width, int32_t height, int32_t channels, uint32_t bits, uint32_t type, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void* data, bool generateMipMaps) const
{
	return new GLTexture(width, height, channels, bits, type, clampType, minFilter, magFilter, data, generateMipMaps);
}

/*! \brief Creates a texture
 *
 * \param (int32_t) width - The width of the texture
 * \param (int32_t) height - The height of the texture
 * \param (in32_t) channels - The amount of channels for the texture
 * \param (uint32_t) type - The type of the texture
 * \param (uint32_t) iFormat - The internal format of the texture
 * \param (uint32_t) format - The OpenGL format of the texture
 * \param (uint32_t) clampType - The type of clamping the texture should use
 * \param (uint32_t) minFilter - The minification filter of the texture
 * \param (uint32_t) magFilter - The magnification filter of the texture
 * \param (const void*) data - The data of the texture
 * \param (bool) generateMipMaps - If mipmaps should be generated
 *
 * \return (Texture*) The newly created Texture
 */
viv::Texture* viv::GLRenderBackend::createTexture(int32_t width, int32_t height, int32_t channels, uint32_t type, uint32_t iFormat, uint32_t format, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void* data, bool generateMipMaps) const
{
	return new GLTexture(width, height, channels, type, iFormat, format, clampType, minFilter, magFilter, data, generateMipMaps);
}

/*! \brief Creates a cubemap texture
 *
 * \param (int32_t) width - The width of a single face of the cubemap
 * \param (int32_t) height - The height of a single face of the cubemap
 * \param (in32_t) channels - The amount of channels for the cubemap
 * \param (uint32_t) bits - The bit depth for the cubemap
 * \param (uint32_t) type - The type of the cubemap
 * \param (uint32_t) clampType - The type of clamping the cubemap should use
 * \param (uint32_t) minFilter - The minification filter of the cubemap
 * \param (uint32_t) magFilter - The magnification filter of the cubemap
 * \param (const void**) data - The data of the cubemap
 * \param (bool) generateMipMaps - If mipmaps should be generated
 *
 * \return (Texture*) The newly created cubemap texture
 */
viv::Texture* viv::GLRenderBackend::createCubeMap(int32_t width, int32_t height, int32_t channels, uint32_t bits, uint32_t type, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void** data, bool generateMipMaps) const
{
	return new GLTexture(width, height, channels, bits, type, clampType, minFilter, magFilter, data, generateMipMaps);
}

/*! \brief Copies a texture to another texture
 *
 * \param (const Texture*) t1 - The texture to copy from
 * \param (const Texture*) t2 - The texture to copy to
 * \param (uint32_t) srcLvl - The mipmap level of the source
 * \param (uint32_t) srcX - The x coord of the left edge of the source image
 * \param (uint32_t) srcY - The y coord of the top edge of the source image
 * \param (uint32_t) srcZ - The z coord of the near edge of the source image
 * \param (uint32_t) dstLvl - The mipmap level of the destination
 * \param (uint32_t) dstX - The x coord of the left edge of the destination image
 * \param (uint32_t) dstY - The y coord of the top edge of the destination image
 * \param (uint32_t) dstZ - The z coord of the near edge of the destination image
 * \param (uint32_t) width - The width of the region to copy
 * \param (uint32_t) height - The height of the region to copy
 * \param (uint32_t) depth - The depth of the region to copy
 */
void viv::GLRenderBackend::copyImage(const Texture* t1, const Texture* t2, uint32_t srcLvl, uint32_t srcX, uint32_t srcY, uint32_t srcZ, uint32_t dstLvl, uint32_t dstX, uint32_t dstY, uint32_t dstZ, uint32_t width, uint32_t height, uint32_t depth) const
{
	const GLTexture* tex1 = dynamic_cast<const GLTexture*>(t1);
	const GLTexture* tex2 = dynamic_cast<const GLTexture*>(t2);
	glCopyImageSubData(tex1->getTexture(), tex1->getTextureType(), srcLvl, srcX, srcY, srcZ,
					   tex2->getTexture(), tex2->getTextureType(), dstLvl, dstX, dstY, dstZ,
					   width, height, depth);
}

/*! \brief Copies a texture to a renderbuffer
 *
 * \param (const Texture*) t1 - The texture to copy from
 * \param (const Framebuffer*) t2 - The framebuffer that has the renderbuffer to copy to
 * \param (uint32_t) srcLvl - The mipmap level of the source
 * \param (uint32_t) srcX - The x coord of the left edge of the source image
 * \param (uint32_t) srcY - The y coord of the top edge of the source image
 * \param (uint32_t) srcZ - The z coord of the near edge of the source image
 * \param (uint32_t) dstX - The x coord of the left edge of the destination image
 * \param (uint32_t) dstY - The y coord of the top edge of the destination image
 * \param (uint32_t) dstZ - The z coord of the near edge of the destination image
 * \param (uint32_t) width - The width of the region to copy
 * \param (uint32_t) height - The height of the region to copy
 * \param (uint32_t) depth - The depth of the region to copy
 */
void viv::GLRenderBackend::copyImage(const Texture* t1, const Framebuffer* t2, uint32_t srcLvl, uint32_t srcX, uint32_t srcY, uint32_t srcZ, uint32_t dstX, uint32_t dstY, uint32_t dstZ, uint32_t width, uint32_t height, uint32_t depth) const
{
	const GLTexture* tex1 = dynamic_cast<const GLTexture*>(t1);
	glCopyImageSubData(tex1->getTexture(), tex1->getTextureType(), srcLvl, srcX, srcY, srcZ,
					   dynamic_cast<const GLFramebuffer*>(t2)->getRenderbuffer(), GL_RENDERBUFFER, 0, dstX, dstY, dstZ,
					   width, height, depth);
}

/*! \brief Copies a renderbuffer to a texture
 *
 * \param (const Framebuffer*) t1 - The framebuffer that has the renderbuffer to copy from
 * \param (const Texture*) t2 - The texture to copy to
 * \param (uint32_t) srcX - The x coord of the left edge of the source image
 * \param (uint32_t) srcY - The y coord of the top edge of the source image
 * \param (uint32_t) srcZ - The z coord of the near edge of the source image
 * \param (uint32_t) dstLvl - The mipmap level of the destination
 * \param (uint32_t) dstX - The x coord of the left edge of the destination image
 * \param (uint32_t) dstY - The y coord of the top edge of the destination image
 * \param (uint32_t) dstZ - The z coord of the near edge of the destination image
 * \param (uint32_t) width - The width of the region to copy
 * \param (uint32_t) height - The height of the region to copy
 * \param (uint32_t) depth - The depth of the region to copy
 */
void viv::GLRenderBackend::copyImage(const Framebuffer* t1, const Texture* t2, uint32_t srcX, uint32_t srcY, uint32_t srcZ, uint32_t dstLvl, uint32_t dstX, uint32_t dstY, uint32_t dstZ, uint32_t width, uint32_t height, uint32_t depth) const
{
	const GLTexture* tex2 = dynamic_cast<const GLTexture*>(t2);
	glCopyImageSubData(dynamic_cast<const GLFramebuffer*>(t1)->getRenderbuffer(), GL_RENDERBUFFER, 0, srcX, srcY, srcZ,
					   tex2->getTexture(), tex2->getTextureType(), dstLvl, dstX, dstY, dstZ,
					   width, height, depth);
}

/*! \brief Copies a renderbuffer to another renderbuffer
 *
 * \param (const Framebuffer*) t1 - The framebuffer that has the renderbuffer to copy from
 * \param (const Framebuffer*) t2 - The framebuffer that has the renderbuffer to copy to
 * \param (uint32_t) srcX - The x coord of the left edge of the source image
 * \param (uint32_t) srcY - The y coord of the top edge of the source image
 * \param (uint32_t) srcZ - The z coord of the near edge of the source image
 * \param (uint32_t) dstX - The x coord of the left edge of the destination image
 * \param (uint32_t) dstY - The y coord of the top edge of the destination image
 * \param (uint32_t) dstZ - The z coord of the near edge of the destination image
 * \param (uint32_t) width - The width of the region to copy
 * \param (uint32_t) height - The height of the region to copy
 * \param (uint32_t) depth - The depth of the region to copy
 */
void viv::GLRenderBackend::copyImage(const Framebuffer* t1, const Framebuffer* t2, uint32_t srcX, uint32_t srcY, uint32_t srcZ, uint32_t dstX, uint32_t dstY, uint32_t dstZ, uint32_t width, uint32_t height, uint32_t depth) const
{
	glCopyImageSubData(dynamic_cast<const GLFramebuffer*>(t1)->getRenderbuffer(), GL_RENDERBUFFER, 0, srcX, srcY, srcZ,
					   dynamic_cast<const GLFramebuffer*>(t2)->getRenderbuffer(), GL_RENDERBUFFER, 0, dstX, dstY, dstZ,
					   width, height, depth);
}

/*! \brief Binds a framebuffer
 *
 * \param (const Framebuffer*) fb - The framebuffer to bind
 * \param (BindingType) bt - The binding type for the framebuffer
 */
void viv::GLRenderBackend::bindFramebuffer(const Framebuffer* fb, BindingType bt) const
{
	uint32_t type;
	switch(bt) {
		case RenderBackend::BindingType::READ:
			type = GL_READ_FRAMEBUFFER;
			break;
		case RenderBackend::BindingType::WRITE:
			type = GL_DRAW_FRAMEBUFFER;
			break;
		case BindingType::NORMAL:
		default:
			type = GL_FRAMEBUFFER;
			break;
	}

	glBindFramebuffer(type, (fb == nullptr ? 0 : ((const GLFramebuffer*)fb)->getFramebuffer()));
}

/*! \brief Enables or disables the depth mask
 *
 * \param (bool) enabled - If the depth mask is enabled
 */
void viv::GLRenderBackend::setDepthMask(bool enabled) const
{
	enabled ? glDepthMask(GL_TRUE) : glDepthMask(GL_FALSE);
}

/*! \brief Enables or disables the depth test
 *
 * \param (bool) enabled - If depth testing is enabled
 */
void viv::GLRenderBackend::setDepthTest(bool enabled) const
{
	enabled ? glEnable(GL_DEPTH_TEST) : glDisable(GL_DEPTH_TEST);
}

/*! \brief Enables or disables the depth funtion
 *
 * \param (DepthFunction) df - The depth function to use
 */
void viv::GLRenderBackend::setDepthFunction(DepthFunction df) const
{
	switch(df) {
		case RenderBackend::DepthFunction::LEQUAL:
			glDepthFunc(GL_LEQUAL);
			break;
		case RenderBackend::DepthFunction::EQUAL:
			glDepthFunc(GL_EQUAL);
			break;
		case RenderBackend::DepthFunction::GEQUAL:
			glDepthFunc(GL_GEQUAL);
			break;
		case RenderBackend::DepthFunction::GREATER:
			glDepthFunc(GL_GREATER);
			break;
		case RenderBackend::DepthFunction::LESS:
		default:
			glDepthFunc(GL_LESS);
			break;
	}
}

/*! \brief Enables or disables blending
 *
 * \param (bool) enabled - If blending is enabled
 */
void viv::GLRenderBackend::setBlending(bool enabled) const
{
	enabled ? glEnable(GL_BLEND) : glDisable(GL_BLEND);
}

/*! \brief Changes the blending equation
 *
 * \param (BlendEquation) bm - The blending equation to use
 */
void viv::GLRenderBackend::setBlendMode(BlendEquation be) const
{
	switch(be) {
		case RenderBackend::BlendEquation::SUB:
			glBlendEquation(GL_FUNC_SUBTRACT);
			break;
		case RenderBackend::BlendEquation::ADD:
		default:
			glBlendEquation(GL_FUNC_ADD);
			break;
	}
}

/*! \brief Changes the blending function
 *
 * \param (BlendFuncParam) bfp1 - The blending function param for the source
 * \param (BlendFuncParam) bfp2 - The blending function param for the destination
 */
void viv::GLRenderBackend::setBlendFunction(BlendFuncParam bfp1, BlendFuncParam bfp2) const
{
	uint32_t src;
	uint32_t dst;

	switch(bfp1) {
		case RenderBackend::BlendFuncParam::DST_COLOR:
			src = GL_DST_COLOR;
			break;
		case RenderBackend::BlendFuncParam::ONE:
			src = GL_ONE;
			break;
		case RenderBackend::BlendFuncParam::ZERO:
			src = GL_ZERO;
			break;
		case RenderBackend::BlendFuncParam::SRC_ALPHA:
			src = GL_SRC_ALPHA;
			break;
		case RenderBackend::BlendFuncParam::ONE_MINUS_SRC_ALPHA:
			src = GL_ONE_MINUS_SRC_ALPHA;
			break;
		case RenderBackend::BlendFuncParam::DST_ALPHA:
			src = GL_DST_ALPHA;
			break;
		case RenderBackend::BlendFuncParam::ONE_MINUS_DST_ALPHA:
			src = GL_ONE_MINUS_DST_ALPHA;
			break;
		case RenderBackend::BlendFuncParam::SRC_COLOR:
		default:
			src = GL_SRC_COLOR;
			break;
	}

	switch(bfp2) {
		case RenderBackend::BlendFuncParam::DST_COLOR:
			dst = GL_DST_COLOR;
			break;
		case RenderBackend::BlendFuncParam::ONE:
			dst = GL_ONE;
			break;
		case RenderBackend::BlendFuncParam::ZERO:
			dst = GL_ZERO;
			break;
		case RenderBackend::BlendFuncParam::SRC_ALPHA:
			dst = GL_SRC_ALPHA;
			break;
		case RenderBackend::BlendFuncParam::ONE_MINUS_SRC_ALPHA:
			dst = GL_ONE_MINUS_SRC_ALPHA;
			break;
		case RenderBackend::BlendFuncParam::DST_ALPHA:
			dst = GL_DST_ALPHA;
			break;
		case RenderBackend::BlendFuncParam::ONE_MINUS_DST_ALPHA:
			dst = GL_ONE_MINUS_DST_ALPHA;
			break;
		case RenderBackend::BlendFuncParam::SRC_COLOR:
		default:
			dst = GL_SRC_COLOR;
			break;
	}

	glBlendFunc(src, dst);
}

/*! \brief Enables or disables face culling
 *
 * \param (bool) enabled - If culling is enabled
 */
void viv::GLRenderBackend::setCulling(bool enabled) const
{
	enabled ? glEnable(GL_CULL_FACE) : glDisable(GL_CULL_FACE);
}

/*! \brief Sets the clear color of the window
 *
 * \param (const Vec4 &) color - The color for clearing
 */
void viv::GLRenderBackend::setClearColor(const Vec4 &color) const
{
	glClearColor(color.x, color.y, color.z, color.w);
}

/*! \brief Sets the cull mode of the renderer
 *
 * \param (RenderManager::CullMode) cm - The culling mode of the renderer
 */
void viv::GLRenderBackend::setCullMode(CullMode cm) const
{
	switch(cm) {
		case RenderBackend::CullMode::BACK:
			glCullFace(GL_BACK);
			break;
		case RenderBackend::CullMode::FRONT_AND_BACK:
			glCullFace(GL_FRONT_AND_BACK);
			break;
		case RenderBackend::CullMode::FRONT:
		default:
			glCullFace(GL_FRONT);
			break;
	}
}

/*! \brief Clears the current framebuffer
 *
 * \param (RenderManager::ClearMask) cm - The clear mask of the renderer
 */
void viv::GLRenderBackend::clear(ClearMask cm) const
{
	switch(cm) {
		case RenderBackend::ClearMask::COLOR:
			glClear(GL_COLOR_BUFFER_BIT);
			break;
		case RenderBackend::ClearMask::DEPTH:
			glClear(GL_DEPTH_BUFFER_BIT);
			break;
		case RenderBackend::ClearMask::COLOR_AND_DEPTH:
		default:
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			break;
	}
}

/*! \brief Draws a object
 *
 * \param (RenderBackend::DrawType) type - The type to use for drawing
 * \param (const MeshData*) md - The MeshData to use for drawing
 * \param (uint32_t) instances - The number of instances to draw
 */
void viv::GLRenderBackend::draw(RenderBackend::DrawType type, const MeshData* md, uint32_t instances) const
{
	uint32_t drawType;
	switch(type) {
		case DrawType::POINTS:
			drawType = GL_POINTS;
			break;
		case DrawType::LINES:
			drawType = GL_LINES;
			break;
		case DrawType::QUADS:
			drawType = GL_QUADS;
			break;
		case DrawType::TRIS:
		default:
			drawType = GL_TRIANGLES;
			break;
	}

	for(uint32_t i = 0; i < md->getIndexAmountCount(); i++)
		glDrawElementsInstancedBaseVertex(drawType, md->getIndexAmounts()[i], GL_UNSIGNED_INT, 0, instances, md->getIndexOffsets()[i]);
}

/*! \brief Swaps the back and front buffers
 *
 * \param (const WId &) winId - The Qt window ID
 */
void viv::GLRenderBackend::swapBuffers(const WId &winId) const
{
#ifdef __linux__
	glXSwapBuffers(QX11Info::display(), winId);
#elif defined _WIN32
	SwapBuffers();
#endif
}

/*! \brief Resizes the viewport
 *
 * \param (int32_t) width - The new width of the viewport
 * \param (int32_t) height - The new height of the viewport
 */
void viv::GLRenderBackend::resize(int32_t width, int32_t height) const
{
	glViewport(0, 0, width, height);
}

/*! \brief GLRenderBackend copy constructor
 *
 * \param (const GLRenderBackend &) grb - The GLRenderBackend to copying
 *
 * \return (const GLRenderBackend &) This renderbackend after being assigned
 */
const viv::GLRenderBackend & viv::GLRenderBackend::operator=(const GLRenderBackend &grb)
{
	RenderBackend::operator=(grb);

	return *this;
}

/*! \brief GLRenderBackend default destructor
 */
viv::GLRenderBackend::~GLRenderBackend()
{
#if defined __linux__
	glXMakeCurrent(QX11Info::display(), None, nullptr);

	if(this->context != NULL)
		glXDestroyContext(QX11Info::display(), this->context);
#elif defined _WIN32
	wglMakeCurrent(nullptr, nullptr);

	if(this->context != NULL)
		wglDeleteContext(this->context);
#endif
}
