/*! \file buffer.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef BUFFER_H
#define BUFFER_H

#include <memory>

#include "vertex.h"
//#ifdef __linux__
//	#include <wine/windows/d3d11.h>
//#endif

namespace viv
{
	/*! \class Buffer
	 *  \brief Generic class for buffer information
	 */
	class Buffer
	{
		public:
			Buffer();
			Buffer(const Buffer &b);
			virtual void bind() const = 0;
			virtual void unbind() const = 0;
			virtual void bufferData(size_t size, const void* data, bool dynamic) const = 0;
			virtual void bufferSubData(uint32_t offset, uint32_t size, const void* subData) const = 0;
			const Buffer & operator=(const Buffer &b);
			virtual ~Buffer();
	};
};

#endif
