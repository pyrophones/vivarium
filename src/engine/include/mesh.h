/*! \file mesh.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#ifndef MODEL_H
#define MODEL_H

#include <GL/glew.h>
#include <memory>
#include <algorithm>
#include "transform.h"
#include "drawable.h"
#include "vertex.h"
#include "material.h"

namespace viv
{
	/*! \class Mesh
	 *  \brief Handles creation and rendering of models
	 */
	class Mesh : public Drawable
	{
		public:
			Mesh(const RenderBackend* backend, std::shared_ptr<Shader> shader, std::shared_ptr<MeshData> md);
			Mesh(const Mesh &m);
			const Mesh & operator=(const Mesh &m);
			virtual ~Mesh();

		protected:
	};
};
#endif
