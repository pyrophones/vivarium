/*! \file lightInstance.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef LIGHTINSTANCE_H
#define LIGHTINSTANCE_H

#include "transform.h"
#include "lights.h"

namespace viv
{
	/*! \class DirectionalLightInstance
	 *  \brief An instance of a directional light. Inherits from Transform
	 */
	class DirectionalLightInstance : public Transform
	{
		public:
			std::shared_ptr<DirectionalLight> light;

			DirectionalLightInstance(std::shared_ptr<DirectionalLight> l = nullptr, const Vec3 &pos = Vec3(), std::string tag = "DirectionalLight");
			DirectionalLightInstance(const DirectionalLightInstance &dli);
			virtual void update();
			const DirectionalLightInstance & operator=(const DirectionalLightInstance &dli);
			virtual ~DirectionalLightInstance();
	};

	/*! \class PointLightInstance
	 *  \brief An instance of a point light. Inherits from Transform
	 */
	class PointLightInstance : public Transform
	{
		public:
			std::shared_ptr<PointLight> light;

			PointLightInstance(std::shared_ptr<PointLight> l = nullptr, const Vec3 &pos = Vec3(), std::string tag = "PointLight");
			PointLightInstance(const PointLightInstance &pli);
			virtual void update();
			const PointLightInstance & operator=(const PointLightInstance &pli);
			virtual ~PointLightInstance();
	};

	/*! \class SpotLightInstance
	 *  \brief An instance of a spot light. Inherits from Transform
	 */
	class SpotLightInstance : public Transform
	{
		public:
			std::shared_ptr<SpotLight> light;

			SpotLightInstance(std::shared_ptr<SpotLight> l = nullptr, const Vec3 &pos = Vec3(), std::string tag = "SpotLight");
			SpotLightInstance(const SpotLightInstance &sli);
			virtual void update();
			const SpotLightInstance & operator=(const SpotLightInstance &sli);
			virtual ~SpotLightInstance();
	};
};

#endif
