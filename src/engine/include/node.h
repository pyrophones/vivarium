/*! \file node.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#ifndef NODE_H
#define NODE_H

#include <string>
#include <unordered_map>
#include <vector>
#include <memory>
#include <new>
#include <cstdint>
#include <GL/glew.h>
#include "script.h"
#include "gmath.h"
//#include "rapidxml.hpp"
//#include "shader.h"

namespace viv
{
	/*! \class Node
	 *  \brief Basic class for game objects
	 */
	class Node
	{
		public:
			Node(const std::string tag = "node");
			Node(const Node &n);
			Node* addSubNode(std::unique_ptr<Node> n);
			virtual void update();
			Node* getParent();
			//Node* getChildByIndex(uint32_t index);
			Node* getChildByTag(std::string tag);
			uint32_t getSubNodeCount();
			std::string getTag();
			const Node & operator=(const Node &n);
			virtual ~Node();

			//Static methods for as bindings
			static void DefCtor(Node* self);
			static void CopyCtor(const Node &other, Node* self);
			static void InitCtor(std::string tag, Node* self);
			static void Dtor(Node* self);

		protected:
			uint32_t subNodeCount;
			Mat4 transMat;
			Mat3 normalMat;
			Node* parent = nullptr;
			//NOTE: Might change this to to a custom data structure
			std::unordered_map<std::string, std::unique_ptr<Node>> subNodes;
			std::string tag;
			//Script script;
		private:

	};
}

#endif
