/*! \file meshInstance.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#ifndef MESHINSTANCE_H
#define MESHINSTANCE_H

#include "transform.h"
#include "mesh.h"

namespace viv
{
	/*! \class MeshInstance
	 *  \brief An instance of a mesh in the world
	 */
	class MeshInstance : public Transform
	{
		public:
			std::shared_ptr<Mesh> mesh;

			MeshInstance(std::shared_ptr<Mesh> m = nullptr, const Vec3 &pos = Vec3(), const std::string tag = "mesh");
			MeshInstance(const MeshInstance &mi);
			void setMaterial(std::shared_ptr<Material> material);
			void setTint(const Vec3 &tint);
			virtual void update();
			const MeshInstance & operator=(const MeshInstance &mi);
			virtual ~MeshInstance();

		private:
			std::shared_ptr<Material> material;
			Vec3 tint = Vec3(1.0f, 1.0f, 1.0f);
	};
};

#endif
