/*! \file glVertexBuffer.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */
#ifndef GLVERTEXBUFFER_H
#define GLVERTEXBUFFER_H

#include <GL/glew.h>
#include "glBuffer.h"
#include "vertexBuffer.h"

namespace viv
{
	/*! \class GLVertexBuffer
	 *  \brief An OpenGL vertex buffer
	 */
	class GLVertexBuffer : public GLBuffer, public VertexBuffer
	{
		public:
			GLVertexBuffer();
			GLVertexBuffer(const GLVertexBuffer &gvb);
			void bind() const override;
			void unbind() const override;
			virtual void bufferData(size_t size, const void* data, bool dynamic) const override;
			virtual void bufferSubData(uint32_t offset, uint32_t size, const void* subData) const override;
			void bufferAttribute(uint32_t location, uint32_t size, uint32_t type, bool normalized, uint32_t stride, uint64_t offset, uint32_t divisor) const override;
			const GLVertexBuffer & operator=(const GLVertexBuffer &gvb);
			virtual ~GLVertexBuffer();
	};
};

#endif

