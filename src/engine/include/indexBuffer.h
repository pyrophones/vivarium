/*! \file indexBuffer.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef INDEXBUFFER_H
#define INDEXBUFFER_H

#include "buffer.h"

namespace viv
{
	/*! \class IndexBuffer
	 *  \brief Holds information specific for vertex buffers
	 */
	class IndexBuffer : public virtual Buffer
	{
	public:
		IndexBuffer();
		IndexBuffer(const IndexBuffer &vb);
		virtual void bind() const override = 0;
		virtual void unbind() const override = 0;
		virtual void bufferData(size_t size, const void* data, bool dynamic) const override = 0;
		virtual void bufferSubData(uint32_t offset, uint32_t size, const void* subData) const override= 0;
		const IndexBuffer & operator=(const IndexBuffer &vb);
		virtual ~IndexBuffer();
	};
};

#endif


