/*! \file game.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef GAME_H
#define GAME_H

#include <memory>
#include "input.h"
#include "assetManager.h"
#include "node.h"
#include "transform.h"
#include "rigidBody.h"
#include "meshInstance.h"
#include "lightInstance.h"
#include "camera.h"
#include "script.h"
#include "skybox.h"
#include "renderBackend.h"

//NOTE:Temporary include
#include "rendererWidget.h"

namespace viv
{
	/*! \class Game
	 *  \brief Handles gamestates and most user interaction
	 */
	class Game
	{
		public:
			enum GameState { MENU, INGAME, PAUSE };

			//TODO: Move this to scene when those are created
			static std::vector<std::shared_ptr<DirectionalLight>> sceneDirectionalLights;
			static std::vector<std::shared_ptr<PointLight>> scenePointLights;
			static std::vector<std::shared_ptr<SpotLight>> sceneSpotLights;
			static std::shared_ptr<Skybox> skybox;

			Game(const Game &game) = delete;
			const Game& operator=(const Game &game) = delete;
			static Game* getInstance();
			static void start(const RenderBackend* backend);
			static void loadAssets();
			static void unloadAssets();
			static void update();
			static void deleteInstance();
			static GameState getCurState();
			static Camera* getActiveCam();
			static void setActiveCam(Camera* cam);
			~Game();

		private:
			static GameState curState; //! The curret gamestate
			static std::unique_ptr<Game> instance; //! The instance of the game singleton
			static Camera* activeCam; //! Pointer to the active camera
			static Node* base; //! pointer to  the base node of the scene
			static const RenderBackend* backend; //! The rendering backend

			Game();
	};
};

#endif
