/*! \file material.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#ifndef MATERIAL_H
#define MATERIAL_H

#include <fstream>
#include <sstream>
#include <string>
#include <memory>
#include <cstdio>
#include "texture.h"

namespace viv
{
	/*! \struct Material
	 *  \brief Holds material information
	 */
	struct Material
	{
		public:
			Material(std::string name, std::shared_ptr<Texture> albedo, std::shared_ptr<Texture> normal, std::shared_ptr<Texture> metal, std::shared_ptr<Texture> rough, std::shared_ptr<Texture> ao, std::shared_ptr<Texture> disp, float parallaxStrength = -0.05f, float parallaxBias = 0.42f);
			Material(const Material &mat);
			//void setMaterialTexture(std::shared_ptr<Texture> materialArray);
			//Texture* getMaterialTexture();
			std::string getName() const;
			const Texture* getAlbedoTexture();
			void setAlbedoTexture(std::shared_ptr<Texture> texture);
			const Texture* getNormalTexture();
			void setNormalTexture(std::shared_ptr<Texture> texture);
			const Texture* getMetalTexture();
			void setMetalTexture(std::shared_ptr<Texture> texture);
			const Texture* getRoughnessTexture();
			void setRoughnessTexture(std::shared_ptr<Texture> texture);
			const Texture* getAoTexture();
			void setAoTexture(std::shared_ptr<Texture> texture);
			const Texture* getDispTexture();
			void setDispTexture(std::shared_ptr<Texture> texture);
			float getParallaxStrength();
			void setParallaxStrength(float parallaxStrength);
			float getParallaxBias();
			void setParallaxBias(float parallaxBias);
			const Material & operator=(const Material &mat);
			~Material();

		private:
			std::shared_ptr<Texture> albedo;
			std::shared_ptr<Texture> normal;
			std::shared_ptr<Texture> metal;
			std::shared_ptr<Texture> rough;
			std::shared_ptr<Texture> ao;
			std::shared_ptr<Texture> disp;
			float parallaxStrength;
			float parallaxBias;
			std::string name;
	};
};
#endif
