/*! \file timer.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef TIME_H
#define TIME_H

#include <iostream>
#include <memory>
#include <ctime>
#include "GL/glew.h"

namespace viv
{
	/*! \class Time
	 *  \brief  Used to keep track of time
	 */
	class Time
	{
		public:
			Time(const Time&) = delete;
			Time operator=(const Time&) = delete;

			static void calcDeltaTime();
			static Time* getInstance();
			static float deltaTime();
			static void deleteInstance();
			~Time();
		private:
			Time();
			static std::unique_ptr<Time> instance; //! The instance of the time singleton
			static float dt; //! The difference in time between frames
			static clock_t time; //! The previous time
			static clock_t curTime; //! The current time
	};
};

#endif
