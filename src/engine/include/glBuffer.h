/*! \file glBuffer.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */
#ifndef GLBUFFER_H
#define GLBUFFER_H

#include <GL/glew.h>
#include "buffer.h"

namespace viv
{
	/*! \class GLBuffer
	 *  \brief An OpenGL buffer
	 */
	class GLBuffer : public virtual Buffer
	{
		public:
			GLBuffer();
			GLBuffer(const GLBuffer &gb);
			void bind() const override = 0;
			void unbind() const override = 0;
			static void unbindAllBuffers();
			virtual void bufferData(size_t size, const void* data, bool dynamic) const override = 0;
			virtual void bufferSubData(uint32_t offset, uint32_t size, const void* subData) const override = 0;
			const GLBuffer & operator=(const GLBuffer &gb);
			virtual ~GLBuffer();

		protected:
			uint32_t bufferId;

		private:
			bool isDirty = false;

	};
};

#endif
