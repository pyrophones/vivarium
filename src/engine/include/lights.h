/*! \file lights.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#ifndef LIGHTS_H
#define LIGHTS_H

#include "drawable.h"

namespace viv
{
	/*! \class Light
	 *  \brief Contains basic data for all lights
	 */
	struct Light
	{
		Vec3 pos; //! The position of the light
		float radius; //! The radius of the light
		Vec3 color; //! The color of the light
		float intensity; //! The intensity of the light
	};

	/*! \class DirectionalLight
	 *  \brief Directional light. Inherits from drawable
	 */
	class DirectionalLight : public Drawable
	{
		public:
			Light l; //! The lighting information
			DirectionalLight(const RenderBackend* backend, std::shared_ptr<Shader> s, const Light &l = Light{ Vec3(), 1.0f, Vec3(1.0f, 1.0f, 1.0f), 1.0f });
			DirectionalLight(const DirectionalLight &dl);
			const DirectionalLight & operator=(const DirectionalLight &dl);
			virtual ~DirectionalLight();

			//Static methods for as bindings
			//static void DefCtor(Light* self);
			//static void CopyCtor(const Light &other, Light* self);
			//static void InitCtor(const Vec3 &pos, const Vec3 &ambColor, const Vec3 &diffColor, const Vec3 &specColor,
			//					 float ambIntensity, float diffIntensity, float specIntensity,
			//					 float constAtten, float linearAtten, float quadAtten,
			//					 const std::string tag, char* scriptPath, Light* self);
			//static void Dtor(Light* self);

		private:
	};

	/*! \class PointLight
	 *  \brief Point light. Inherits from drawable
	 */
	class PointLight : public Drawable
	{
		public:
			Light l; //! The lighting information
			PointLight(const RenderBackend* backend, std::shared_ptr<Shader> s, const Light &l = Light{ Vec3(), 1.0f, Vec3(1.0f, 1.0f, 1.0f), 1.0f});
			PointLight(const PointLight &pl);
			const PointLight & operator=(const PointLight & pl);
			virtual ~PointLight();
	};

	/*! \class SpotLight
	 *  \brief Spot light. Inherits from drawable
	 */
	class SpotLight : public Drawable
	{
		public:
			Light l;
			Vec3 lightDir;
			float range;

			SpotLight(const RenderBackend* backend, std::shared_ptr<Shader> s, const Light &l = Light{ Vec3(), 1.0f, Vec3(1.0f, 1.0f, 1.0f), 1.0f },
					  float range = 5);
			SpotLight(const SpotLight &sl);
			const SpotLight & operator=(const SpotLight &sl);
			virtual ~SpotLight();
	};
};

#endif
