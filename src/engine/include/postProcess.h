/*! \file postProcess.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef POSTPROCESS_H
#define POSTPROCESS_H

#include <memory>
#include <GL/glew.h>
#include "texture.h"
#include "drawable.h"
#include "windowData.h"

namespace viv
{
	/*! \class PostProcess
	 *  \brief An Post Process effect
	 */
	class PostProcess : public Drawable
	{
		public:
			PostProcess(const RenderBackend* backend, std::shared_ptr<Shader> s, int32_t priority = 0, bool useDefaultDrawFunctions = true);
			PostProcess(const PostProcess &pp);
			int32_t getPriority();
			void setPriority(int32_t priority);
			const PostProcess & operator=(const PostProcess &pp);
			bool operator==(const PostProcess &pp) const;
			bool operator!=(const PostProcess &pp) const;
			bool operator>(const PostProcess &pp) const;
			bool operator<(const PostProcess &pp) const;
			bool operator>=(const PostProcess &pp) const;
			bool operator<=(const PostProcess &pp) const;
			virtual ~PostProcess();

			static bool compare(std::shared_ptr<PostProcess> pp1, std::shared_ptr<PostProcess> pp2);

		private:
			int32_t priority = 0;
	};
};

#endif

