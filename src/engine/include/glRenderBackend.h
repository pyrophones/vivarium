/*! \file glRenderBackend.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef GLRENDERBACKEND_H
#define GLRENDERBACKEND_H

#include "renderBackend.h"
#include "glBufferCollection.h"
#include "glFramebuffer.h"
#include "glTexture.h"

//Includes for X11 and GLX stuff. These need to be here since Qt is picky about header placement.
#ifdef __linux__
	#include <QtX11Extras/QX11Info>
	#include <X11/Xutil.h>
	#include <X11/extensions/Xrender.h>
	#include <GL/glx.h>
#endif

#ifdef _WIN64
	#include <QtWinExtras>
	#include <Windows.h>
#endif

namespace viv
{
	/*! \class GLRenderBackend
	 *  \brief Controls functions relating to OpenGL
	 */
	class GLRenderBackend: public RenderBackend
	{
		public:
			GLRenderBackend();
			GLRenderBackend(const GLRenderBackend &grb);
			const GLRenderBackend & operator=(const GLRenderBackend &grb);
			virtual int32_t init(WId winId) override;
			virtual BufferCollection* createBufferCollection(uint32_t vertexBufferAmount, uint32_t indexBufferAmount, uint32_t uniformBufferAmount) const override;
			virtual Framebuffer* createFramebuffer(uint32_t width, uint32_t height, bool renderbuffer) const override;
			virtual Texture* createTexture(int32_t width, int32_t height, int32_t channels, uint32_t bits, uint32_t type, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void* data, bool generateMipMaps) const override;
			virtual Texture* createTexture(int32_t width, int32_t height, int32_t channels, uint32_t type, uint32_t iFormat, uint32_t format, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void* data, bool generateMipMaps) const override;
			virtual Texture* createCubeMap(int32_t width, int32_t height, int32_t channels, uint32_t bits, uint32_t type, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void** data, bool generateMipMaps) const override;
			virtual void copyImage(const Texture* t1, const Texture* t2, uint32_t srcLvl, uint32_t srcX, uint32_t srcY, uint32_t srcZ, uint32_t dstLvl, uint32_t dstX, uint32_t dstY, uint32_t dstZ, uint32_t width, uint32_t height, uint32_t depth) const override;
			virtual void copyImage(const Texture* t1, const Framebuffer* t2, uint32_t srcLvl, uint32_t srcX, uint32_t srcY, uint32_t srcZ, uint32_t dstX, uint32_t dstY, uint32_t dstZ, uint32_t width, uint32_t height, uint32_t depth) const override;
			virtual void copyImage(const Framebuffer* t1, const Texture* t2, uint32_t srcX, uint32_t srcY, uint32_t srcZ, uint32_t dstLvl, uint32_t dstX, uint32_t dstY, uint32_t dstZ, uint32_t width, uint32_t height, uint32_t depth) const override;
			virtual void copyImage(const Framebuffer* t1, const Framebuffer* t2, uint32_t srcX, uint32_t srcY, uint32_t srcZ, uint32_t dstX, uint32_t dstY, uint32_t dstZ, uint32_t width, uint32_t height, uint32_t depth) const override;
			virtual void bindFramebuffer(const Framebuffer* fb, BindingType bt = BindingType::NORMAL) const override;
			virtual void setDepthMask(bool enabled) const override;
			virtual void setDepthTest(bool enabled) const override;
			virtual void setDepthFunction(DepthFunction df) const override;
			virtual void setBlending(bool enabled) const override;
			virtual void setBlendMode(BlendEquation be) const override;
			virtual void setBlendFunction(BlendFuncParam bfp1, BlendFuncParam bfp2) const override;
			virtual void setCulling(bool enabled) const override;
			virtual void setCullMode(CullMode cm) const override;
			virtual void setClearColor(const Vec4 &color) const override;
			virtual void clear(ClearMask cm) const override;
			virtual void draw(RenderBackend::DrawType type, const MeshData* md, uint32_t instances) const override;
			virtual void swapBuffers(const WId &winId) const override;
			virtual void resize(int32_t width, int32_t height) const override;
			~GLRenderBackend();
		private:
		#if defined __linux__
			GLXContext context;
		#elif definied _WIN32
			HDC hdc;
			HGLRC context;
		#endif

	};
};

#endif
