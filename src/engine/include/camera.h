/*! \file camera.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef CAMERA_H
#define CAMERA_H

#include "transform.h"
#include "timer.h"
#include "rigidBody.h"

namespace viv
{
	/*! \class Camera
	 *  \brief Handles creation of camera objects
	 */
	class Camera : public Transform
	{
		public:
			Camera(float fov = 0, float aspect = 0, float zear = 0, float far = 0, const Vec3 &pos = Vec3(), std::string tag = "camera");
			Camera(const Camera &c);
			virtual void update();
			const Mat4 & getViewMat() const;
			const Mat4 & getProjMat() const;
			const Mat4 & getProjViewInvMat() const;
			const float & getNear() const;
			const float & getFar() const;
			void setAspectRatio(float aspect);
			const Camera & operator=(const Camera &c);
			virtual ~Camera();

		private:
			float fov; //! The field of view of the camera
			float aspect; //! The aspect ratio of the camera
			float near; //! The near clipping plane of the camera
			float far; //! The far clipping plane of the camera

			Mat4 view; //! The view matrix
			Mat4 proj; //! The projection matrix
			Mat4 projViewInv; //! Inverted projection view matrix

			void updateProjection();
	};
};
#endif
