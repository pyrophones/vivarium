/*! \file input.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#ifndef INPUT_H
#define INPUT_H

#include <memory>
#include <map>
#include <Qt>
#include <QApplication>
#include <QEvent>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QWidget>
#include "gmath.h"
#include "windowData.h"

namespace viv
{
	/*! \class Input
	 *  \brief Input manager
	 */
	class Input
	{
		struct KeyData;
		struct MouseData;

		public:
			Input(const Input &input) = delete;
			const Input & operator=(const Input&) = delete;
			static Input* getInstance();

			//Keyboard Input
			static void keyPressEvent(QKeyEvent* event);
			static void keyReleaseEvent(QKeyEvent* event);
			static bool isKeyDown(Qt::Key key);
			static bool isKeyActive(Qt::Key key);
			static bool isKeyUp(Qt::Key key);
			static float getKeyPressDuration(Qt::Key key);
			//Mouse Input
			static void mouseMoveEvent(QMouseEvent* event);
			static void mousePressEvent(QMouseEvent* event);
			static void mouseReleaseEvent(QMouseEvent* event);
			static void mouseLeave();
			static bool isMouseButtonDown(Qt::MouseButton btn);
			static bool isMouseButtonActive(Qt::MouseButton btn);
			static bool isMouseButtonUp(Qt::MouseButton btn);
			static float getMouseButtonPressDuration(Qt::MouseButton btn);
			static const Vec2 & getCurrentMousePosition();
			static void setMousePosition(const Vec2 &newPos);
			static const Vec2 & getPrevMousePosition();
			static void disableCursor(bool disabled);
			static void lockMouse(bool locked);
			//static bool isMouseWheelActive();
			static void cleanStates();
			static void deleteInstance();
			~Input();

		private:
			static std::unique_ptr<Input> instance; //! The instance of the Input singleton
			static std::map<Qt::Key, KeyData> keyStates; //! Map of keys and their data
			static std::map<Qt::MouseButton, MouseData> mouseStates; //! Map of mouse buttons and their data
			static bool mouseLocked; //! Bool to check if the mouse is locked

			Input();

			/*! \struct KeyData
			 *  \brief Holds data about key presses
			 */
			struct KeyData
			{
				public:
					float pressDuration; //! How long the key has been pressed
					bool isPressed; //! If the key was pressed
					bool isActive; //! If the key is active (held)
					bool isReleased; //! If the key is released
			};

			/*! \struct MouseData
			 *  \brief Holds data about mouse presses and position
			 */
			struct MouseData
			{
				public:
					static Vec2 curMousePosition; //! The position of the mouse
					static Vec2 prevMousePosition; //! The previous position of the mouse
					float pressDuration; //! How long the key has been pressed
					bool isPressed; //! If the key was pressed
					bool isActive; //! If the key is active (held)
					bool isReleased; //! If the key is released
			};
	};

};

#endif
