/*! \file transform.h
 * \author Giovanni Aleman
 *
  ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#ifndef TRANSFORM_H
#define TRANSFORM_H
#define GLM_FORCE_RADIANS

#include <GL/glew.h>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include "node.h"

namespace viv
{
	/*! \class Transform
	 *  \brief Handles the creation of transform nodes. Inherits from Node
	 */
	class Transform : public Node
	{
		public:
			Transform(const Vec3 &pos = Vec3(), const std::string tag = "transform");
			Transform(const Mat4 &trans);
			Transform(const Transform &t);
			virtual void update();
			const Vec3 & getPos() const;
			void setPos(const Vec3 &pos);
			const Vec3 & getScale() const;
			void setScale(const Vec3 &scale);
			const Quat & getRot() const;
			void setRot(const Quat &rot);
			const Vec3 & getFront() const;
			const Vec3 & getRight() const;
			const Vec3 & getUp() const;
			const Transform & operator=(const Transform &t);
			virtual ~Transform();

			//Static methods for as bindings
			static void DefCtor(Transform* self);
			static void CopyCtor(const Transform &other, Transform* self);
			static void InitCtor(const Vec3 &pos, std::string tag, Transform* self);
			static void Dtor(Transform* self);

		protected:
			Vec3 front; //! The front vector of the object
			Vec3 right; //! The right vector of the object
			Vec3 up; //! The up vector of the object

		private:
			Vec3 pos; //! The position of the object
			Vec3 scale; //! The scale of the object
			Quat rot; //! The rotation of the object
			bool isDirty; //! If the transform matrix should be updated
	};
};
#endif
