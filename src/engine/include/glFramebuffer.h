/*! \file glFramebuffer.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef GLFRAMEBUFFER_H
#define GLFRAMEBUFFER_H

#include <memory>
#include <GL/glew.h>
#include "framebuffer.h"
#include "glTexture.h"

namespace viv
{
	/*! \class GLFramebuffer
	 *  \brief An OpenGL frame buffer
	 */
	class GLFramebuffer : public Framebuffer
	{
		public:
			GLFramebuffer(uint32_t rboWidth, uint32_t rboHeight, bool renderBuffer);
			GLFramebuffer(const GLFramebuffer &gfb);
			virtual void attachTexture(const Texture* texture, uint32_t attachmentNum, uint32_t type, uint32_t mipLevel) const override;
			virtual void attachDepthStencil(const Texture* texture, bool depth, bool stencil, uint32_t type, uint32_t mipLevel) const override;
			virtual void setDrawBuffers(uint32_t* attachments, uint32_t size) const override;
			virtual void resize(uint32_t width, uint32_t height) override;
			virtual uint32_t getFramebuffer() const;
			virtual uint32_t getRenderbuffer() const;
			const GLFramebuffer & operator=(const GLFramebuffer &gfb);
			virtual ~GLFramebuffer();

		private:
			uint32_t fbo;
			uint32_t rbo;
	};
};

#endif

