/*! \file mesh.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef VERTEX_H
#define VERTEX_H

#include "gmath.h"

namespace viv
{
	/*! \struct Vertex
	 *  \brief Helper structure for holding mesh data
	 */
	struct Vertex
	{
		public:
			Vec3 pos;
			Vec2 uv;
			Vec3 norm;
			Vec3 tan;
			Vec3 bitan;

			Vertex(const Vec3 &pos= Vec3(), const Vec2 &uv = Vec2(), const Vec3 &norm = Vec3(0.0, 0.0, 1.0), const Vec3 &tan = Vec3(), const Vec3 &bitan = Vec3());
			Vertex(const Vertex &v);
			const Vertex & operator=(const Vertex &v);
			bool operator==(const Vertex &v);
			bool operator!=(const Vertex &v);
			~Vertex();
	};
};

#endif
