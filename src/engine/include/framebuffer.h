/*! \file frameBuffer.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <memory>
#include "texture.h"

namespace viv
{
	/*! \class Framebuffer
	 *  \brief Generic class for frame buffers
	 */
	class Framebuffer
	{
		public:
			Framebuffer(uint32_t rboWidth, uint32_t rboHeight);
			Framebuffer(const Framebuffer &fb);
			virtual void attachTexture(const Texture* texture, uint32_t attachmentNum, uint32_t type, uint32_t mipLevel) const = 0;
			virtual void attachDepthStencil(const Texture* texture, bool depth, bool stencil, uint32_t type, uint32_t mipLevel) const = 0;
			virtual const Texture* getDepthTexture() const;
			virtual void setDrawBuffers(uint32_t* attachments, uint32_t size) const = 0;
			virtual void resize(uint32_t width, uint32_t height);
			const Framebuffer & operator=(const Framebuffer &fb);
			virtual ~Framebuffer();

		protected:
			uint32_t rboWidth;
			uint32_t rboHeight;
			Texture* depthTex = nullptr;
	};
};

#endif

