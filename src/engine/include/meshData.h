/*! \file meshData.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef MESHDATA_H
#define MESHDATA_H

#include <memory>
#include <vector>
#include "vertex.h"

namespace viv
{
	/*! \struct MeshData
	 *  \brief Contains mesh data
	 */
	struct MeshData
	{
		public:
			MeshData();
			MeshData(std::shared_ptr<Vertex[]> verts, std::shared_ptr<uint32_t[]> inds, std::shared_ptr<uint32_t[]> offsets, std::shared_ptr<uint32_t[]> amounts, uint32_t vCount, uint32_t iCount, uint32_t oCount, uint32_t aCount);
			MeshData(const MeshData &md);
			void copyVerticesFromVector(std::vector<Vertex> verts);
			void copyIndicesFromVector(std::vector<uint32_t> inds);
			void copyIndexOffsetsFromVector(std::vector<uint32_t> offsets);
			void copyIndexAmountsFromVector(std::vector<uint32_t> amounts);
			uint32_t getVertexCount() const;
			uint32_t getIndexCount() const;
			uint32_t getIndexOffsetCount() const;
			uint32_t getIndexAmountCount() const;
			const Vertex* getVertices() const;
			const uint32_t* getIndices() const;
			const uint32_t* getIndexOffsets() const;
			const uint32_t* getIndexAmounts() const;
			const MeshData & operator=(const MeshData &md);
			~MeshData();

		private:
			uint32_t vCount;
			uint32_t iCount;
			uint32_t oCount;
			uint32_t aCount;
			std::shared_ptr<Vertex[]> verts = std::shared_ptr<Vertex[]>(nullptr);
			std::shared_ptr<uint32_t[]> inds = std::shared_ptr<uint32_t[]>(nullptr);
			std::shared_ptr<uint32_t[]> offsets = std::shared_ptr<uint32_t[]>(nullptr);
			std::shared_ptr<uint32_t[]> amounts = std::shared_ptr<uint32_t[]>(nullptr);
	};
};

#endif
