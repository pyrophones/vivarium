/*! \file rigidBody.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#ifndef RIGIDBODY_H
#define RIGIDBODY_H

#include "timer.h"
#include "collisionBody.h"

namespace viv
{
	/*! \class RigidBody
	 *  \brief Handles creation and calculations of basic RigidBodies. Inherits from CollisionBody
	 */
	class RigidBody : public CollisionBody
	{
		public:
			Vec3 vel = Vec3(), force = Vec3();
			Vec3 aVel = Vec3(), torq = Vec3();
			Vec3 grav = Vec3(0.0, -9.8, 0.0);

			float mass = 1, speed = 0, drag = 1;
			float inert = 1, aSpeed = 0, aDrag = 1;
			float maxSpeed = 0, maxASpeed = 0;
			bool useGravity = true;

			RigidBody(bool useGravity = true, const Vec3 &pos = Vec3(), const Vec3 &min = Vec3(), const Vec3 &max = Vec3(), std::string tag = "collisionBody");
			RigidBody(const RigidBody &r);
			virtual void update();
			const RigidBody & operator=(const RigidBody &r);
			virtual ~RigidBody();

			//Static methods for as bindings
			static void DefCtor(RigidBody* self);
			static void CopyCtor(const RigidBody &other, RigidBody* self);
			static void InitCtor(bool useGravity, const Vec3 &pos, const Vec3 &min, const Vec3 &max, std::string tag, RigidBody* self);
			static void Dtor(RigidBody* self);
	};
};

#endif
