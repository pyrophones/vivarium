/*! \file glUniformBuffer.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */
#ifndef GLUNIFORMBUFFER_H
#define GLUNIFORMBUFFER_H

#include <GL/glew.h>
#include "glBuffer.h"
#include "uniformBuffer.h"

namespace viv
{
	/*! \class GLUniformBuffer
	 *  \brief An OpenGL uniform buffer
	 */
	class GLUniformBuffer : public GLBuffer, public UniformBuffer
	{
		public:
			GLUniformBuffer();
			GLUniformBuffer(const GLUniformBuffer &gub);
			void bind() const override;
			void unbind() const override;
			virtual void bufferData(size_t size, const void* data, bool dynamic) const override;
			virtual void bufferSubData(uint32_t offset, uint32_t size, const void* subData) const override;
			void bindBufferBase(uint32_t) const override;
			const GLUniformBuffer & operator=(const GLUniformBuffer &gub);
			virtual ~GLUniformBuffer();
	};
};

#endif


