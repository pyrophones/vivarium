/*! \file window.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef WINDOW_H
#define WINDOW_H

#include "gmath.h"

namespace viv
{
	/*! \class WindowData
	 *  \brief Holds properties assosiated with the window
	 */
	struct WindowData
	{
		static Vec2 outerSize;
		static Vec2 innerSize;
		static Vec2 outerPos;
		static Vec2 innerPos;
		static Vec2 center;
	};
};

#endif
