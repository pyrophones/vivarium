/*! \file vkWidget.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef GRAPHICSVK_H
#define GRAPHICSVK_H

#include <iostream>
#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "console.h"
#include "input.h"
#include "texture.h"
#include "shader.h"
#include "game.h"
#include "camera.h"

namespace viv
{
	class VK
	{
		public:
			VK(const VK&) = delete;
			VK operator= (const VK&) = delete;
			static int32_t init(GLFWwindow** window, int32_t width, int32_t height, void (*errorCallback)(int32_t, const char*)); //Initialization method
			static int run();
			static int32_t end();

			static VK* getInstance(); //Gets instance and creates one if none exists
			~VK();
		private:
			VK();
			static std::unique_ptr<VK> instance;
			static VkSurfaceKHR surface;
	};
};


#endif
