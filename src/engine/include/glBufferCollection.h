/*! \file glBbufferCollection.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */
#ifndef GLBUFFERCOLLECTION_H
#define GLBUFFERCOLLECTION_H

#include <GL/glew.h>
#include "bufferCollection.h"
#include "glVertexBuffer.h"
#include "glIndexBuffer.h"
#include "glUniformBuffer.h"

namespace viv
{
	/*! \class GLBufferCollection
	 *  \brief An collection of OpenGL buffers
	 */
	class GLBufferCollection : public BufferCollection
	{
		public:
			GLBufferCollection(uint32_t vertexBufferAmount, uint32_t indexBufferAmount, uint32_t uniformBufferAmount);
			GLBufferCollection(const GLBufferCollection &gbc);
			virtual void bind() const override;
			virtual void unbind() const override;
			virtual const GLVertexBuffer* getVertexBuffer(uint32_t index) const override;
			virtual const GLIndexBuffer* getIndexBuffer(uint32_t index) const override;
			virtual const GLUniformBuffer* getUniformBuffer(uint32_t index) const override;
			virtual void deleteBuffer(BufferCollection::BufferType type, uint32_t index) override;
			const GLBufferCollection & operator=(const GLBufferCollection &gbc);
			virtual ~GLBufferCollection();

		private:
			uint32_t vao;
			std::vector<GLVertexBuffer*> vertexBuffers;
			std::vector<GLIndexBuffer*> indexBuffers;
			std::vector<GLUniformBuffer*> uniformBuffers;

			virtual void createBuffer(BufferCollection::BufferType type, uint32_t amount) override;
	};
};

#endif



