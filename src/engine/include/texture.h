/*! \file texture.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef TEXTURE_H
#define TEXTURE_H

#include <cstdint>

namespace viv
{
	/*! \class Texture
	 *  \brief Handles creation and usage of textures
	 */
	class Texture
	{
		public:
			Texture(int32_t width, int32_t height, int32_t channels);
			Texture(const Texture &t);

			virtual void genMipMaps() = 0;
			virtual void bind(uint32_t bindingPoint) const = 0;
			virtual void unbind(uint32_t bindingPoint) const = 0;
			virtual void resize(uint32_t width, uint32_t height, const void* data) = 0;
			virtual void resize(uint32_t width, uint32_t height, const void** data) = 0;
			virtual void destroy() = 0;
			const int32_t & getWidth() const;
			const int32_t & getHeight() const;
			const int32_t & getChannels() const;
			const Texture & operator=(const Texture &t);
			virtual ~Texture();

		protected:
			int32_t width; //! The width of the texture
			int32_t height; //! The height of the texture
			int32_t channels; //! The channels of the texture
	};
};
#endif
