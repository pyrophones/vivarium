/*! \file script.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef SCRIPT_H
#define SCRIPT_H

#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>
#include <cstring>
#include <GL/glew.h>
#include "angelscript.h"

BEGIN_AS_NAMESPACE

namespace viv
{
	/*! \class Script
	 *  \brief Handles the creation and execution of scripts
	 */
	class Script
	{
		public:
			Script(const char* data = nullptr, const char* name = nullptr);
			Script(const Script &script);
			void loadScript(const char* data);
			void callFunc(const char* funcName);
			//void callMethod(const char* method);
			const Script & operator=(const Script &s);
			~Script();

		private:
			std::string name;
			static std::unordered_map<std::string, std::unordered_map<std::string, asIScriptFunction*>> controllers;
			static std::unordered_map<std::string, asIScriptContext*> contexts;
	};
};

#endif
