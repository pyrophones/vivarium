/*! \file drawable.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <memory>
#include <vector>
#include <functional>
#include <cstring>
#include "shader.h"
#include "renderBackend.h"
#include "meshData.h"
#include "material.h"

namespace viv
{
	/*! \class Drawable
	 *  \brief Basic class for items that are drawn to the screen
	 */
	class Drawable
	{
		public:
			bool shouldRender = false;
			enum TexHandleType { ALBEDO, NORMAL, METAL, ROUGH, AO, DISP };

			//NOTE: Change these to function pointers?
			std::function<void(void)> buffer; //! \brief Buffers the drawable
			std::function<void(void)> preDraw; //! \brief Prepares the mesh for drawing
			std::function<void(void)> postDraw; //! \brief Unbinds mesh properties after drawing
			Drawable();
			Drawable(Vertex* vertices, uint32_t* indices, uint32_t* offsets, uint32_t* amounts, uint32_t vertexCount, uint32_t indexCount, uint32_t offsetCount, uint32_t amountCount, std::shared_ptr<Shader> shader);
			Drawable(std::shared_ptr<MeshData> md, std::function<void(void)> bufferFunc, std::function<void(void)> preDrawFunc, std::function<void(void)> postDrawFunc, std::shared_ptr<Shader> shader);
			Drawable(std::shared_ptr<MeshData> md, std::shared_ptr<Shader> shader);
			Drawable(const Drawable &d);
			void addWorldAndNormalMat(Mat4 &wMat, Mat3 &nMat);
			//void removeWorldMat(Mat4 &mat);
			void removeLastWorldAndNormalMat();
			void addObjectTint(const Vec3 &tint);
			void removeLastTint();
			void addMaterial(std::shared_ptr<Material> mat);
			void removeMaterial(std::shared_ptr<Material> mat);
			const MeshData* getMeshData() const;
			Shader* getShader() const;
			const BufferCollection* getBufferCollection() const;
			uint32_t getWorldMatCount();
			void setMeshData(std::shared_ptr<MeshData> md);
			void setShader(std::shared_ptr<Shader> shader);
			void setBufferCollection(BufferCollection* bc);
			const Drawable & operator=(const Drawable &d);
			virtual ~Drawable();

		protected:
			std::shared_ptr<MeshData> md;
			std::shared_ptr<Shader> shad = nullptr;
			std::vector<Mat4*> worldMats;
			std::vector<Mat3*> normalMats;
			std::vector<const Vec3*> objTints;
			std::shared_ptr<Material> mats[5] = {};
			float parallaxStrengths[5] = {};
			float parallaxBiases[5] = {};
			std::vector<uint32_t> matNums;
			BufferCollection* bufferCollection = nullptr;
		private:
	};
};

#endif
