/*! \file vivarium.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef VIVARIUM_H
#define VIVARIUM_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QApplication>
#include <QDesktopWidget>
#include "system.h"

namespace Ui {
	class Vivarium;
};

namespace viv
{
	/*! \class Vivarium
	 *  \brief Handles main window creation
	 */
	class Vivarium : public QMainWindow
	{
		Q_OBJECT

		public:
		    explicit Vivarium(QWidget* parent = 0);
			void closeEvent(QCloseEvent* event);
			int32_t startSystem();
		    ~Vivarium();

		private:
		    Ui::Vivarium* ui; //! UI file for the main window
	};
};
#endif // CITYSCAPES_H
