/*! \file fileLoader.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef FILELOADER_H
#define FILELOADER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <utility>
#include <vector>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#endif
#include "rapidxml.hpp"
#include "meshData.h"

namespace viv
{
	/*! \namespace FileLoader
	 *  \brief File loading methods
	 */
	namespace FileLoader
	{
		std::shared_ptr<MeshData> loadMeshData(const char* path);
		void setLoaderProperties();
		void loadTextureData(const char* path, unsigned char* &data, int32_t* width, int32_t* height, int32_t* channels);
		void deleteTextureData(unsigned char* &data);
		void loadTextureHDR(const char* path, float* &data, int32_t* width, int32_t* height, int32_t* channels);
		void deleteTextureHDR(float* &data);
		void loadText(const char* path, char* &data);
		void deleteText(char* &data);
		void loadMaterialData(const char* path, std::string* matName, std::map<std::string, std::pair<std::string, std::string>>* data);
		void deleteMaterialData();
		void loadShader(const char* path, std::string* name, std::map<std::string, std::string>* data);
	};
};

#endif

