/*! \file component.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#ifndef COMPONENT_H
#define COMPONENT_H

#include <string>
#include <memory>
#include <new>
#include <cstdint>
#include "gmath.h"
//#include "rapidxml.hpp"

namespace viv
{
	/*! \class Component
	 *  \brief Base class for components
	 */
	class Component
	{
		public:
			Component();
			Component(const Component &c);
			virtual void update() = 0;
			//Node* getParent();
			const Component & operator=(const Component &c);
			virtual ~Component();

			//Static methods for as bindings
			//static void DefCtor(Component* self);
			//static void CopyCtor(const Component &other, Component* self);
			//static void InitCtor(Component* self);
			//static void Dtor(Component* self);
	};
};

#endif


