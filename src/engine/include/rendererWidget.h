/*! \file rendererWidget.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef RENDERERWIDGET_H
#define RENDERERWIDGET_H

//#include <iostream>
//#include <cstring>
//#include <memory>
//#include <cstdint>
//#include <GL/glew.h>
//#define GLM_FORCE_RADIANS
//#include <glm/glm.hpp>
//#include <glm/gtc/type_ptr.hpp>

#include <QWidget>
#include <QResizeEvent>
#include "renderBackend.h"
#include "input.h"
#include "windowData.h"
#include "assetManager.h"
#include "console.h"
#include "input.h"
#include "texture.h"
#include "shader.h"
#include "game.h"
#include "camera.h"
#include "lights.h"

namespace Ui
{
	class Renderer;
};

namespace viv
{
	/*! \class Renderer
	 *  \brief Abstract QWidget for rendering engines
	 */
	class Renderer : public QWidget
	{
	    Q_OBJECT
		public:
			Renderer(QWidget* parent = nullptr);
			virtual void init(const RenderBackend* backend); //Initialization method
			virtual void render(); //Render method
			virtual QPaintEngine* paintEngine() const override;
			virtual ~Renderer();

		protected:
			virtual void paintEvent(QPaintEvent* event) override;
			virtual void resizeEvent(QResizeEvent* event) override;
			virtual void keyPressEvent(QKeyEvent* event) override;
			virtual void keyReleaseEvent(QKeyEvent* event) override;
			virtual void mousePressEvent(QMouseEvent* event) override;
			virtual void mouseReleaseEvent(QMouseEvent* event) override;
			virtual void mouseMoveEvent(QMouseEvent* event) override;
			virtual void leaveEvent(QEvent* event) override;
			virtual void wheelEvent(QWheelEvent* event) override;
		private:
			Ui::Renderer* ui = nullptr; //! Pointer for the UI
			bool pingPong1 = false;
			Texture* gRoughTexture;
			Texture* gAlbedoMetalTexture;
			Texture* gNormalAoTexture;
			Texture* brdfLUT;
			Texture* pingPongTexture1;
			Texture* pingPongTexture2;
			Framebuffer* gBuffer;
			Framebuffer* pingPongBuffer1;
			Framebuffer* pingPongBuffer2;
			Drawable fsQuadAmbient;
			PostProcess* finalProcess;
			const RenderBackend* backend = nullptr;
			//std::vector<std::unique_ptr<RenderComponent>> renderComponents;
	};
};
#endif
