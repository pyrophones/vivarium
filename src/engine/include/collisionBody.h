/*! \file collisionBody.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#ifndef COLLISIONBODY_H
#define COLLISIONBODY_H

#include <unordered_set>
#include "transform.h"

namespace viv
{
	/*! \class CollisionBody
	 *  \brief Handles the creation of CollisionBodies. Inherits from Transform
	 */
	class CollisionBody : public Transform
	{
		public:
			bool isColliding, doSphere = true, doSAT = true;

			CollisionBody(const Vec3 &min = Vec3(), const Vec3 &max = Vec3(), const Vec3 &pos = Vec3(), std::string tag = "collisionBody");
			CollisionBody(const CollisionBody &cb);
			virtual void update();
			std::unordered_set<CollisionBody*> getCollidingBodies();
			const CollisionBody & operator=(const CollisionBody &cb);
			virtual ~CollisionBody();

			//Static methods for as bindings
			static void DefCtor(CollisionBody* self);
			static void CopyCtor(const CollisionBody &other, CollisionBody* self);
			static void InitCtor(const Vec3 &min, const Vec3 &max, const Vec3 &pos, std::string tag, CollisionBody* self);
			static void Dtor(CollisionBody* self);

		protected:
			static std::vector<CollisionBody*> collisionObjects;
			std::unordered_set<CollisionBody*> collidingWith;

			bool AABB(const CollisionBody &cb);
			bool Sphere(const CollisionBody &cb);
			bool SAT(const CollisionBody &cb);
			void reorientBox();

		private:
			Vec3 localMin; //! The minimum point in local coords
			Vec3 localMax; //! The maximum point in local coords
			Vec3 globalMin; //! The minumum point in global coords
			Vec3 globalMax; //! The maximum point in global coords
			Vec3 halves; //! The halfway vector
			float radius; //! The radius of the body
			Mat4 prevTransMat; //! The previous bounding box matrix
	};
};
#endif
