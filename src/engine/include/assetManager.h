/*! \file assetManager.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef ASSETLOADER_H
#define ASSETLOADER_H

#include <string>
#include <vector>
#include <unordered_map>
#include <memory>
#include <algorithm>
#include <locale>
#include <cstdio>
#include <cstring>
#include <dirent.h>
#include <dlfcn.h>
#include "fileLoader.h"
#include "renderBackend.h"
#include "mesh.h"
#include "postProcess.h"
#include "texture.h"
#include "material.h"
#include "script.h"
#include "lights.h"

namespace viv
{
	/*! \class AssetManager
	 *  \brief Assists in loading assets into the game engine
	 */
	class AssetManager
	{
		public:
			enum FileType { MESH, TEXTURE, SCRIPT, TEXTURE_HDR, MATERIAL, SHADER, AUD, LIB, SCENE }; //Holds the different filetypes

			AssetManager(const AssetManager&) = delete;
			void operator=(const AssetManager&) = delete;
			static void init(const RenderBackend* backend);
			static AssetManager* getInstance();
			static std::shared_ptr<MeshData> getMeshData(const char* id);
			static std::shared_ptr<Mesh> getMesh(const char* id);
			static std::shared_ptr<Texture> getTexture(const char* id);
			static std::shared_ptr<Script> getScript(const char* id);
			static std::shared_ptr<Material> getMaterial(const char* id);
			static std::shared_ptr<Shader> getShader(const char* id);
			static std::unordered_map<std::string, std::shared_ptr<Mesh>> getAllMeshes();
			static std::vector<std::shared_ptr<PostProcess>> getAllPostProcesses();
			static void createMeshData(const char* name, const MeshData &md);
			static void createMesh(const char* name, const char* meshDataName, const char* shaderName);
			static void createTexture(const char* name, int32_t width, int32_t height, int32_t channels, uint32_t bits, uint32_t type, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void* data, bool genMipMaps);
			static void createMaterial(const char* name, const char* albedo, const char* normal, const char* metal, const char* roughness, const char* ao, const char* disp);
			static void createPostProcess(const char* shaderName, int32_t priority, bool useDefaultDrawFunctions = true);
			static void loadAsset(const char* path, FileType f, std::string name);
			static void loadDirectory(const char* path);
			static void deleteInstance();
			~AssetManager();
		private:
			//typedef void (*LibUpdate)();
			AssetManager();
			static void createPrimatives();
			static void loadLibrary(const char* path);

			static std::unique_ptr<AssetManager> instance;
			//static std::vector<void *> loadedLibs;
			//static std::vector<libUpdate*> libUpdates;
			static std::unordered_map<std::string, std::shared_ptr<MeshData>> loadedMeshData;
			static std::unordered_map<std::string, std::shared_ptr<Mesh>> loadedMeshes;
			static std::unordered_map<std::string, std::shared_ptr<Texture>> loadedTextures;
			static std::unordered_map<std::string, std::shared_ptr<Script>> loadedScripts;
			static std::unordered_map<std::string, std::shared_ptr<Material>> loadedMaterials;
			static std::unordered_map<std::string, std::shared_ptr<Shader>> loadedShaders;
			static std::vector<std::shared_ptr<PostProcess>> loadedPostProcesses;
			//TODO: unique ids
			//static std::unordered_map<uint32_t, std::shared_ptr<Asset>> loadedAssets;
			static const RenderBackend* backend;
	};
};

#endif
